# prevents vulture errors. TODO Why is this necessary
from oct2py import octave

import holoshed.initialize_holoshed
import holoshed.lung_fusion
import holoshed.makeGREIT
import holoshed.plot_global
import holoshed.setup_forward_model
import holoshed.utils

holoshed.initialize_holoshed.initialize_holoshed  # unused function (holoshed/initialize_holoshed.py:7)
holoshed.setup_forward_model.setup_forward_model  # unused function (holoshed/setup_forward_model.py:10)
holoshed.lung_fusion.lungFusion  # unused function (holoshed/lung_fusion.py:10)
holoshed.utils.get_working_path  # unused function (holoshed/utils.py:15)
holoshed.makeGREIT.makeGREIT  # unused function (holoshed/makeGREIT.py:5)
holoshed.utils.plot_eit()  # unused function (holoshed/utils.py:33)
holoshed.plot_global.plot_global()
holoshed.plot_global.plot_detached_electrodes()
pl = holoshed.utils.FePlot()  # unused class (holoshed/utils.py:56)
pl.plot_fe_surf()  # unused method (holoshed/utils.py:70)

# Attributes are only used in octave scripts
m = holoshed.lung_fusion.Model()
m.fmdl.stimulation
m.fmdl.meas_select
m.all_img_i[0].time
m.all_img_i[0].time_step
img_i = octave.mk_image(m.fmdl, 0, "conductivity")
img_i.fwd_solve.get_all_meas

m.open_vtu()

fmdl = holoshed.setup_forward_model.mesh2EIDORSfmdl("dummy_file", {})
fmdl.gnd_node
fmdl.solve
fmdl.jacobian
fmdl.system_mat
fmdl.normalize_measurements
fmdl.perm_sym
