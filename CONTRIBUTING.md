# Contributing to holoshed

1. [Prerequisites](#prerequisites)
1. [Installation](#installation)
1. [Testing](#testing)
1. [Documentation](#documentation)
1. [Troubleshooting](#troubleshooting)

## Installation

If you are a developer, execute the following steps to install holoshed in
editable mode. This ensures that any changes you make to holoshed,
automatically reflect in the python package.

#### Create a development environment
The use of a virtual environment like
[Miniconda](https://docs.conda.io/en/latest/miniconda.html)
is highly recommended.
After installing Miniconda, create a new environment by running:      
```bash
conda create -n holoshed python=3.9
```

This will create a new Python 3.9 environment called  *holoshed*.
To activate it run:   
```bash
conda activate holoshed
```

#### Install holoshed
After setting up Miniconda and the new, dedicated conda
environment *holoshed* is created and activated as described above,
**holoshed** can be installed by:
```bash
pip install -e ".[devel]"
```
This will install **holoshed** and all its requirements for running and
testing automatically. If you are contibuting to **holoshed** please make
sure you also have the pre-commit hooks installed by running:
```bash
pre-commit install
```
They ensure that your commits are formatted correctly and meet some
basic code style requirements.

To update all python packages in your conda environment type:  
```bash
conda update --all
```

## Testing
Holoshed uses a variety of tests to ensure correct functioning of all
modules and that every line of code meets some basic requirements to
format and code style. In particular, the tests in holoshed can be
grouped into the following categories:
* Unit and integration tests with **[pytest](https://docs.pytest.org/)**
* Formatting with **[black](https://pypi.org/project/black/)**, **[flake8](https://flake8.pycqa.org/)** and **[isort](https://pypi.org/project/isort/)**
* Dead code with **[vulture](https://pypi.org/project/vulture/)**


### Unit and integration tests
To run the unit and integration tests simply type:
```bash
pytest
```
or with verbose output:
```bash
pytest -ra -v
```
A coverage report will be automatically generated in `doc/cov` when running the tests.


### Formatting
Formatting in holoshed is automatically done by **black**. Simply run
```bash
black .
```
to format all your code. To further sort all your imports in the correct way
you can run **isort** by :
```bash
isort .
```
After running black and isort you can additionally check if
**flake8** finds any remaining issues with:
```bash
flake8
```
In contrast to black and isort, however, flake8 does not format your code
automatically and requires manual fixing of the detected issues.

### Dead code
Holoshed is also tested for dead code with **vulture**. You can run the test
locally with 
```bash
vulture .
```

## Documentation

Holoshed uses **[sphinx](https://www.sphinx-doc.org/en/master/)** to automatically build an html-documentation from docstring.

#### Build the documentation locally

To build the holoshed documentation locally, navigate into the root
folder of holoshed and type:    
```bash
sphinx-apidoc -o doc/source holoshed -f -M
sphinx-build -b html -d doc/build/doctrees  doc/source doc/build/html
```
The html version of your documentation can then be found in `doc/build`.

#### In the GitLab CI/CD
The master version of the documentation is automatically published on GitLab
pages and can be viewed by clicking on the **sphinx badge** on the top of this
page.

## Troubleshooting
If you run into a problem, please proceed according to the following steps:
1. Search through our [open issues](https://gitlab.com/ebenbuild/holoshed/-/issues) to find out whether the problem has already been reported and whether a workaround exists.
1. Report it as a new issue using the template for bug reports from the dropdown menu.
