from os.path import join as join

import numpy as np
import pytest

import holoshed.lung_fusion as lung_fusion
import holoshed.VTUReader as VTUReader
from holoshed.utils import repo_base_path as repo_base_path


def test_static_reader_load_split():
    """Test whether the split reader automatically loads constant data"""
    vtu_path = get_test_vtu_path("simulation_0002.26.vtu", True)
    config = {"vtu_split_flag": True}
    reader = VTUReader.VTUReader.create(config, vtu_path)
    assert isinstance(reader.static_reader, VTUReader.VTUReaderSplit)


def test_static_reader_skip_split():
    """Test whether the split reader properly skips loading constant data"""
    vtu_path = get_test_vtu_path("simulation_0002.constant.0.vtu", True)
    config = {"vtu_split_flag": True}
    reader = VTUReader.VTUReader.create(config, vtu_path)
    assert reader.static_reader is None


def test_get_ac_split():
    """Testing the get_ac method for VTUs with a static/dynamic split."""
    vtu_path = get_test_vtu_path("simulation_0002.constant.0.vtu", True)
    config = {"vtu_split_flag": True}
    dummy_reader = VTUReader.VTUReader.create(config, vtu_path)

    assert not dummy_reader.is_ac[0]
    assert dummy_reader.is_ac[-1]
    assert np.argmax(dummy_reader.is_ac) == 255


def test_get_ac_combined():
    """Testing the get_ac method for combined VTUs."""
    vtu_path = get_test_vtu_path("dummy-07817.vtu", False)
    config = {"vtu_split_flag": False}
    dummy_reader = VTUReader.VTUReader.create(config, vtu_path)

    assert not dummy_reader.is_ac[0]
    assert dummy_reader.is_ac[7880]
    assert np.argmax(dummy_reader.is_ac) == 918


def test_get_ac_nodes_and_indices():
    """Testing the get_ac_nodes method of the VTUReader parent class"""
    vtu_path = get_test_vtu_path("simulation_0002.constant.0.vtu", True)
    config = {"vtu_split_flag": True}
    dummy_reader = VTUReader.VTUReader.create(config, vtu_path)
    dummy_reader.get_ac()

    assert dummy_reader.ac_indices[0][0] == 255
    assert dummy_reader.ac_indices[-1][0] == 382
    assert np.allclose(
        dummy_reader.ac_nodes[0],
        np.array((103.67570496, -227.3001709, 868.3972168)),
        1e-7,
    )
    assert np.allclose(
        dummy_reader.ac_nodes[-1],
        np.array((-112.97685242, -134.58062744, 973.25073242)),
        1e-7,
    )


def test_ac_radius():
    """Testing the ac_radius method of the VTUReader class."""
    vtu_path = get_test_vtu_path("simulation_0002.constant.0.vtu", True)
    config = {"vtu_split_flag": True}
    dummy_reader = VTUReader.VTUReader.create(config, vtu_path)
    assert np.allclose(
        dummy_reader.ac_radius[[0, -1]],
        np.array([[21.32433891], [17.07932854]]),
        1e-7,
    )


def test_get_filling_factor_split():
    """Testing the get_filling_factor method for VTUs with a static/dynamic split."""
    vtu_path = get_test_vtu_path("simulation_0002.26.vtu", True)
    config = {
        "working_path": "/dummy/path",
        "eidors_path": "/dummy/path",
        "vtu_split_flag": True,
    }
    dummy_reader = VTUReader.VTUReader.create(config, vtu_path)
    m = lung_fusion.Model(config, "dummy_fmdl")
    m.ac_mapping = np.array([[297], [342]])
    FF = dummy_reader.get_filling_factor(m)
    assert np.allclose(FF, np.array([[2.24332545], [2.9646832]]), 1e-7)
    with pytest.raises(RuntimeError):
        dummy_reader.static_reader = None
        dummy_reader.get_filling_factor(m)


def test_get_tracheal_flow_split():
    """Testing the get_tracheal_flow method for VTUs with a static/dynamic split."""
    vtu_path = vtu_path = get_test_vtu_path("simulation_0002.26.vtu", True)
    config = {"vtu_split_flag": True}
    dummy_reader = VTUReader.VTUReader.create(config, vtu_path)
    flow = dummy_reader.get_tracheal_flow(0)
    assert np.abs(flow - 79882.57031370213) < 1e-7


def test_get_ventil_pressure_split():
    """Testing the get_ventil_pressure method for VTUs with a static/dynamic split."""
    vtu_path = vtu_path = get_test_vtu_path("simulation_0002.26.vtu", True)
    config = {"vtu_split_flag": True, "tubus": "mallinckrodt_107_80"}
    dummy_reader = VTUReader.VTUReader.create(config, vtu_path)
    press = dummy_reader.get_ventil_pressure(0, np.float16(-3e4))
    print(press)
    assert np.abs(press - 884.2154389984483) < 1e-7


def test_tracheal_idx_split():
    """Testing the tracheal_idx method for VTUs with a static/dynamic split."""
    vtu_path = vtu_path = get_test_vtu_path(
        "simulation_0002.constant.0.vtu", True
    )
    config = {"vtu_split_flag": True}
    dummy_reader = VTUReader.VTUReader.create(config, vtu_path)
    assert dummy_reader.tracheal_idx == 0


def test_get_filling_factor_combined():
    """Testing the get_filling_factor method for combined VTUs."""
    vtu_path = vtu_path = get_test_vtu_path("dummy-07817.vtu", False)
    config = {
        "working_path": "/dummy/path",
        "eidors_path": "/dummy/path",
    }
    m = lung_fusion.Model(config, "dummy_fmdl")
    m.ac_mapping = [31895, 31895]
    config = {"vtu_split_flag": False}
    dummy_reader = VTUReader.VTUReader.create(config, vtu_path)

    FF = dummy_reader.get_filling_factor(m)
    assert np.allclose(FF, np.array((1.69644016, 1.69644016)), 1e-7)


def test_get_tracheal_flow_combined():
    """Testing the get_tracheal_flow method for combined VTUs."""
    vtu_path = vtu_path = get_test_vtu_path("dummy-07817.vtu", False)
    config = {"vtu_split_flag": False}
    dummy_reader = VTUReader.VTUReader.create(config, vtu_path)
    flow = dummy_reader.get_tracheal_flow(1371)
    assert np.abs(flow - 41808.9296144231) < 1e-7


def test_get_ventil_pressure_combined():
    """Testing the get_ventil_pressure method for combined VTUs."""
    vtu_path = vtu_path = get_test_vtu_path("dummy-07817.vtu", False)
    config = {"vtu_split_flag": False}
    dummy_reader = VTUReader.VTUReader.create(config, vtu_path)
    press = dummy_reader.get_ventil_pressure(1371, None)
    assert np.abs(press - 1092.8593864132283) < 1e-7


def test_tracheal_idx_combined():
    """Testing the get_tracheal_idx method for combined VTUs."""
    vtu_path = vtu_path = get_test_vtu_path("dummy-07817.vtu", False)
    config = {"vtu_split_flag": False}
    dummy_reader = VTUReader.VTUReader.create(config, vtu_path)
    assert dummy_reader.tracheal_idx == 1371


def get_test_vtu_path(filename, vtu_split_flag):
    """Utility function returning the joined absolute path of a vtu file in the reference files.
    Args:
        filename        (str):  Name of the vtu_file
        vtu_split_flag (bool):  True if desired vtu is a split vtu with dynamic or static quantities.
    Returns:
        vtu_path        (str):  Path to the desired vtu for testing purposes.
    """
    if vtu_split_flag:
        vtu_path = join(
            repo_base_path(),
            "tests",
            "reference_files",
            "split_vtu",
            "01_input",
            filename,
        )
    else:
        vtu_path = join(
            repo_base_path(),
            "tests",
            "reference_files",
            "comb_vtu",
            "01_input",
            filename,
        )

    return vtu_path
