import numpy as np

import holoshed.JsonDataIO
from holoshed.JsonDataIO import JsonDataIO as JsonDataIO


def test_write(mocker):
    """Testing the write method of the JsonDataIO class."""
    mocker.patch("holoshed.JsonDataIO.modify_dict")
    mock_gzip_open = mocker.patch("gzip.open")
    mock_json_dumps = mocker.patch("json.dumps")
    json_pars = JsonDataIO({"dummy": "data"})
    json_pars.write("/dummy/path/dummy.json")

    mock_gzip_open.assert_called_once()
    mock_json_dumps.assert_called_once()


def test_reader(mocker):
    """Testing the read method of the JsonDataIO class."""
    mock_gzip_open = mocker.patch("gzip.open")
    mock_json_loads = mocker.patch(
        "json.loads", return_value={"dummy": "data"}
    )
    json_pars = JsonDataIO()
    data = json_pars.read("/dummy/path/dummy.json")

    mock_gzip_open.assert_called_once()
    mock_json_loads.assert_called_once()
    assert data["dummy"] == "data"


def test_restore_original_dict(mocker):
    """Testing the restore_original_dict function."""
    mod_dict = {
        "a": [["(1+0.2j)"], ["(1+0.2j)"]],
        "b": [[1.0, 1.0], [1.0, 1.0]],
        "c": 3.14,
        "d": {"mat": [[1.0], [1.0]]},
        "e": [2, 4],
    }
    orig_dict = holoshed.JsonDataIO.restore_original_dict(mod_dict)

    a = np.ones((2, 1)) * complex(1, 0.2)
    b = np.ones((2, 2))
    c = 3.14
    d = {"mat": np.ones((2, 1))}
    e = [2, 4]

    ref_orig_dict = {"a": a, "b": b, "c": c, "d": d, "e": e}
    np.testing.assert_equal(orig_dict, ref_orig_dict)


def test_modify_dict():
    """Testing the modify_dict function."""
    a = np.ones((2, 1)) * complex(1, 0.2)
    b = np.ones((2, 2))
    c = 3.14
    d = {"mat": np.ones((2, 1))}
    e = [2, 4]

    dummy_dict = {"a": a, "b": b, "c": c, "d": d, "e": e}

    mod_dict = holoshed.JsonDataIO.modify_dict(dummy_dict)

    ref_mod_dict = {
        "a": [["(1+0.2j)"], ["(1+0.2j)"]],
        "b": [[1.0, 1.0], [1.0, 1.0]],
        "c": 3.14,
        "d": {"mat": [[1.0], [1.0]]},
        "e": [2, 4],
    }
    assert mod_dict == ref_mod_dict


def test_np_array_to_list_of_strings():
    """Testing the np_array_to_list_of_strings function."""
    arr1 = np.array([complex(1, 2), complex(4, 6)])
    arr2 = np.array([[arr1], [arr1], [arr1]])

    l1 = holoshed.JsonDataIO.np_array_to_list_of_strings(arr1)
    l2 = holoshed.JsonDataIO.np_array_to_list_of_strings(arr2)

    assert l1 == ["(1+2j)", "(4+6j)"]
    assert l2 == [
        [["(1+2j)", "(4+6j)"]],
        [["(1+2j)", "(4+6j)"]],
        [["(1+2j)", "(4+6j)"]],
    ]


def test_get_first_instance_of_list():
    """Testing the get_first_instance_of_list function."""
    l1 = [[["dummy"]]]
    l2 = ["dummy"]
    l3 = []
    assert holoshed.JsonDataIO.get_first_instance_of_list(l1) == "dummy"
    assert holoshed.JsonDataIO.get_first_instance_of_list(l2) == "dummy"
    assert holoshed.JsonDataIO.get_first_instance_of_list(l3) == []


def test_contains_complex_number():
    """Testing the contains_complex_number function."""
    s1 = "(1+3j)"
    s2 = "dummy"

    assert holoshed.JsonDataIO.contains_complex_number(s1)
    assert not holoshed.JsonDataIO.contains_complex_number(s2)
