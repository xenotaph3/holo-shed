import numpy as np

from scripts.lnm.get_reconst_timesteps_const_peep import (
    determine_rt_file_id,
    determine_rt_time_limits,
    get_eval_and_ref_timesteps,
    get_volume_data,
    main,
)


def test_main(mocker):
    """Test the main function."""
    config = {
        "working_path": "/dummy/path",
        "sim_name": "dummy",
        "evaluate_times": {"maneuver1": [460, 536], "maneuver2": [810, 870]},
        "remove_id_begin": [[1, 400, 700, 1100], [1, 200, 600]],
        "remove_id_end": [[50, 600, 750, 1200], [100, 400, 700]],
        "considered_maneuver": "maneuver1",
        "sim_freq": 10,
        "realtime_freq": 10,
        "ramping_time": 1.5,
        "num_add_reco_timesteps": 3,
    }
    mocker.patch("json.load", return_value=config)
    mocker.patch("builtins.open")
    mocker.patch(
        "scripts.lnm.get_reconst_timesteps_const_peep.determine_rt_file_id",
        return_value=(0, np.array([80, 100])),
    )
    mocker.patch(
        "scripts.lnm.get_reconst_timesteps_const_peep.determine_rt_time_limits",
        return_value=(74.4, 82.0),
    )
    mocker.patch(
        "scripts.lnm.get_reconst_timesteps_const_peep.get_volume_data",
        return_value=(np.array([1, 2, 3]), np.array([0, 0.1, 0.2])),
    )
    mocker.patch("scipy.signal.find_peaks", return_value=(np.array([1]),))
    mocker.patch(
        "scripts.lnm.get_reconst_timesteps_const_peep.get_eval_and_ref_timesteps",
        return_value=(None, None, None),
    )
    mocker.patch("matplotlib.pyplot.plot")
    mocker.patch("matplotlib.pyplot.show")
    assert main("/dummy/path/00_config/pre_config.json") == 0


def test_determine_rt_file_id():
    """Test the determine_rt_file_id function."""
    begin_rm = [[1, 400, 700, 1100], [1, 200, 600]]
    end_rm = [[50, 600, 750, 1200], [100, 400, 700]]
    config = {
        "evaluate_times": {"maneuver1": [460, 536], "maneuver2": [810, 870]},
        "considered_maneuver": "maneuver1",
        "sim_freq": 10,
        "realtime_freq": 10,
    }

    eval_frames = config["evaluate_times"][config["considered_maneuver"]]
    rt_file_id, cum_time = determine_rt_file_id(
        begin_rm, end_rm, eval_frames, config
    )
    assert rt_file_id == 0
    assert np.allclose(cum_time, np.array([80, 110]))

    config["considered_maneuver"] = "maneuver2"
    eval_frames = config["evaluate_times"][config["considered_maneuver"]]
    rt_file_id, cum_time = determine_rt_file_id(
        begin_rm, end_rm, eval_frames, config
    )
    assert rt_file_id == 1
    assert np.allclose(cum_time, np.array([80, 110]))


def test_determine_rt_time_limits():
    """Test the determine_rt_time_limits function."""
    begin_rm = [[1, 400, 700, 1100], [1, 200, 600]]
    end_rm = [[50, 600, 750, 1200], [100, 400, 700]]
    rt_file_id = 0
    step_offset = 460
    eval_duration = (536 - 460) * 1 / 10
    config = {
        "evaluate_times": {"maneuver1": [460, 536], "maneuver2": [810, 870]},
        "considered_maneuver": "maneuver1",
        "sim_freq": 10,
        "realtime_freq": 10,
        "ramping_time": 1.5,
    }
    cum_time = np.array([80, 100])
    rt_start, rt_end = determine_rt_time_limits(
        begin_rm,
        end_rm,
        rt_file_id,
        step_offset,
        eval_duration,
        cum_time,
        config,
    )
    assert rt_start == 74.4
    assert rt_end == 82.0

    rt_file_id = 1
    step_offset = 810
    eval_duration = (870 - 810) * 1 / 10
    config["considered_maneuver"] = "maneuver2"

    rt_start, rt_end = determine_rt_time_limits(
        begin_rm,
        end_rm,
        rt_file_id,
        step_offset,
        eval_duration,
        cum_time,
        config,
    )
    assert rt_start == 9.4
    assert rt_end == 15.4


def test_get_volume_data(mocker):
    """Test the get_volume_data function."""
    config = {"working_path": "/dummy/path", "sim_name": "dummy"}
    mocker.patch("os.path.isdir", return_value=False)
    mocker.patch("os.mkdir")
    mocker.patch(
        "holoshed.lung_fusion.Model", return_value=mock_lung_fusion_model()
    )
    mocker.patch(
        "holoshed.JsonDataIO.JsonDataIO.read",
        return_value={
            "lung_vol_l": np.array([1, 2, 3]),
            "sim_time_s": np.array([0, 0.1, 0.2]),
        },
    )

    vol, time = get_volume_data(config)

    assert np.array_equal(vol, np.array([1, 2, 3]))
    assert np.array_equal(time, np.array([0, 0.1, 0.2]))


def test_get_eval_and_ref_timesteps():
    """Test the get_eval_and_ref_timesteps function."""
    maxima = (np.array([40, 70, 120]),)
    minima = (np.array([10, 55, 90]),)
    step_offset = 100
    num_add_reco_timesteps = 2

    eval_timesteps, ref_timesteps, _ = get_eval_and_ref_timesteps(
        maxima, minima, step_offset, num_add_reco_timesteps
    )

    assert np.array_equal(
        eval_timesteps,
        np.array([110, 138, 139, 140, 155, 168, 169, 170, 190, 218, 219, 220]),
    )
    assert np.array_equal(ref_timesteps, np.array([110, 155, 190]))


class mock_lung_fusion_model:
    """Mock class for the lung_fusion.Model class."""

    def __init__(self):
        pass

    def check_for_valid_vtu(self):
        pass

    def read_global_quantities_from_sim_vtu(self):
        pass
