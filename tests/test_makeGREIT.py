import os

import numpy as np
from oct2py import octave

import holoshed.JsonDataIO
import holoshed.makeGREIT as makeGREIT
import holoshed.utils


def test_makeGREIT(mocker):
    """Test covering the makeGREIT function"""
    all_img_i = [octave.struct("fwd_model", "dummy_mdl")]
    config = {
        "dummy_key": "dummy_value",
        "reconstruct_from_meas_data": True,
        "reconstruct_from_sim_data": True,
        "GREIT_template_conductivity_lung": 3.0,
        "GREIT_template_conductivity_thorax": 1.0,
        "GREIT_template_conductivity_other_tissues": [0.5],
    }

    base_path = holoshed.utils.repo_base_path()
    octave.addpath(os.path.join(base_path, "holoshed", "octave_scripts"))
    octave.addpath(
        os.path.join(base_path, "tests", "reference_files", "eidors_mock")
    )
    mock_init_makeGREIT = mocker.patch(
        "holoshed.makeGREIT.initialize_makeGREIT", return_value=config
    )
    mock_prep_eidors_img = mocker.patch(
        "holoshed.makeGREIT.prepare_eidors_image", return_value=all_img_i[0]
    )
    mock_assign_stim_pat = mocker.patch(
        "holoshed.makeGREIT.assign_stimulation_patterns",
        return_value=all_img_i[0],
    )
    mock_prep_sim_data = mocker.patch(
        "holoshed.makeGREIT.prepare_simulation_data", return_value="dummy"
    )
    mock_prep_meas_data = mocker.patch(
        "holoshed.makeGREIT.prepare_measurement_data",
        return_value=["dummy", "dummy"],
    )

    mock_oct_call_mk_GREIT_mod = mocker.patch(
        "holoshed.makeGREIT.octave.call_mk_GREIT_model",
        return_value="dummy_image",
    )

    makeGREIT.makeGREIT(config, all_img_i)

    mock_prep_eidors_img.assert_called_once()
    mock_assign_stim_pat.assert_called_once()
    mock_init_makeGREIT.assert_called_once()
    mock_prep_sim_data.assert_called_once()
    mock_prep_meas_data.assert_called_once()
    assert mock_oct_call_mk_GREIT_mod.call_count == 2


def test_initialize_makeGREIT():
    """Testing the function initialize_makeGREIT."""
    config = {
        "reconstruct_from_sim_data": True,
        "sim_duration_tot": 15.0,
        "realtime_start": 25.0,
        "realtime_end": 39.0,
    }

    config = makeGREIT.initialize_makeGREIT(config)
    assert config["prestress_time"] == 1.0


def test_prepare_eidors_image(mocker):
    """Testing the prepare_eidors_image function of the makeGREIT module."""
    fmdl_dummy = octave.struct(
        "meas_select",
        "dummy_pattern",
        "lung_IDs",
        np.arange(3, 7),
        "tiss_IDs",
        "dummy",
    )
    fmdl_dummy.tiss_IDs = [np.array([1]), np.array([2])]
    img_dummy = octave.struct(
        "fwd_model", fmdl_dummy, "elem_data", np.ones((7, 1))
    )

    base_path = holoshed.utils.repo_base_path()
    octave.addpath(
        os.path.join(base_path, "tests", "reference_files", "eidors_mock")
    )

    mock_oct_mk_img = mocker.patch(
        "holoshed.makeGREIT.octave.mk_image", return_value=img_dummy
    )

    img = makeGREIT.prepare_eidors_image(fmdl_dummy, 3.0, 1.0, [0.5, 0.7])

    mock_oct_mk_img.assert_called_once()
    assert not ("meas_select" in img.fwd_model)
    assert np.array_equal(
        img.elem_data,
        np.array([[1], [0.5], [0.7], [3], [3], [3], [3]]),
    )


def test_assign_stimulation_patterns(mocker):
    """Testing the assign_stimulation_patterns function of the makeGREIT module."""
    dummy_stim = octave.struct("stim_pattern", "dummy_stimulation")
    dummy_meas = np.array((0, 0, 1, 1, 0))
    config = {
        "num_electrodes": 32,
        "num_rings": 1,
        "inject_pattern": "{adj}",
        "meas_pattern": "{ad}",
        "meas_current": "no_meas_current_next2",
        "amp": 0.005,
    }
    fmdl_dummy = octave.struct(
        "meas_select", "dummy_pattern", "stimulation", "dummy_pattern"
    )
    img_dummy = octave.struct("fwd_model", fmdl_dummy)

    base_path = holoshed.utils.repo_base_path()
    octave.addpath(
        os.path.join(base_path, "tests", "reference_files", "eidors_mock")
    )
    mock_oct_mk_stim_pat = mocker.patch(
        "holoshed.makeGREIT.octave.mk_stim_patterns",
        return_value=[dummy_stim, dummy_meas],
    )

    img_dummy = makeGREIT.assign_stimulation_patterns(config, img_dummy)

    assert img_dummy.fwd_model.stimulation == dummy_stim
    assert np.all(img_dummy.fwd_model.meas_select == np.array((0, 0, 1, 1, 0)))
    assert mock_oct_mk_stim_pat.called_once


def test_prepare_measurement_data(mocker):
    """Testing the prepare_measurement_data function."""
    mocker.patch(
        "holoshed.makeGREIT.MeasEitReader", return_value=DummyMeasEitReader()
    )
    vv, detach_elec = makeGREIT.prepare_measurement_data("dummy_config")
    assert np.allclose(vv, np.ones((10, 2)))
    assert detach_elec == [3, 6]


def test_prepare_simulation_data(mocker):
    """Testing the prepare_simulation_data function."""
    config = {"working_path": "/dummy/path", "sim_name": "dummy_name"}
    mocker.patch("builtins.open")
    mock_json_read = mocker.patch(
        "holoshed.makeGREIT.JsonDataIO.read",
        return_value={"volt_data": np.ones((10, 2))},
    )

    vv = makeGREIT.prepare_simulation_data(config)

    assert np.allclose(vv, np.ones((10, 2)))
    mock_json_read.assert_called_once()


class DummyMeasEitReader:
    """Dummy class mocking the behavior of the MeasEitReader."""

    def __init__(self):
        pass

    def get_volt_reconst_frames(self):
        return np.ones((10, 2))

    def get_detached_electrodes(self):
        return [3, 6]
