import os
from os.path import join as join
from tempfile import TemporaryDirectory

import defusedxml.ElementTree as ET
import pytest

from holoshed.PvdIO import PvdIO as PvdIO
from holoshed.utils import repo_base_path as repo_base_path


def test_read():
    """Testing the read method of PvdIO"""
    base_path = repo_base_path()
    pvd_path = join(
        base_path,
        "tests",
        "reference_files",
        "split_vtu",
        "01_input",
        "simulation_0002.pvd",
    )
    pvd = PvdIO.from_file(pvd_path)
    assert pvd.data == [
        {
            "timestep": "2.60000000000000e-01",
            "group": "",
            "part": "0",
            "file": "simulation_0002.26.vtu",
        },
        {
            "timestep": "1.30000000000000e+01",
            "group": "",
            "part": "0",
            "file": "simulation_0002.1300.vtu",
        },
    ]


def test_write():
    """Testing the write method of PvdIO"""
    pvd = PvdIO()
    pvd.create_data_collection([0.1], ["dummy.1.vtu"])
    with TemporaryDirectory() as tmpdir:
        path = join(tmpdir, "dummy.pvd")
        pvd.write(path)

        assert os.path.isfile(path)
        tree = ET.parse(path)
        root = tree.getroot()  #
        assert root[0][0].attrib["timestep"] == "0.1"
        assert root[0][0].attrib["part"] == "0"


def test_get_single_vtu_time():
    """Testing the get_single_vtu_time method of PvdIO"""
    dummy_data_split = [
        {"timestep": "0.1", "file": "simulation_0002.1.vtu"},
        {"timestep": "0.2", "file": "simulation_0002.2.vtu"},
    ]
    dummy_data_comb = [
        {"timestep": "0.1", "file": "red_airway-1-0.vtu"},
        {"timestep": "0.2", "file": "red_airway-2-0.vtu"},
    ]

    pvd_split = PvdIO(dummy_data_split)
    pvd_comb = PvdIO(dummy_data_comb)

    assert pvd_split.get_single_vtu_time("simulation_0002.1.vtu") == 0.1
    assert pvd_comb.get_single_vtu_time("red_airway-2-0.vtu") == 0.2
    with pytest.raises(RuntimeError):
        pvd_split.get_single_vtu_time("dummy.vtu")


def test_get_timesteps_and_filenames():
    """Testing the get_timesteps_and_filenames method of PvdIO."""
    dummy_data = [
        {"timestep": "0.1", "file": "file1.vtu"},
        {"timestep": "0.2", "file": "file2.vtu"},
    ]
    pvd = PvdIO(dummy_data)
    timesteps, filenames = pvd.get_timesteps_and_filenames()
    assert timesteps == ["0.1", "0.2"]
    assert filenames == ["file1.vtu", "file2.vtu"]


def test_create_data_collection():
    """Testing the create_data_collection method of PvdIO"""
    dummy_timesteps = [0.1, 0.2]
    dummy_filenames = ["dummy.1.vtu", "dummy.2.vtu"]

    pvd = PvdIO()
    pvd.create_data_collection(dummy_timesteps, dummy_filenames)

    ref_data = [
        {"timestep": "0.1", "group": "", "part": "0", "file": "dummy.1.vtu"},
        {"timestep": "0.2", "group": "", "part": "0", "file": "dummy.2.vtu"},
    ]

    assert pvd.data == ref_data
    with pytest.raises(RuntimeError):
        pvd.create_data_collection([0, 1], ["file.vtu"])
