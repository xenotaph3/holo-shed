import os
from os.path import join as join
from tempfile import TemporaryDirectory

import numpy as np
import pytest
from oct2py import Struct, octave

import holoshed.setup_forward_model
from holoshed import utils


def test_setup_forward_model(mocker):
    """Testing the setup_forward_model function."""
    base_path = utils.repo_base_path()
    octave.addpath(os.path.join(base_path, "holoshed", "octave_scripts"))
    config = {
        "working_path": "/dummy/path",
        "surfaceID_elec": None,
        "surfaceID_lungs": None,
        "volumeID_mat": None,
        "show_lung_contours": True,
    }
    dummy_fmdl = octave.struct("nodes", None, "mat_idx", octave.cell(4, 1))
    dummy_fmdl.mat_idx[0][0] = np.arange(0)  # dummy material indices
    dummy_fmdl.mat_idx[0][1] = np.array([1, 2]).reshape(2, 1)
    dummy_fmdl.mat_idx[0][2] = np.array([3, 4]).reshape(2, 1)
    dummy_fmdl.mat_idx[0][3] = np.array([5, 6]).reshape(2, 1)
    mock_check_mesh = mocker.patch(
        "holoshed.setup_forward_model.check_for_valid_mesh"
    )
    mock_mesh2EIDORSfmdl = mocker.patch(
        "holoshed.setup_forward_model.mesh2EIDORSfmdl",
        return_value=[dummy_fmdl, None, None],
    )
    mock_write_lung_cache = mocker.patch(
        "holoshed.setup_forward_model.write_lung_mesh_to_cache"
    )
    mock_define_stim_pat = mocker.patch(
        "holoshed.setup_forward_model.define_stimulation_pattern",
        return_value=dummy_fmdl,
    )
    mocker.patch(
        "holoshed.setup_forward_model.determine_IDs_of_other_tissues",
        return_value=["dummy"],
    )

    fmdl = holoshed.setup_forward_model.setup_forward_model(config)

    mock_check_mesh.assert_called_once()
    mock_mesh2EIDORSfmdl.assert_called_once()
    mock_write_lung_cache.assert_called_once()
    mock_define_stim_pat.assert_called_once()
    assert np.array_equal(fmdl.lung_IDs, np.array([0, 1, 2, 3], dtype=int))
    assert fmdl.tiss_IDs == ["dummy"]


def test_check_for_valid_mesh():
    """Testing the check_for_valid_mesh function of the setup_forward_model module."""
    with TemporaryDirectory() as tmpdir:
        input_path = join(tmpdir, "01_input")
        os.mkdir(input_path)
        with pytest.raises(RuntimeError):
            holoshed.setup_forward_model.check_for_valid_mesh(tmpdir)

        open(join(input_path, "dummy1.mesh"), "a").close()
        mesh = holoshed.setup_forward_model.check_for_valid_mesh(tmpdir)
        assert mesh == join(input_path, "dummy1.mesh")

        open(join(input_path, "dummy2.mesh"), "a").close()
        with pytest.raises(RuntimeError):
            holoshed.setup_forward_model.check_for_valid_mesh(tmpdir)


def test_mesh2EIDORSfmdl(reference_files, mocker):
    """Testing the mesh2EIDORSfmdl function of the setup_forward_model module."""
    base_path = utils.repo_base_path()
    octave.addpath(
        os.path.join(base_path, "tests", "reference_files", "eidors_mock")
    )
    mesh_file = os.path.join(
        reference_files, "comb_vtu", "01_input", "dummy.mesh"
    )
    mocker.patch(
        "holoshed.setup_forward_model.octave.eidors_obj",
        return_value=dummy_eidors_fmdl(),
    )
    config = {
        "conductivity_other_tissues": [1, 2],
        "working_path": os.path.join(reference_files, "comb_vtu"),
        "num_electrodes": 32,
        "surfaceID_elec": [
            4,
            5,
            6,
            7,
            8,
            9,
            10,
            11,
            12,
            13,
            14,
            15,
            16,
            17,
            18,
            19,
            20,
            21,
            22,
            23,
            24,
            25,
            26,
            27,
            28,
            29,
            30,
            31,
            32,
            33,
            34,
            35,
        ],
        "surfaceID_lungs": [1, 2],
        "volumeID_mat": [1003, 1001, 1002],
    }

    (
        fmdl,
        topo_left_lung,
        topo_right_lung,
    ) = holoshed.setup_forward_model.mesh2EIDORSfmdl(mesh_file, config)

    assert fmdl.nodes.shape == (27309, 3)
    assert np.array_equal(
        fmdl.nodes[10000], np.array([139.3925, -80.4501, 504.1239])
    )
    assert fmdl.elems.shape == (117388, 4)
    assert np.array_equal(
        fmdl.elems[10000], np.array([20319, 18780, 22128, 23943])
    )
    assert fmdl.boundary.shape == (32898, 3)
    assert np.array_equal(fmdl.boundary[20000], np.array([1350, 9856, 1348]))
    assert fmdl.gnd_node == 5128
    assert fmdl.electrode.nodes[0].shape == (199,)
    assert np.array_equal(
        fmdl.electrode.nodes[0][30:35],
        np.array([2011, 2012, 2013, 2014, 2015]),
    )
    assert all(fmdl.electrode.z_contact[:] == 0.01 * np.ones(32))
    assert fmdl.mat_idx[0, 0].shape == (108802, 1)
    assert np.array_equal(fmdl.mat_idx[0, 0][10000], np.array([18587]))
    assert all(
        [
            f == "eidors_default"
            for f in [fmdl.solve, fmdl.jacobian, fmdl.system_mat]
        ]
    )
    assert fmdl.normalize_measurements == 0
    assert fmdl.np_fwd_solve.perm_sym == "{n}"
    assert topo_left_lung.shape == (1618, 3)
    assert np.array_equal(topo_left_lung[1500], np.array([3169, 3192, 3170]))
    assert topo_right_lung.shape == (1814, 3)
    assert np.array_equal(topo_right_lung[1500], np.array([3971, 4008, 3993]))


def test_write_lung_mesh_to_cache(mocker):
    """Testing the write_lung_mesh_to_cache function of the setup_forward_model module."""
    working_path = "/dummy/path"
    mocker.patch("builtins.open")
    mock_json_write = mocker.patch("holoshed.MeasEitReader.JsonDataIO.write")

    holoshed.setup_forward_model.write_lung_mesh_to_cache(
        working_path, None, None, None
    )
    mock_json_write.assert_called_once()


def test_define_stimulation_pattern(mocker):
    """Testing the define_stimulation_pattern function of the setup_forward_model module."""
    base_path = utils.repo_base_path()
    octave.addpath(
        os.path.join(base_path, "tests", "reference_files", "eidors_mock")
    )
    mock_oct_stim_pat = mocker.patch(
        "holoshed.setup_forward_model.octave.mk_stim_patterns",
        return_value=["dummy_stim", "dummy_meas"],
    )
    fmdl = Struct()
    fmdl.no_meas = None
    fmdl.stimulation = None
    fmdl.meas_select = None

    config = {
        "num_electrodes": 32,
        "num_rings": 1,
        "inject_pattern": [0, 5],
        "meas_pattern": [0, 5],
        "meas_current": "no_meas_current_next2",
        "amp": 0.005,
    }
    new_fmdl = holoshed.setup_forward_model.define_stimulation_pattern(
        fmdl, config
    )
    assert mock_oct_stim_pat.call_count == 2
    assert new_fmdl.no_meas == "dummy_meas"
    assert new_fmdl.stimulation == "dummy_stim"
    assert new_fmdl.meas_select == "dummy_meas"


def test_determine_IDs_of_other_tissues():
    """Testing the determine_IDs_of_other_tissues function of the setup_forward_model module."""
    dummy_fmdl = octave.struct("nodes", None, "mat_idx", octave.cell(5, 1))
    dummy_fmdl.mat_idx[0][3] = np.array([1, 2]).reshape(2, 1)
    dummy_fmdl.mat_idx[0][4] = np.array([3, 4]).reshape(2, 1)
    config = {
        "conductivity_other_tissues": [1, 2],
    }
    tiss_IDs = holoshed.setup_forward_model.determine_IDs_of_other_tissues(
        dummy_fmdl, config
    )
    # compare all elements of the list
    assert np.all(tiss_IDs[0] == np.array([0, 1]))
    assert np.all(tiss_IDs[1] == np.array([2, 3]))


def dummy_eidors_fmdl():
    """Create a dummy EIDORS forward model object."""
    fmdl = Struct()
    return fmdl
