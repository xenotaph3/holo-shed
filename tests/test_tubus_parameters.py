import pytest

import holoshed.tubus_parameters


def test_get_guttmann_constants():
    """Testing the get_guttmann_constants function of the tubus_parameters module."""

    config_mal_107_80 = {"tubus": "mallinckrodt_107_80"}
    (
        k1_mal_107_80_pos,
        k2_mal_107_80_pos,
    ) = holoshed.tubus_parameters.get_guttmann_constants(
        config_mal_107_80, True
    )
    (
        k1_mal_107_80_neg,
        k2_mal_107_80_neg,
    ) = holoshed.tubus_parameters.get_guttmann_constants(
        config_mal_107_80, False
    )

    assert k1_mal_107_80_pos == 644.296905
    assert k2_mal_107_80_pos == 1.94
    assert k1_mal_107_80_neg == 735.49875
    assert k2_mal_107_80_neg == 1.75


def test_get_guttmann_constants_runtime_error():
    """Testing the runtime error of the get_guttmann_constants function."""

    config = {"tubus": "dummy"}
    with pytest.raises(RuntimeError):
        holoshed.tubus_parameters.get_guttmann_constants(config, True)
