"""Testing the plot_eit module."""
import os

import numpy as np
from oct2py import octave
from scipy.sparse import csc_array

import holoshed.plot_eit
import holoshed.utils


def test_plot_eit(mocker):
    """Test the function plot_eit returning the reconstructed EIT image.
    Constructs a dummy problem on 4x4 pixels and compares the reconstructed image rimg to the dummy result rimg_ref."""

    # creating sparse unit matrix with asymmetrical shape (7x5)
    c2f = csc_array(
        (np.ones(5, dtype=int), (np.arange(0, 5), np.arange(0, 5))),
        shape=(7, 5),
    )

    mdl_slice_mapper = octave.struct(
        "x_pts",
        np.linspace(-1, 1, 4),
        "y_pts",
        np.linspace(-1, 1, 4),
        "level",
        np.array(np.array([0, 0, 1])),
    )
    fmdl_dummy = octave.struct(
        "coarse2fine", c2f, "mdl_slice_mapper", mdl_slice_mapper
    )
    eit_dummy = octave.struct(
        "fwd_model", fmdl_dummy, "time_step", 1, "time", 0.4
    )
    eit_dummy.elem_data = np.array((1, 1, 2, 2, 3))

    eit_images_dummy = octave.cell(2, 1)
    eit_images_dummy[0][0] = eit_dummy
    eit_images_dummy[1][0] = eit_dummy

    elem_ptr_dummy = np.array(
        [[0, 0, 0, 0], [0, 1, 2, 0], [0, 3, 4, 0], [0, 5, 0, 0]]
    )
    config = {
        "GREIT_imgsz": 4,
        "working_path": "/dummy/path",
        "vtu_timesteps": [1, 2],
        "color_levels": 100,
        "show_lung_contours": True,
    }

    rimg_ref = np.full((4, 4), np.nan)
    rimg_ref[1:4, 1:3] = np.array([[1, 1], [2, 2], [3, np.nan]])

    base_path = holoshed.utils.repo_base_path()
    octave.addpath(
        os.path.join(base_path, "tests", "reference_files", "eidors_mock")
    )

    mock_mdl_slice_mapper = mocker.patch(
        "holoshed.plot_eit.octave.mdl_slice_mapper",
        return_value=elem_ptr_dummy,
    )
    mock_contourf = mocker.patch("holoshed.plot_eit.plt.contourf")
    mocker.patch("holoshed.plot_eit.plt.colorbar")
    mocker.patch("os.makedirs")
    mocker.patch("holoshed.plot_eit.plt.savefig")
    mocker.patch("holoshed.plot_eit.plt.show")
    mock_get_lung_contours = mocker.patch(
        "holoshed.plot_eit.get_lung_contours",
        return_value={
            "left": {"nodes": [1, 2], "ele": [3, 4]},
            "right": {"nodes": [4, 5], "ele": [6, 7]},
        },
    )
    mock_plot_lung_contour = mocker.patch(
        "holoshed.plot_eit.plot_lung_contour"
    )
    mocker_export_npz = mocker.patch("holoshed.plot_eit.np.savez_compressed")

    rec_images = holoshed.plot_eit.plot_eit(
        eit_images_dummy, config, "/dummy/path", "dummy_prefix", True
    )

    assert mock_mdl_slice_mapper.call_count == 2
    assert mock_contourf.call_count == 2
    assert mocker_export_npz.call_count == 1
    mock_get_lung_contours.assert_called_once()
    assert mock_plot_lung_contour.call_count == 4
    np.testing.assert_equal(rec_images[0], rimg_ref)
    np.testing.assert_equal(rec_images[1], rimg_ref)


def test_get_lung_contours(mocker):
    """Testing the get_lung_contours function."""
    config = {"working_path": "/dummy/path"}
    level = 0
    mocker.patch("builtins.open")
    nodes = np.array([[1, 0, 1], [1, 0, -1], [-1, 0, 1]])
    topo = np.array([[1.0, 2.0, 3.0]])
    mock_json_read = mocker.patch(
        "holoshed.makeGREIT.JsonDataIO.read",
        return_value={"nodes": nodes, "left_lung": topo, "right_lung": topo},
    )
    mock_get_intersection_line = mocker.patch(
        "holoshed.plot_eit.get_intersection_line", return_value="dummy_line"
    )
    holoshed.plot_eit.get_lung_contours(config, level)

    mock_json_read.assert_called_once()
    assert mock_get_intersection_line.call_count == 2


def test_plot_lung_contour(mocker):
    """Testing the plot_lung_contour function."""
    mock_plot = mocker.patch("matplotlib.pyplot.plot")
    dummy_lung = np.array([[1, 0, 0], [0, 1, 0], [0, 0, 0], [1, 0, 0]])
    holoshed.plot_eit.plot_lung_contour(dummy_lung)
    mock_plot.assert_called_once()
