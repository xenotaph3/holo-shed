import os
import subprocess
import sys

import pytest


@pytest.fixture
def run_in_docker():
    """
    Tests if we are currently running in our own docker container.
    """
    if sys.platform == "linux":
        if os.path.isfile("/opt/holoshed/includes/run_in_docker"):
            return
    pytest.skip()


def test_integration_comb(run_in_docker, base_path, reference_files):
    """
    Runs main and tests the existence of all expected output files for the 'comb_vtu' test case
    """

    comb_path = os.path.join(reference_files, "comb_vtu/")

    # run main
    main_path = os.path.join(base_path, "main.py")
    config_path = os.path.join(comb_path, "00_config", "config.json")
    subprocess.run(["python", main_path, config_path])

    dir_list = [
        os.path.join(dir[0], file)
        for dir in os.walk(comb_path)
        for file in os.listdir(dir[0])
    ]
    dir_list.sort()

    ref_path_list = [
        "00_config",
        "00_config/config.json",
        "01_input",
        "01_input/dummy-07817.vtu",
        "01_input/dummy-08618.vtu",
        "01_input/dummy.eit",
        "01_input/dummy.mesh",
        "01_input/dummy.pvd",
        "01_input/realtimeValues_20210707094239.csv",
        "02_inhomogeneous_mesh",
        "02_inhomogeneous_mesh/electrode_order_frontal_view.png",
        "02_inhomogeneous_mesh/electrode_order_top_view.png",
        "02_inhomogeneous_mesh/lung_topology.json",
        "03_forward_results",
        "03_forward_results/comb_vtu_dummy_elec_voltages.json",
        "04_simulated_reconstruction_results",
        "04_simulated_reconstruction_results/comb_vtu_dummy_EIT_image_ts7817.png",
        "04_simulated_reconstruction_results/comb_vtu_dummy_EIT_image_ts8618.png",
        "04_simulated_reconstruction_results/eit_reconstruction_data.npz",
        "05_measured_reconstruction_results",
        "05_measured_reconstruction_results/detached_electrodes.png",
        "05_measured_reconstruction_results/eit_meas.json",
        "05_measured_reconstruction_results/eit_reconstruction_data.npz",
        "05_measured_reconstruction_results/meas_EIT_image_ts7817.png",
        "05_measured_reconstruction_results/meas_EIT_image_ts8618.png",
        "06_global_postprocessing_results",
        "06_global_postprocessing_results/comb_vtu_dummy_global_quant_simulation.json",
        "06_global_postprocessing_results/comb_vtu_dummy_global_quantities.png",
    ]

    ref_list = [comb_path + i_path for i_path in ref_path_list]

    assert dir_list == ref_list


def test_integration_split(run_in_docker, base_path, reference_files):
    """
    Runs main and tests the existence of all expected output files for the 'split_vtu' test case
    """

    split_path = os.path.join(reference_files, "split_vtu/")

    # run main
    main_path = os.path.join(base_path, "main.py")
    config_path = os.path.join(split_path, "00_config", "config.json")
    subprocess.run(["python", main_path, config_path])

    dir_list = [
        os.path.join(dir[0], file)
        for dir in os.walk(split_path)
        for file in os.listdir(dir[0])
    ]
    dir_list.sort()
    ref_path_list = [
        "00_config",
        "00_config/config.json",
        "01_input",
        "01_input/SMART_E2_mesh10.mesh",
        "01_input/dummy.eit",
        "01_input/realtimeValues_20210707094239.csv",
        "01_input/simulation_0002.1300.vtu",
        "01_input/simulation_0002.26.vtu",
        "01_input/simulation_0002.constant.0.vtu",
        "01_input/simulation_0002.pvd",
        "02_inhomogeneous_mesh",
        "02_inhomogeneous_mesh/electrode_order_frontal_view.png",
        "02_inhomogeneous_mesh/electrode_order_top_view.png",
        "02_inhomogeneous_mesh/lung_topology.json",
        "03_forward_results",
        "03_forward_results/split_vtu_dummy_elec_voltages.json",
        "04_simulated_reconstruction_results",
        "04_simulated_reconstruction_results/eit_reconstruction_data.npz",
        "04_simulated_reconstruction_results/split_vtu_dummy_EIT_image_ts1300.png",
        "04_simulated_reconstruction_results/split_vtu_dummy_EIT_image_ts26.png",
        "05_measured_reconstruction_results",
        "05_measured_reconstruction_results/detached_electrodes.png",
        "05_measured_reconstruction_results/eit_meas.json",
        "05_measured_reconstruction_results/eit_reconstruction_data.npz",
        "05_measured_reconstruction_results/meas_EIT_image_ts1300.png",
        "05_measured_reconstruction_results/meas_EIT_image_ts26.png",
        "06_global_postprocessing_results",
        "06_global_postprocessing_results/split_vtu_dummy_global_quant_simulation.json",
        "06_global_postprocessing_results/split_vtu_dummy_global_quantities.png",
    ]

    ref_list = [split_path + i_path for i_path in ref_path_list]

    assert dir_list == ref_list
