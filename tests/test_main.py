"""Tests for main module of holoshed."""
import json
from distutils.dir_util import copy_tree
from os.path import join as join
from tempfile import TemporaryDirectory

import pytest
from click.testing import CliRunner

import holoshed.utils
from main import main


@pytest.mark.usefixtures("reference_files")
def test_main(reference_files, mocker):
    """Test main function of holoshed."""
    runner = CliRunner()
    with TemporaryDirectory() as tmpdir:
        copy_tree(join(reference_files, "split_vtu"), tmpdir)
        config_path = join(tmpdir, "00_config", "config.json")
        with open(config_path) as ff:
            config = json.load(ff)
        config["working_path"] = holoshed.utils.get_working_path(config_path)

        mock_init_holoshed = mocker.patch(
            "main.initialize_holoshed", return_value=config
        )
        mock_setup_fmdl = mocker.patch(
            "main.setup_forward_model", return_value="dummy_fmdl"
        )
        mock_lungFusion = mocker.patch(
            "main.lungFusion",
            return_value=config,
        )
        mocker.patch("holoshed.plot_fe.plot_fe_surf")
        mock_makeGreit = mocker.patch(
            "main.makeGREIT", return_value=["dummy", "dummy", config]
        )
        mock_plot_eit = mocker.patch("holoshed.plot_eit.plot_eit")
        mock_plot_det_ele = mocker.patch(
            "holoshed.plot_global.plot_detached_electrodes"
        )
        mock_plot_global = mocker.patch("holoshed.plot_global.plot_global")
        result = runner.invoke(
            main, [join(tmpdir, "00_config", "config.json")]
        )
    assert result.exit_code == 0
    mock_init_holoshed.assert_called_once()
    mock_setup_fmdl.assert_called_once()
    mock_lungFusion.assert_called_once()
    mock_makeGreit.assert_called_once()
    assert mock_plot_eit.call_count == 2
    mock_plot_det_ele.assert_called_once()
    mock_plot_global.assert_called_once()
