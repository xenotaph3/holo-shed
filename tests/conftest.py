import os

import pytest


@pytest.fixture
def reference_files():
    """Returns absolute path to the reference files folder."""
    this_file_dir = os.path.dirname(os.path.abspath(__file__))
    return os.path.join(this_file_dir, "reference_files")


@pytest.fixture
def base_path(reference_files):
    """Returns absolute path to the holoshed folder."""
    return os.path.join(reference_files, "..", "..")
