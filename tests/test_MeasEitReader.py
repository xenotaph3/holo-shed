from os.path import join as join

import numpy as np
from oct2py import Struct, octave

import holoshed.MeasEitReader
import holoshed.utils


def test_constructor(mocker):
    """Testing the class constructor."""
    config = {"working_path": "/dummy/path"}
    mocker.patch("os.path.exists", return_value=False)
    mock_read_raw_eit = mocker.patch(
        "holoshed.MeasEitReader.MeasEitReader.read_raw_eit"
    )
    mocker.patch("builtins.open")
    mocker.patch("holoshed.MeasEitReader.JsonDataIO.write")
    mock_json_read = mocker.patch("holoshed.MeasEitReader.JsonDataIO.read")
    mock_det_detach_elec = mocker.patch(
        "holoshed.MeasEitReader.MeasEitReader.determine_detached_electrodes",
        return_value=[None, None, None],
    )
    mock_det_faulty_ch = mocker.patch(
        "holoshed.MeasEitReader.MeasEitReader.determine_faulty_channels",
        return_value=None,
    )

    reader = holoshed.MeasEitReader.MeasEitReader(config)
    reader.detached_electrodes
    reader.imped_thresh
    reader.high_imped_electrodes
    reader.faulty_ch

    mocker.patch("os.path.exists", return_value=True)

    holoshed.MeasEitReader.MeasEitReader(config)

    mock_read_raw_eit.assert_called_once()
    mock_json_read.assert_called_once()
    assert mock_det_detach_elec.call_count == 3
    mock_det_faulty_ch.assert_called_once()


def test_read_raw_eit(mocker):
    """Testing the read_raw_eit method for the case that the user defined timeslot
    lies completely within one eit-file."""
    config = setup_read_raw_eit_test_mock(
        mocker, ["/dummy/path/dummy1.eit"], [1, 3]
    )
    eit_reader = holoshed.MeasEitReader.MeasEitReader(config)
    eit_meas = eit_reader.read_raw_eit(config)
    assert np.allclose(
        eit_meas["vv"][:, 17],
        np.array((0.33498815 + 0.00334988j, 0.33498815 + 0.00334988j)),
    )
    assert np.allclose(eit_meas["relative_eit_time"], np.arange(0, 2, 0.1))
    assert eit_meas["reco_times"][1][1] == 4
    assert eit_meas["detached_electrodes"]() == [2, 5]
    assert eit_meas["imped_thresh"]() == [3, 6]
    assert eit_meas["electrode_imped"][0][0] == 0


def test_read_raw_eit_two_eit_necessary(mocker):
    """Testing the read_raw_eit method for the case that the user defined timeslot is split into two eit-files.
    This means the considered timeslot starts is in the first eit-file and ends in the consecutive.
    """
    config = setup_read_raw_eit_test_mock(
        mocker,
        [
            "/dummy/path/dummy1.eit",
            "/dummy/path/dummy2.eit",
            "/dummy/path/dummy3.eit",
        ],
        [3, 5],
    )
    eit_reader = holoshed.MeasEitReader.MeasEitReader(config)
    eit_meas = eit_reader.read_raw_eit(config)
    assert np.allclose(
        eit_meas["vv"][:, 17],
        np.array((-0.99616461 - 0.00996165j, -0.99616461 - 0.00996165j)),
    )
    assert np.allclose(eit_meas["relative_eit_time"], np.arange(0, 2, 0.1))
    assert eit_meas["reco_times"][1][1] == 4
    assert eit_meas["detached_electrodes"]() == [2, 5]
    assert eit_meas["imped_thresh"]() == [3, 6]
    assert eit_meas["electrode_imped"][0][0] == 0

    config["realtime_start"] = 6
    config["realtime_end"] = 10
    eit_reader = holoshed.MeasEitReader.MeasEitReader(config)
    eit_meas = eit_reader.read_raw_eit(config)
    assert np.allclose(
        eit_meas["vv"][:, 23],
        np.array((0.4921184 + 0.00492118j, 0.4921184 + 0.00492118j)),
    )
    assert np.allclose(
        eit_meas["relative_eit_time"][18:23],
        np.array([1.8, 1.9, 2.1, 2.3, 2.5]),
    )


def test_read_raw_eit_meas_outlier_exception(mocker):
    """Testing the read_raw_eit method for the case that an eit-file where initial timestep is much bigger than
    the normal sampling rate. This triggers the outlier removal and is necessary for SMART_E2/20210707094239.eit
    """
    config = setup_read_raw_eit_test_mock(
        mocker, ["/dummy/path/dummy_with_outlier.eit"], [1, 3]
    )
    eit_reader = holoshed.MeasEitReader.MeasEitReader(config)
    eit_meas = eit_reader.read_raw_eit(config)
    assert np.allclose(
        eit_meas["vv"][:, 17],
        np.array((-0.91616594 - 0.00916166j, -0.91616594 - 0.00916166j)),
    )
    assert np.allclose(eit_meas["relative_eit_time"], np.arange(0, 1.9, 0.1))
    assert eit_meas["reco_times"][1][1] == 4
    assert eit_meas["detached_electrodes"]() == [2, 5]
    assert eit_meas["imped_thresh"]() == [3, 6]
    assert eit_meas["electrode_imped"][0][0] == 0


def test_apply_meas_pattern_to_voltages(mocker):
    """Testing the apply_meas_pattern_to_voltages method."""

    eit_meas = {
        "vv": np.array(
            (
                (np.sin(np.linspace(4.1, 8, 40)) * complex(1, 0.01)),
                (np.sin(np.linspace(4.1, 8, 40)) * complex(1, 0.01)),
            )
        )
    }
    setup_constructor_mock(mocker, eit_meas)
    mocker.patch(
        "holoshed.lung_fusion.octave.mk_stim_patterns",
        return_value=["dummy_stim", np.array([[1], [0]])],
    )
    config = {
        "working_path": "/dummy/path",
        "num_electrodes": 32,
        "num_rings": 1,
        "inject_pattern": [0, 5],
        "meas_pattern": [0, 5],
        "meas_current": "no_meas_current_next2",
        "amp": 0.005,
    }
    eit_reader = holoshed.MeasEitReader.MeasEitReader(config)
    vv = eit_reader.apply_meas_pattern_to_voltages(eit_meas["vv"])
    assert np.allclose(vv[0, :], eit_meas["vv"][0, :])
    assert np.allclose(vv[1, :], np.zeros([40, 1]))


def test_get_normalized_mean_voltage(mocker):
    """Testing the get_normalized_mean voltage method."""
    eit_meas = {
        "vv": np.array(
            (
                (np.sin(np.linspace(4.1, 8, 40)) * complex(1, 0.01)),
                (np.sin(np.linspace(4.1, 8, 40)) * complex(1, 0.01)),
            )
        )
    }
    setup_constructor_mock(mocker, eit_meas)

    config = {
        "working_path": "/dummy/path",
    }
    eit_reader = holoshed.MeasEitReader.MeasEitReader(config)
    norm_volt = eit_reader.get_normalized_mean_voltage()
    assert np.allclose(
        norm_volt[0:6],
        np.array(
            [
                0.81523302,
                0.8694474,
                0.91480366,
                0.95084862,
                0.97722213,
                0.99366067,
            ]
        ),
    )


def test_filter_voltages(mocker):
    """Testing the filter_voltages method of the MeasEitReader class."""
    setup_constructor_mock(mocker, "dummy")
    config = {"working_path": "/dummy/path/"}
    filt_type = "lowpass"
    filt_order = 1
    filt_cutoff = 6.7
    t = np.linspace(0, 30, 1500)
    sig = np.sin(2 * np.pi * 0.5 * t) + np.sin(2 * np.pi * 10 * t)
    s1 = complex(1, 0.01) * sig
    s2 = complex(2, 0.03) * sig
    vv = np.array((s1, s2))
    reader = holoshed.MeasEitReader.MeasEitReader(config)
    vv_filt = reader.filter_voltages(vv, t, filt_type, filt_order, filt_cutoff)

    assert np.allclose(
        vv_filt[:, 1], [0.3240344 + 0.00324034j, 0.64806879 + 0.00972103j]
    )
    assert np.allclose(
        vv_filt[:, -2], [-0.32455613 - 0.00324556j, -0.64911226 - 0.00973668j]
    )


def test_get_eit_time(mocker):
    """Testing the get_eit_time method."""
    eit_meas = {"relative_eit_time": np.linspace(0, 4, 41)}
    setup_constructor_mock(mocker, eit_meas)
    config = {"working_path": "/dummy/path"}
    eit_reader = holoshed.MeasEitReader.MeasEitReader(config)
    assert np.allclose(eit_reader.get_eit_time(), np.linspace(0, 4, 41))


def test_get_reco_time_indices(mocker):
    """Testing the get_reco_time_indices method of the MeasEitReader class."""
    eit_meas = {"reco_times": ([10, 12], [3.4, 4.5])}
    setup_constructor_mock(mocker, eit_meas)
    config = {"working_path": "/dummy/path"}
    eit_reader = holoshed.MeasEitReader.MeasEitReader(config)
    ids = eit_reader.get_reco_time_indices()
    assert ids == [10, 12]


def test_get_volt_reconst_frames(mocker):
    """Testing the get_volt_reconst_frames voltage method."""
    eit_meas = {
        "vv": np.array(
            (
                (np.sin(np.linspace(4.1, 8, 40)) * complex(1, 0.01)),
                (np.sin(np.linspace(4.1, 8, 40)) * complex(1, 0.01)),
            )
        ),
        "reco_times": ([6, 12], [7.8, 9.3]),
    }
    mocker.patch(
        "holoshed.MeasEitReader.MeasEitReader.average_reference_voltage",
        side_effect=mock_average_reference_voltage,
    )
    setup_constructor_mock(mocker, eit_meas)
    config = {
        "working_path": "/dummy/path",
        "num_measurements_for_reference": 3,
        "reconst_timesteps": [2, 4],
        "reference_timestep": [2],
    }

    eit_reader = holoshed.MeasEitReader.MeasEitReader(config)
    vv = eit_reader.get_volt_reconst_frames()

    vv_ref = np.array(
        (
            (np.array((-0.99992326 - 0.00999923j, -0.83226744 - 0.00832267j))),
            (np.array((-0.99992326 - 0.00999923j, -0.83226744 - 0.00832267j))),
        )
    )
    assert np.allclose(vv, vv_ref)


def test_average_reference_voltage(mocker):
    """Testing the average_reference_voltage method of MeasEitReader."""
    eit_meas = {
        "vv": np.tile(
            np.array([1, 2, 3, 4, 2, 4, 3, 2, 1, 0], dtype=float)
            * complex(1, 0.01),
            (4, 1),
        ),
    }
    setup_constructor_mock(mocker, eit_meas)
    config = {
        "working_path": "/dummy/path",
    }
    num_meas_for_ref = 5
    eit_reader = holoshed.MeasEitReader.MeasEitReader(config)
    reconst_ids = [0, 4, 9]
    ref_index = [1]
    voltages = eit_meas["vv"][:, reconst_ids]
    mod_voltage = eit_reader.average_reference_voltage(
        voltages, reconst_ids, ref_index, num_meas_for_ref
    )
    assert mod_voltage[1, 1] == complex(3.2, 0.032)


def test_get_detached_electrodes(mocker):
    """Testing the get_detached_electrodes method."""
    eit_meas = {"detached_electrodes": [3, 7]}
    setup_constructor_mock(mocker, eit_meas)
    config = {"working_path": "/dummy/path"}
    eit_reader = holoshed.MeasEitReader.MeasEitReader(config)
    assert eit_reader.get_detached_electrodes() == [3, 7]


def test_determine_detached_electrodes(mocker):
    """Testing the determine_detached_electrodes method."""
    setup_constructor_mock(mocker, "dummy_eit_meas")
    config = {
        "working_path": "/dummy/path",
        "num_electrodes": 32,
        "meas_pattern": [0, 5],
        "electrode_detachment_controller": 8,
    }
    eit_reader = holoshed.MeasEitReader.MeasEitReader(config)
    np.random.seed(42)
    imped = np.random.normal(loc=1, scale=0.1, size=(32, 4))
    imped[(2, 17, 31), :] = np.random.normal(loc=3, scale=0.2, size=(1, 4))
    imped[(4, 7, 22), :] = np.random.normal(loc=4, scale=0.4, size=(1, 4))
    eit_reader.electrode_imped = imped
    (
        det_ele,
        thresh,
        high_imped_elec,
    ) = eit_reader.determine_detached_electrodes()
    assert det_ele == [4, 7, 22]
    assert np.allclose(
        thresh, np.array([1.521972688047363, 1.788875259901022])
    )
    assert np.all(high_imped_elec == [2, 4, 7, 17, 22, 31])


def test_determine_faulty_channels(mocker):
    """Testing the determine_faulty_channels method."""
    setup_constructor_mock(mocker, "dummy_eit_meas")
    config = {
        "working_path": "/dummy/path",
        "num_electrodes": 32,
    }
    eit_reader = holoshed.MeasEitReader.MeasEitReader(config)
    eit_reader._high_imped_electrodes = [7, 12]
    faulty_ch = eit_reader.determine_faulty_channels()

    assert np.all(faulty_ch[7 * 32 : 8 * 32])
    assert np.all(faulty_ch[12 * 32 : 13 * 32])
    assert np.all(faulty_ch[7:13] == [True, False, False, False, False, True])


def setup_constructor_mock(mocker, eit_meas):
    """Utility function setting up necessary mock patches to call the class constructor.
    Args:
        eit_meas    (dict):     Preprocessed measurement data.
    """
    base_path = holoshed.utils.repo_base_path()
    octave.addpath(join(base_path, "tests", "reference_files", "eidors_mock"))
    mocker.patch("os.path.exists", return_value=True)
    mocker.patch(
        "holoshed.MeasEitReader.JsonDataIO.read", return_value=eit_meas
    )
    mocker.patch("builtins.open")


def setup_read_raw_eit_test_mock(mocker, eit_meas_list, realtime_lims):
    """Utility function necessary for testing the read_raw_eit method. Mocks the call of several functions and
    sets up a config dictionary.
    Args:
        eit_meas_list   (list):     Paths to .eit files
        realtime_lims   (list):     Tuple with values for "realtime_strart" and "realtime_end"
    Returns:
        config          (dict):     Configuration suitable for testing the read_raw_eit.
    """
    setup_constructor_mock(mocker, "dummy_eit_meas")
    mocker.patch(
        "holoshed.MeasEitReader.get_time_mapping_indices", return_value=[0, 1]
    )
    config = {
        "working_path": "/dummy/path",
        "eit_meas_list": eit_meas_list,
        "glob_reco_times": np.array([1, 4]),
        "realtime_start": realtime_lims[0],
        "realtime_end": realtime_lims[1],
        "eit_realtime_offset": 0.0,
        "eidors_path": "/dummy/path",
        "filter_type": "lowpass",
        "filter_order": 1,
        "cutoff_frequency": 6.7,
        "prestress_time": 0.0,
    }
    mocker.patch(
        "holoshed.MeasEitReader.octave.eidors_readdata",
        side_effect=mock_readdata,
    )
    mocker.patch(
        "holoshed.MeasEitReader.MeasEitReader.detached_electrodes",
        return_value=[2, 5],
    )
    mocker.patch(
        "holoshed.MeasEitReader.MeasEitReader.imped_thresh",
        return_value=[3, 6],
    )
    mocker.patch(
        "holoshed.MeasEitReader.MeasEitReader.high_imped_electrodes",
        return_value=[2, 5, 29, 32],
    )
    mocker.patch(
        "holoshed.MeasEitReader.MeasEitReader.faulty_ch", return_value="dummy"
    )
    mocker.patch(
        "holoshed.MeasEitReader.MeasEitReader.filter_voltages",
        side_effect=mock_filter_voltages,
    )
    mocker.patch(
        "holoshed.MeasEitReader.MeasEitReader.apply_meas_pattern_to_voltages",
        side_effect=mock_apply_meas_pattern_to_voltages,
    )
    return config


def mock_average_reference_voltage(voltage, *_):
    """Function mocking the behavior of average_reference_voltage method. Doing nothing."""
    return voltage


def mock_readdata(path, *_, nout):
    """Mocks eidors_readdata function and returns dummy data."""
    nout
    if path == "/dummy/path/dummy1.eit":
        volt_raw = np.array(
            (
                (np.sin(np.linspace(0, 4, 41)) * complex(1, 0.01)),
                (np.sin(np.linspace(0, 4, 41)) * complex(1, 0.01)),
            )
        )
        t_abs = np.array((np.linspace(0, 4, 41) / 3600 / 24))
        elec_imped = np.zeros((2, 41))
    elif path == "/dummy/path/dummy2.eit":
        volt_raw = np.array(
            (
                (np.sin(np.linspace(4.1, 8, 40)) * complex(1, 0.01)),
                (np.sin(np.linspace(4.1, 8, 40)) * complex(1, 0.01)),
            )
        )
        t_abs = np.array((np.linspace(4.1, 8, 40) / 3600 / 24))
        elec_imped = np.zeros((2, 40))
    elif path == "/dummy/path/dummy3.eit":
        volt_raw = np.array(
            (
                (np.sin(np.linspace(8.1, 16, 40)) * complex(1, 0.01)),
                (np.sin(np.linspace(8.1, 16, 40)) * complex(1, 0.01)),
            )
        )
        t_abs = np.array((np.arange(8.1, 16, 0.2) / 3600 / 24))
        elec_imped = np.zeros((2, 40))
    elif path == "/dummy/path/dummy_with_outlier.eit":
        volt_raw = np.array(
            (
                (np.sin(np.linspace(1.5, 5, 36)) * complex(1, 0.01)),
                (np.sin(np.linspace(1.5, 5, 36)) * complex(1, 0.01)),
            )
        )
        t_abs = np.array((np.linspace(1.5, 5, 36) / 3600 / 24))
        volt_raw = np.insert(volt_raw, 0, np.array((0, 0)), 1)
        t_abs = np.insert(t_abs, 0, 0)
        elec_imped = np.zeros((2, 37))
    aux = Struct()
    aux.t_abs = np.array([t_abs])
    aux.elec_impedance = elec_imped
    return volt_raw, aux, None


def mock_filter_voltages(vv, *_):
    """Utility method mocking the behavior of the filter_voltages method. Returning the original voltage data."""
    return vv


def mock_apply_meas_pattern_to_voltages(vv, *_):
    """Utility method mocking the behavior of the apply_meas_pattern_to_voltages method. Returning the original voltage data."""
    return vv
