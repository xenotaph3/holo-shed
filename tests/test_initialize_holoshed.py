import json
import os
import shutil
from distutils.dir_util import copy_tree
from os.path import join as join
from tempfile import TemporaryDirectory

import numpy as np
import pytest
from oct2py import octave

import holoshed
import holoshed.initialize_holoshed
import holoshed.utils


def test_initialize_holoshed(mocker):
    """Testing the initialize_holoshed function."""
    mocker.patch("builtins.open")
    mock_check_config = mocker.patch(
        "holoshed.initialize_holoshed.HoloshedInitializer.check_config"
    )
    mock_init_folder = mocker.patch(
        "holoshed.initialize_holoshed.HoloshedInitializer.initialize_folder_structure"
    )
    mock_init_octave = mocker.patch(
        "holoshed.initialize_holoshed.HoloshedInitializer.initialize_octave"
    )
    mock_check_realtimeValues = mocker.patch(
        "holoshed.initialize_holoshed.HoloshedInitializer.check_for_valid_realtimeValues"
    )
    mock_check_eit = mocker.patch(
        "holoshed.initialize_holoshed.HoloshedInitializer.check_for_valid_eit_meas"
    )
    config_path = "/dummy/path/00_config/config.json"

    dummy_config = {
        "reconstruct_from_meas_data": True,
        "reconstruct_from_sim_data": True,
    }
    mocker.patch("json.load", return_value=dummy_config)
    config = holoshed.initialize_holoshed.initialize_holoshed(config_path)

    assert config["working_path"] == "/dummy/path"
    mock_check_config.assert_called_once()
    mock_init_folder.assert_called_once()
    mock_init_octave.assert_called_once()
    mock_check_realtimeValues.assert_called_once()
    mock_check_eit.assert_called_once()

    dummy_config = {
        "reconstruct_from_meas_data": True,
        "reconstruct_from_sim_data": False,
    }
    mocker.patch("json.load", return_value=dummy_config)
    mock_create_dummy_sim_time = mocker.patch(
        "holoshed.initialize_holoshed.HoloshedInitializer.create_dummy_simulation_time"
    )

    config = holoshed.initialize_holoshed.initialize_holoshed(config_path)

    mock_create_dummy_sim_time.assert_called_once()


def test_check_config(mocker):
    """Testing the check_config method of the HoloshedInitializer class."""
    mocker.patch("builtins.open")
    with pytest.raises(ValueError):
        run_check_config(mocker, {"filter_type": "dummy", "filter_order": 1})
    with pytest.raises(TypeError):
        run_check_config(
            mocker,
            {
                "filter_type": "lowpass",
                "cutoff_frequency": [1.3, 4.0],
                "filter_order": 1,
            },
        )
    with pytest.raises(RuntimeError):
        run_check_config(
            mocker,
            {
                "filter_type": "bandpass",
                "cutoff_frequency": 2.3,
                "filter_order": 1,
            },
        )
    with pytest.raises(TypeError):
        run_check_config(mocker, {"filter_order": 1.5})

    test_filter = {
        "filter_order": 1,
        "filter_type": "lowpass",
        "cutoff_frequency": 1.0,
    }
    with pytest.raises(ValueError):
        run_check_config(
            mocker,
            {
                **test_filter,
                "num_measurements_for_reference": 0,
            },
        )
    with pytest.raises(ValueError):
        run_check_config(
            mocker,
            {
                "filter_order": 1,
                "filter_type": "lowpass",
                "cutoff_frequency": 1.0,
                "num_measurements_for_reference": 21,
            },
        )
    with pytest.raises(ValueError):
        run_check_config(
            mocker,
            {
                **test_filter,
                "reconstruct_from_sim_data": False,
                "reconstruct_from_meas_data": False,
            },
        )

    mock_warning = mocker.patch("warnings.warn")
    run_check_config(
        mocker,
        {
            **test_filter,
            "reconstruct_from_sim_data": True,
            "reconstruct_from_meas_data": True,
            "meas_reconst_frequency": 1,
        },
    )
    mock_warning.assert_called_once()

    mock_warning = mocker.patch("warnings.warn")
    run_check_config(
        mocker,
        {
            **test_filter,
            "reconstruct_from_sim_data": True,
            "reconstruct_from_meas_data": True,
            "realtime_reference": 1,
        },
    )
    mock_warning.assert_called_once()

    with pytest.raises(RuntimeError):
        run_check_config(
            mocker,
            {
                **test_filter,
                "reconstruct_from_sim_data": False,
                "reconstruct_from_meas_data": True,
            },
        )

    mock_warning = mocker.patch("warnings.warn")
    run_check_config(
        mocker,
        {
            **test_filter,
            "reconstruct_from_sim_data": False,
            "reconstruct_from_meas_data": True,
            "realtime_reference": 1,
            "meas_reconst_frequency": 1,
            "realtime_reconst_times": [1, 2],
        },
    )
    mock_warning.assert_called_once()

    mock_warning = mocker.patch("warnings.warn")
    run_check_config(
        mocker,
        {
            **test_filter,
            "reconstruct_from_sim_data": True,
            "reconstruct_from_meas_data": True,
            "start_timestep": 1,
            "reconst_timesteps": [1, 2],
        },
    )
    mock_warning.assert_called_once()
    with pytest.raises(ValueError):
        run_check_config(
            mocker,
            {
                **test_filter,
                "reconstruct_from_sim_data": True,
                "reconstruct_from_meas_data": True,
                "reconst_timesteps": [3, 3, 2],
            },
        )
    with pytest.raises(ValueError):
        run_check_config(
            mocker,
            {
                **test_filter,
                "reconstruct_from_sim_data": False,
                "reconstruct_from_meas_data": True,
                "realtime_reference": 2,
                "realtime_reconst_times": [3, 3, 2],
            },
        )


def run_check_config(mocker, config):
    """Utility function running the config check."""
    config_path = "/dummy/path/00_config/config.json"
    mocker.patch("json.load", return_value=config)
    init = holoshed.initialize_holoshed.HoloshedInitializer(config_path)
    init.check_config()


def test_initialize_folder_structure(reference_files):
    """Test the initialize_folder_structure function of the initilize_holoshed module."""
    with TemporaryDirectory() as tmpdir:
        copy_tree(os.path.join(reference_files, "split_vtu"), tmpdir)
        config_path = os.path.join(tmpdir, "00_config", "config.json")
        with open(config_path) as ff:
            config = json.load(ff)
        config["working_path"] = tmpdir
        dirs = [
            "02_inhomogeneous_mesh",
            "03_forward_results",
            "04_simulated_reconstruction_results",
            "05_measured_reconstruction_results",
            "06_global_postprocessing_results",
        ]
        abs_dirs = [os.path.join(tmpdir, dir) for dir in dirs]

        meas_path = abs_dirs[3]
        os.makedirs(meas_path)
        with open(os.path.join(meas_path, "eit_meas.json"), "w") as ff:
            pass

        init = holoshed.initialize_holoshed.HoloshedInitializer(config_path)
        init.initialize_folder_structure()

        for dir in abs_dirs:
            assert os.path.exists(dir)
        assert not os.path.exists(os.path.join(meas_path, "eit_meas.json"))


def test_initialize_octave(mocker):
    """Test the initialize_octave method of the HoloshedInitializer class."""
    epath = "/opt/eidors"
    rpath = "/the/repo/path"
    base_path = holoshed.utils.repo_base_path()
    octave.addpath(join(base_path, "tests", "reference_files", "eidors_mock"))
    mock_oct_run = mocker.patch("holoshed.initialize_holoshed.octave.run")
    mock_oct_add = mocker.patch("holoshed.initialize_holoshed.octave.addpath")
    mocker.patch("holoshed.initialize_holoshed.octave.eidors_cache")
    mocker.patch("builtins.open")
    mocker.patch(
        "json.load",
        return_value={"eidors_path": epath, "eidors_cache_size": 2.0},
    )

    config_path = "/dummy/path/00_config/config.json"
    init = holoshed.initialize_holoshed.HoloshedInitializer(config_path)
    init.base_path = rpath
    init.initialize_octave()

    mock_oct_run.assert_called_once_with(join(epath, "startup.m"), nout=0)
    mock_oct_add.assert_has_calls(
        [
            mocker.call(join(rpath, "holoshed", "octave_scripts")),
            mocker.call(epath),
        ]
    )


def test_check_for_valid_realtime_values(mocker):
    """Testing the check_for_valid_realtime_values method of the HoloshedInitializer class."""
    with TemporaryDirectory() as tmpdir:
        open(join(tmpdir, "realtimeValues1.csv"), "a").close()
        open(join(tmpdir, "realtimeValues2.csv"), "a").close()

        config_path = join(tmpdir, "00_config", "config.json")
        input_path = join(tmpdir, "01_input")
        os.mkdir(input_path)

        mocker.patch("builtins.open")
        mocker.patch("json.load", return_value={})
        init = holoshed.initialize_holoshed.HoloshedInitializer(config_path)

        with pytest.raises(RuntimeError):
            init.check_for_valid_realtimeValues()

        shutil.move(join(tmpdir, "realtimeValues1.csv"), input_path)
        init.check_for_valid_realtimeValues()
        assert init.config["realtime_values_path"] == join(
            input_path, "realtimeValues1.csv"
        )

        shutil.move(join(tmpdir, "realtimeValues2.csv"), input_path)
        with pytest.raises(RuntimeError):
            init.check_for_valid_realtimeValues()


def test_check_for_valid_eit_meas(mocker):
    """Testing the check_for_valid_eit_meas method of the HoloshedInitializer class."""
    with TemporaryDirectory() as tmpdir:
        open(join(tmpdir, "dummy1.eit"), "a").close()
        open(join(tmpdir, "dummy2.eit"), "a").close()

        config_path = join(tmpdir, "00_config", "config.json")
        input_path = join(tmpdir, "01_input")
        os.mkdir(input_path)

        mocker.patch("builtins.open")
        mocker.patch("json.load", return_value={})
        init = holoshed.initialize_holoshed.HoloshedInitializer(config_path)
        with pytest.raises(RuntimeError):
            init.check_for_valid_eit_meas()

        shutil.move(join(tmpdir, "dummy1.eit"), input_path)
        shutil.move(join(tmpdir, "dummy2.eit"), input_path)
        init.check_for_valid_eit_meas()
        assert init.config["eit_meas_list"] == [
            join(input_path, "dummy1.eit"),
            join(input_path, "dummy2.eit"),
        ]


def test_create_dummy_simulation_time(mocker):
    """Testing the create_dummy_simulation_time method of the HoloshedInitializer class."""

    config_path = "/dummy/path/00_config/config.json"
    mocker.patch("builtins.open")
    config = {
        "realtime_start": 10.0,
        "realtime_end": 15.0,
        "realtime_reference": [12.0],
        "meas_reconst_frequency": 1,
    }
    mocker.patch("json.load", return_value=config)
    init = holoshed.initialize_holoshed.HoloshedInitializer(config_path)
    init.create_dummy_simulation_time()

    assert init.config["reconst_timesteps"][2] == 2
    assert np.isclose(init.config["glob_reco_times"][3], 3.0)
    assert init.config["num_pseudo_timesteps"] == 6
    assert init.config["reference_timestep"] == [2]
    assert np.isclose(init.config["prestress_time"], 0.0)

    config = {
        "realtime_start": 10.0,
        "realtime_end": 15.0,
        "realtime_reference": [11.0, 12.0],
        "realtime_reconst_times": [11.0, 11.5, 12.0, 14.5],
    }
    mocker.patch("json.load", return_value=config)

    init = holoshed.initialize_holoshed.HoloshedInitializer(config_path)
    init.create_dummy_simulation_time()

    assert np.isclose(init.config["glob_reco_times"][3], 4.5)
    assert init.config["reconst_timesteps"][2] == 200
    assert init.config["num_pseudo_timesteps"] == 500
    assert init.config["reference_timestep"] == [100, 200]
