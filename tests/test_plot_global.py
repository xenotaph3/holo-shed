import matplotlib.pyplot as plt
import numpy as np

import holoshed.plot_global as plot_global


def test_plot_global(mocker):
    """Test plot_global function of plot_global module."""
    mock_plot_global_sim = mocker.patch(
        "holoshed.plot_global.plot_global_sim_data"
    )
    mock_plot_global_meas = mocker.patch(
        "holoshed.plot_global.plot_global_meas_data"
    )
    mock_sort_legend = mocker.patch("holoshed.plot_global.sort_legend")
    mock_savefig = mocker.patch("matplotlib.pyplot.savefig")
    mocker.patch("matplotlib.pyplot.show")
    config = {
        "reconstruct_from_sim_data": True,
        "sim_name": "dummy_sim",
        "working_path": "/dummy/path",
    }

    plot_global.plot_global(config, True)
    mock_plot_global_sim.assert_called_once()
    mock_plot_global_meas.assert_called_once()
    mock_sort_legend.assert_called_once()
    assert mock_savefig.called_once()


def test_plot_global_sim_data(mocker):
    """Testing the plot_global_meas_data function of the plot_global module."""
    config = {
        "reference_timestep": [1],
        "all_vtus": {1: "dummy_1.vtu", 2: "dummy_2.vtu"},
        "glob_reco_times": np.array([1.00001, 2.999991]),
    }
    dummy_data = np.ones((3, 1))
    mocker.patch(
        "holoshed.plot_global.get_tracheal_quant",
        return_value=[dummy_data] * 3,
    )
    mocker.patch(
        "holoshed.plot_global.get_time_mapping_indices", return_value=[0, 2]
    )
    mocker.patch(
        "holoshed.plot_global.get_mean_voltage_sim", return_value=dummy_data
    )
    mock_plot_markers = mocker.patch("holoshed.plot_global.plot_markers")
    mock_plot_data = mocker.patch("matplotlib.pyplot.plot")
    plot_global.plot_global_sim_data(config)
    assert mock_plot_markers.call_count == 3
    assert mock_plot_data.call_count == 3


def test_plot_global_meas_data(mocker):
    """Testing the plot_global_meas_data function of the plot_global module."""
    config = {
        "reconstruct_from_meas_data": True,
        "reference_timestep": [1],
        "all_vtus": {1: "dummy_1.vtu", 2: "dummy_2.vtu"},
        "glob_reco_times": np.array([1.00001, 2.999991]),
        "prestress_time": 0.2,
        "reconst_timesteps": [0, 1],
        "working_path": "/dummy/path",
    }
    dummy_data = np.ones((3, 1))
    mocker.patch(
        "holoshed.plot_global.get_realtime_values",
        return_value=[dummy_data] * 3,
    )
    mocker.patch(
        "holoshed.plot_global.get_mean_voltage_meas",
        return_value=[dummy_data, dummy_data, [1, 2]],
    )
    mock_rt_map = mocker.patch(
        "holoshed.plot_global.map_sim_index_on_realtime_index",
        side_effect=mock_map_sim_index_on_realtime_index,
    )
    mock_plot_markers = mocker.patch("holoshed.plot_global.plot_markers")
    mock_plot_data = mocker.patch("matplotlib.pyplot.plot")

    plot_global.plot_global_meas_data(config)
    assert mock_rt_map.call_count == 2
    assert mock_plot_markers.call_count == 3
    assert mock_plot_data.call_count == 3

    config.pop("all_vtus")
    config["num_pseudo_timesteps"] = 3

    plot_global.plot_global_meas_data(config)
    assert mock_rt_map.call_count == 4
    assert mock_plot_markers.call_count == 6
    assert mock_plot_data.call_count == 6


def mock_map_sim_index_on_realtime_index(idx, *_):
    """Mocks map_sim_index_on_realtime_index function and returns dummy data."""
    if isinstance(idx, int):
        return 1
    elif isinstance(idx, list):
        return np.array([0, 1])


def test_get_tracheal_quant(mocker):
    """Test get_tracheal_quant function of plot_global module."""
    config = {
        "working_path": "/dummy/path",
        "vtu_split_flag": False,
        "start_volume_sim": 0,
        "sim_name": "dummy",
    }
    mocker.patch("builtins.open")
    dummy_dat = np.arange(3)
    mocker.patch(
        "holoshed.plot_global.JsonDataIO.read",
        return_value={
            "sim_time_s": dummy_dat,
            "lung_vol_l": dummy_dat + 1,
            "trach_pres_mBar": dummy_dat + 2,
        },
    )
    time, volume, pres = plot_global.get_tracheal_quant(config)
    assert np.allclose(time, dummy_dat)
    assert np.allclose(volume, dummy_dat + 1)
    assert np.allclose(pres, dummy_dat + 2)


def test_get_realtime_values(mocker):
    """Test get_realtime_values function of plot_global module."""
    mocker.patch(
        "holoshed.plot_global.RealtimeReader",
        return_value=DummyRealtimeReader(),
    )
    time, pres, vol = plot_global.get_realtime_values("dummy_config")

    assert np.allclose(time, np.arange(3))
    assert np.allclose(pres, np.arange(3) + 1)
    assert np.allclose(vol, np.arange(3) + 2)


def test_get_mean_voltage_sim(mocker):
    """Testing the get_mean_voltage_sim function of plot_global module."""
    mocker.patch("builtins.open")
    mocker.patch(
        "holoshed.plot_global.JsonDataIO.read",
        return_value={"volt_data": np.array(((1, 2, 3), (1, 2, 3)))},
    )
    config = {"working_path": "/dummy/path", "sim_name": "dummy_sim"}
    mean_volt = plot_global.get_mean_voltage_sim(config)
    assert np.all(mean_volt == [0.0, 0.5, 1.0])


def test_get_mean_voltage_meas(mocker):
    """Testing the get_mean_voltage_meas function of the plot_global module."""
    mocker.patch(
        "holoshed.plot_global.MeasEitReader", return_value=DummyMeasEitReader()
    )
    volt, time, ids = plot_global.get_mean_voltage_meas("dummy_config")
    assert np.allclose(volt, 2 * np.arange(3))
    assert np.allclose(time, np.arange(3))
    assert ids == [0, 2]


def test_plot_markers(mocker):
    """Testing the plot_markers function of the plot_global module."""
    mock_plot = mocker.patch("matplotlib.pyplot.plot")
    data = np.ones((4, 1))
    reco_ids = [1, 3]
    idx_ref = 1
    plot_global.plot_markers(data, data, reco_ids, idx_ref)
    assert mock_plot.call_count == 4


def test_map_sim_index_on_realtime_index():
    """Testing the map_sim_index_on_realtime_index function of the plot_global module."""
    rt_idx = plot_global.map_sim_index_on_realtime_index(
        1, [0, 1, 2, 3], np.arange(0, 10, 0.1), 2
    )
    assert rt_idx == 40


def test_sort_legend():
    """Testing the sort_legend function of the plot_global module."""
    labels = [
        "Reference reconstruction",
        "Simulation",
        "Reconstruction samples",
        "End reconstruction",
        "Start reconstruction",
        "Reconstruction samples",
    ]
    fig = plt.figure(figsize=(16, 9))
    plt.subplot(1, 1, 1)
    for lab in labels:
        plt.plot(1, 1, label=lab)
    labels = plot_global.sort_legend(fig)
    labels_ref = [
        "Simulation",
        "Start reconstruction",
        "End reconstruction",
        "Reference reconstruction",
        "Reconstruction samples",
    ]

    assert labels == labels_ref


def test_plot_detached_electrodes(mocker):
    config = {
        "num_electrodes": 32,
        "meas_pattern": [0, 5],
        "working_path": "/dummy/path",
    }
    mocker.patch(
        "holoshed.plot_global.MeasEitReader",
        return_value=DummyMeasEitReader(),
    )
    mocker.patch("matplotlib.pyplot.show")
    mock_bar_plot = mocker.patch("matplotlib.axes.Axes.bar")
    mock_savefig = mocker.patch("matplotlib.figure.Figure.savefig")
    plot_global.plot_detached_electrodes(config, True)

    assert mock_bar_plot.call_count == 3
    mock_savefig.assert_called_once


class DummyRealtimeReader:
    """Dummy class mocking the behavior of the RealtimeReader."""

    def __init__(self):
        pass

    def get_relative_time(self):
        return np.arange(3)

    def get_pressure_mbar(self):
        return np.arange(3) + 1

    def get_volume_l(self):
        return np.arange(3) + 2


class DummyMeasEitReader:
    """Dummy class mocking the behavior of the MeasEitReader."""

    def __init__(self):
        self.eit_meas = {
            "electrode_imped": np.ones([32, 1]),
            "imped_thresh": [1, 2],
            "detached_electrodes": [2, 7, 30],
        }

    def get_normalized_mean_voltage(self):
        return 2 * np.arange(3)

    def get_eit_time(self):
        return np.arange(3)

    def get_reco_time_indices(self):
        return [0, 2]
