import numpy as np
import pytest

import holoshed.utils


def test_get_working_path():
    """Tests funcion get_working_path which is to return the folder two levels above."""
    dummy_config = "/my/dummy/path/00_config/config.json"
    assert holoshed.utils.get_working_path(dummy_config) == "/my/dummy/path"


def test_get_element_center_coordinates():
    """Tests function get_element_center_coordinates for two simple line elements."""
    dummy_ele = np.array([[0, 1], [1, 2]])
    dummy_nod = np.array([[0, 0, 0], [1, 1, 1], [1, 1, 0]])
    dummy_cent = holoshed.utils.get_element_center_coordinates(
        dummy_ele, dummy_nod
    )

    assert np.array_equal(dummy_cent, np.array([[0.5, 0.5, 0.5], [1, 1, 0.5]]))


def test_get_time_mapping_indices():
    """Tests the function get_time_mapping_indices with two simple vectors."""
    target = np.arange(1, 5)
    sample = np.array((2.05, 3.94))
    ids = holoshed.utils.get_time_mapping_indices(sample, target)
    assert ids == [1, 3]


def test_get_intersection_line():
    """Testing the get_intersection_line function."""
    nodes = np.array(
        [[1, 0, 1], [1, 0, -1], [-1, 0, 1], [-1, 2, -1]], dtype=float
    )
    topo = np.array([[0, 1, 2], [1, 2, 3]])
    # Create the nodes
    nodes = np.array([[0, 0, -1], [1, 0, 1], [0, 1, 1], [0, -1, 1]])

    # Create the topology
    topo = np.array([[0, 1, 2], [0, 1, 3], [0, 2, 3], [1, 2, 3]])

    line = holoshed.utils.get_intersection_line(nodes, topo, level=0)
    assert np.allclose(
        line,
        np.array(
            [
                [0.5, 0.0, 0.0],
                [0.0, 0.5, 0.0],
                [0.0, -0.0, 0.0],
                [0.0, -0.5, 0.0],
                [0.25, -0.25, 0.0],
                [0.5, 0.0, 0.0],
            ]
        ),
    )


def test_sort_vertices():
    """Tests the sort_vertices function for a closed curve with 5 vertices."""
    ele = np.array([[1, 2], [3, 0], [2, 4], [0, 1], [4, 3]])
    vert = np.array(
        [[0, 0, 0], [1, 0, 0], [1, 1, 0], [0, 1, 0], [0.5, 0.5, 0]]
    )

    sorted_verts_consecutive_order = holoshed.utils.sort_vertices(ele, vert)

    expected_result = np.array(
        [[0, 0, 0], [1, 0, 0], [1, 1, 0], [0.5, 0.5, 0], [0, 1, 0], [0, 0, 0]]
    )

    assert np.array_equal(sorted_verts_consecutive_order, expected_result)

    with pytest.raises(ValueError):
        ele = ele[:-1]
        sorted_verts_consecutive_order = holoshed.utils.sort_vertices(
            ele, vert
        )


def test_ismember_row():
    a = np.array([[1, 2], [7, 8], [3, 4], [9, 10], [5, 6]])
    b = np.array([[3, 4], [5, 6], [1, 2]])

    result = holoshed.utils.ismember_row(a, b)

    expected_result = np.array([True, False, True, False, True])
    assert np.array_equal(result, expected_result)


def test_tet():

    np.random.seed(42)

    for n in range(10):

        coords = np.random.normal(0, 1, (4, 3))
        tet = holoshed.utils.Tet(coords)
        center = np.mean(coords, 0)

        for m in range(4):
            diff = coords[m] - center
            alpha = np.sort(np.random.uniform(0, 1.5, 50))
            inside_ref = alpha < 1.0
            check_points = center.reshape(3, 1) + alpha.reshape(
                1, -1
            ) * diff.reshape(3, 1)
            inside = tet.points_inside(check_points.T)

            assert np.all(inside == inside_ref)
