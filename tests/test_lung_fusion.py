import json
import os
from distutils.dir_util import copy_tree
from os.path import join as join
from tempfile import TemporaryDirectory

import numpy as np
import pytest
import SimpleITK as sitk
from oct2py import octave
from scipy import spatial

import holoshed.JsonDataIO
import holoshed.lung_fusion as lung_fusion
import holoshed.utils
import holoshed.VTUReader as VTUReader


def test_lungFusion(reference_files, mocker):
    """Test covering the lungFusion function"""
    mocker.patch("holoshed.lung_fusion.octave.run")
    mock_check_valid_vtu = mocker.patch(
        "holoshed.lung_fusion.Model.check_for_valid_vtu"
    )
    mock_read_glob_quant_vtu = mocker.patch(
        "holoshed.lung_fusion.Model.read_global_quantities_from_sim_vtu"
    )
    mock_def_inhom_cond = mocker.patch(
        "holoshed.lung_fusion.Model.define_inhomogeneous_conductivities"
    )
    mock_write_conductivity = mocker.patch(
        "holoshed.lung_fusion.Model.write_conductivity"
    )
    mock_solve_inhom_fwd = mocker.patch(
        "holoshed.lung_fusion.Model.solve_inhom_forward_problem"
    )
    mock_save_sim_volt = mocker.patch(
        "holoshed.lung_fusion.Model.save_simulated_voltages"
    )
    with TemporaryDirectory() as tmpdir:
        copy_tree(join(reference_files, "split_vtu"), tmpdir)

        config = initialize_config(join(tmpdir, "00_config", "config.json"))

        lung_fusion.lungFusion(config, "dummy_fmdl", write_conductivity=True)

        mock_check_valid_vtu.assert_called_once()
        mock_read_glob_quant_vtu.assert_called_once()
        mock_def_inhom_cond.assert_called_once()
        mock_solve_inhom_fwd.assert_called_once()
        mock_save_sim_volt.assert_called_once()
        mock_write_conductivity.assert_called_once()


def test_check_for_valid_vtu_runtime_error_too_few_vtus(mocker):
    """Test check_for_valid_vtu method of Model class in case of too few vtu-files declared in config."""
    with TemporaryDirectory() as tmpdir:
        config = {
            "start_timestep": 201,
            "end_timestep": 201,
            "timestep_sampling": 1,
            "working_path": tmpdir,
            "eidors_path": "/dummy/path",
        }
        input_path = join(tmpdir, "01_input")
        os.mkdir(input_path)
        dummy_vtu = "my_vtu-201.vtu"
        vtu_path = join(input_path, dummy_vtu)
        open(vtu_path, "a").close()

        m = lung_fusion.Model(config, "dummy_fmdl")
        with pytest.raises(RuntimeError):
            m.check_for_valid_vtu()


def test_check_for_valid_vtu_runtime_two_const_vtu(mocker):
    """Test check_for_valid_vtu method of Model class in the case of two static/constant VTUs"""
    with TemporaryDirectory() as tmpdir:
        config = {
            "working_path": tmpdir,
            "eidors_path": "/dummy/path",
        }
        input_path = join(tmpdir, "01_input")
        os.mkdir(input_path)
        for i in range(2):
            dummy_vtu = "my_vtu" + str(i) + "constant.0.vtu"
            vtu_path = join(input_path, dummy_vtu)
            open(vtu_path, "a").close()
        m = lung_fusion.Model(config, "dummy_fmdl")
        with pytest.raises(RuntimeError):
            m.check_for_valid_vtu()


def test_read_global_quantities_from_sim_vtu(mocker):
    """Testing the read_global_quantities_from_sim_vtu method."""
    config = {
        "working_path": "/dummy/path/",
        "realtime_start": 400.0,
        "realtime_end": 402.0,
        "start_volume_sim": 2,
        "sim_name": "dummy",
    }
    mocker.patch(
        "holoshed.lung_fusion.VTUReader.create", side_effect=DummyVTUReader
    )
    pvd_mock = mocker.MagicMock()
    pvd_mock.get_single_vtu_time = mocker.MagicMock(
        side_effect=(1.0, 1.0, 2.0, 3.0)
    )
    mocker.patch("holoshed.lung_fusion.PvdIO.from_file", return_value=pvd_mock)

    m = lung_fusion.Model(config, "dummy_fmdl")
    m.all_vtus = {
        0: "/dummy/path/dummy1.vtu",
        1: "/dummy/path/dummy2.vtu",
        2: "dummy/path/dummy3.vtu",
    }
    m.static_vtu_path = "/dummy/path/static.vtu"
    with TemporaryDirectory() as tmpdir:
        glob_quant_json_path = os.path.join(tmpdir, "tmp.json")
        mocker.patch(
            "holoshed.lung_fusion.join", return_value=glob_quant_json_path
        )
        m.read_global_quantities_from_sim_vtu()

        json_pars = holoshed.JsonDataIO.JsonDataIO()
        glob_quant = json_pars.read(glob_quant_json_path)

        assert np.allclose(glob_quant["sim_time_s"], np.array([0.0, 1.0, 2.0]))
        assert np.allclose(
            glob_quant["trach_pres_mBar"], np.array([0.03, 0.06, 0.09])
        )
        assert np.allclose(glob_quant["lung_vol_l"], np.array([2.0, 3.5, 6.0]))
        assert np.allclose(m.sim_duration_tot, 2.0)


def test_check_for_valid_vtu_combined():
    """Test check_for_valid_vtu method of Model class in regular case for combined VTUs."""
    with TemporaryDirectory() as tmpdir:
        config = {
            "start_timestep": 201,
            "end_timestep": 203,
            "timestep_sampling": 2,
            "reference_timestep": [201],
            "working_path": tmpdir,
            "eidors_path": "/dummy/path",
        }
        input_path = join(tmpdir, "01_input")
        os.mkdir(input_path)
        for i in range(201, 204):
            dummy_vtu = "my_vtu-" + str(i) + ".vtu"
            vtu_path = join(input_path, dummy_vtu)
            open(vtu_path, "a").close()

        with pytest.raises(RuntimeError):
            m = lung_fusion.Model(config, "dummy_fmdl")
            m.check_for_valid_vtu()

        open(join(input_path, "dummy.pvd"), "a").close()
        m = lung_fusion.Model(config, "dummy_fmdl")
        m.check_for_valid_vtu()
        assert m.vtu_list == (
            join(input_path, "my_vtu-201.vtu"),
            join(input_path, "my_vtu-203.vtu"),
        )
        assert not m.vtu_split_flag

        config["reconst_timesteps"] = [201, 203]
        m = lung_fusion.Model(config, "dummy_fmdl")
        m.check_for_valid_vtu()
        assert m.vtu_list == [
            join(input_path, "my_vtu-201.vtu"),
            join(input_path, "my_vtu-203.vtu"),
        ]

        open(join(input_path, "dummy_dupl.pvd"), "a").close()
        with pytest.raises(RuntimeError):
            m.check_for_valid_vtu()


def test_check_for_valid_vtu_split():
    """Test check_for_valid_vtu method of Model class in regular case for split VTUs."""
    with TemporaryDirectory() as tmpdir:
        config = {
            "start_timestep": 201,
            "end_timestep": 203,
            "timestep_sampling": 2,
            "reference_timestep": [201],
            "working_path": tmpdir,
            "eidors_path": "/dummy/path",
        }
        input_path = join(tmpdir, "01_input")
        os.mkdir(input_path)
        open(join(input_path, "my_vtu.constant.0.vtu"), "a").close()
        open(join(input_path, "dummy.pvd"), "a").close()
        for i in range(201, 204):
            dummy_vtu = "my_vtu_0_" + str(i) + ".vtu"
            vtu_path = join(input_path, dummy_vtu)
            open(vtu_path, "a").close()
        m = lung_fusion.Model(config, "dummy_fmdl")
        m.check_for_valid_vtu()
        assert m.vtu_list == (
            join(input_path, "my_vtu_0_201.vtu"),
            join(input_path, "my_vtu_0_203.vtu"),
        )
        assert m.vtu_split_flag


def test_volumetric_ac_tet_overlap_responsibilities(mocker):
    "Test volumetric overlap approach to map ac to tets of model class (for combined VTUs)."
    config = {
        "working_path": "/dummy/path",
        "eidors_path": "/dummy/path",
        "vtu_split_flag": True,
    }
    mocker.patch("meshio.read")
    mocker.patch("holoshed.VTUReader.VTUReader.get_ac_nodes_and_indices")
    m = lung_fusion.Model(config, "dummy_fmdl")
    m.fmdl = octave.struct(
        "stimulation", "", "meas_select", "", "mat_idx", octave.cell(3, 1)
    )

    m.static_vtu = VTUReader.VTUReader.create(config, "/dummy/path/dummy.vtu")
    m.fmdl.lung_IDs = np.array((0, 1))
    m.fmdl.elems = np.array(((1, 2, 3, 4),))
    m.fmdl.nodes = np.array(
        ((0, 0, 0), (1, 0, 0), (0, 1, 0), (0, 0, 1), (1, 1, 1))
    )
    m.static_vtu._ac_nodes = np.array(
        (
            (3, 3, 3),
            (0.1, 0.1, 0.1),
            (0.5, 0.5, 0.5),
        )
    )
    m.static_vtu._ac_indices = np.array((1, 2, 3))
    m.static_vtu.num_ac = 3

    # self.static_vtu.vtu.get_cell_data("ele_id", "line")
    def side_effect(*_):
        return np.array([2, 1, 0, 3])

    m.static_vtu.vtu.get_cell_data.side_effect = side_effect

    def _create_img(img_np: np.ndarray):
        ac_img = sitk.GetImageFromArray(img_np.astype(np.int64))
        ac_img.SetOrigin((-0.1, -0.1, -0.1))
        ac_img.SetSpacing((0.1, 0.1, 0.1))
        return ac_img

    nodes = m.fmdl.nodes
    elements = m.fmdl.elems
    tree = spatial.KDTree(m.static_vtu.ac_nodes)

    # volumetric fraction of the tet given by a single voxel
    voxelfraction = 0.0025974025974025974
    num_voxels_in_tet = int(1 / voxelfraction)

    # Case 1: only background voxels, nearest neighbor fall back
    ac_img = np.zeros((12, 12, 12))
    R = m._volumetric_ac_tet_overlap_responsibilities(
        nodes, elements, _create_img(ac_img), tree
    )
    # closest centre of gravity (0.5, 0.5 0.5) is AC 3, which is global element 4
    assert np.allclose(R.flatten(), np.array([0.0, 0.0, 0.0, 1.0]))

    # Case 2: all identical voxels
    for ac_id in [1, 2, 3]:
        ac_img = ac_id * np.ones((12, 12, 12))
        R = m._volumetric_ac_tet_overlap_responsibilities(
            nodes, elements, _create_img(ac_img), tree
        )
        r = np.zeros(4)
        r[side_effect() == ac_id] = 1.0
        assert np.allclose(R.flatten(), r)

    # Case 3: identical voxel or background
    for ac_id in [1, 2, 3]:
        ac_img = ac_id * np.ones((12, 12, 12))
        ac_img[6, 9, 4] = 0
        ac_img[4, 7, 5] = 0
        R = m._volumetric_ac_tet_overlap_responsibilities(
            nodes, elements, _create_img(ac_img), tree
        )
        r = np.zeros(4)
        r[side_effect() == ac_id] = 1.0
        assert np.allclose(R.flatten(), r)

    # Case 4: different AC mask voxels present
    ac_img = np.ones((12, 12, 12))
    ac_img[6, 9, 4] = 2
    ac_img[4, 7, 5] = 3
    R = m._volumetric_ac_tet_overlap_responsibilities(
        nodes, elements, _create_img(ac_img), tree
    )
    r = np.array([voxelfraction, 1 - 2 * voxelfraction, 0.0, voxelfraction])
    assert np.allclose(R.flatten(), r)

    # Case 5: different AC mask voxels present and non-assigned voxel
    ac_img = np.ones((12, 12, 12))
    ac_img[6, 9, 4] = 2
    ac_img[4, 7, 5] = 0
    R = m._volumetric_ac_tet_overlap_responsibilities(
        nodes, elements, _create_img(ac_img), tree
    )
    adj_voxelfraction = 1 / (num_voxels_in_tet - 1)
    r = np.array([adj_voxelfraction, 1 - adj_voxelfraction, 0.0, 0.0])
    assert np.allclose(R.flatten(), r)


def test_map_ac_to_tetrahedra_combined(mocker):
    "Test map_ac_to_tetrahedra method of Model class for combined VTUs."
    config = {
        "working_path": "/dummy/path",
        "eidors_path": "/dummy/path",
        "vtu_split_flag": True,
    }
    mocker.patch("meshio.read")
    mocker.patch("holoshed.VTUReader.VTUReader.get_ac_nodes_and_indices")
    m = lung_fusion.Model(config, "dummy_fmdl")
    m.fmdl = octave.struct(
        "stimulation", "", "meas_select", "", "mat_idx", octave.cell(3, 1)
    )

    m.static_vtu = VTUReader.VTUReader.create(config, "/dummy/path/dummy.vtu")
    m.fmdl.lung_IDs = np.array((0, 1))
    m.fmdl.elems = np.array(((1, 2, 3, 4), (2, 3, 4, 5)))
    m.fmdl.nodes = np.array(
        ((0, 0, 0), (1, 0, 0), (0, 1, 0), (0, 0, 1), (1, 1, 1))
    )
    m.static_vtu._ac_nodes = np.array(
        (
            (3, 3, 3),
            (0.1, 0.1, 0.1),
            (0.7, 0.7, 0.7),
        )
    )
    m.static_vtu._ac_indices = np.array((1, 2, 3))
    m.static_vtu.num_ac = 3

    m.map_ac_to_tetrahedra()

    assert np.array_equal(m.ac_mapping, np.array((2, 3)))


def test_map_ac_to_tetrahedra_split(mocker):
    "Test map_ac_to_tetrahedra method of Model class for split VTUs."
    config = {
        "working_path": "/dummy/path",
        "eidors_path": "/dummy/path",
        "vtu_split_flag": True,
    }
    mocker.patch("meshio.read")
    mocker.patch("holoshed.VTUReader.VTUReader.get_ac_nodes_and_indices")
    m = lung_fusion.Model(config, "dummy_fmdl")
    m.fmdl = octave.struct("stimulation", "")

    m.static_vtu = VTUReader.VTUReader.create(config, "/dummy/path/dummy.vtu")
    m.fmdl.lung_IDs = np.array((0, 1))
    m.fmdl.elems = np.array(((1, 2, 3, 4), (2, 3, 4, 5)))
    m.fmdl.nodes = np.array(
        ((0, 0, 0), (1, 0, 0), (0, 1, 0), (0, 0, 1), (1, 1, 1))
    )
    m.vtu_split_flag = True
    m.static_vtu._ac_nodes = np.array(
        (
            (3, 3, 3),
            (0.1, 0.1, 0.1),
            (0.7, 0.7, 0.7),
        )
    )
    m.static_vtu._ac_indices = np.array((1, 2, 3))
    m.static_vtu.num_ac = 3

    m.map_ac_to_tetrahedra()
    assert np.all(m.ac_mapping == np.array((2, 3)))


def test_map_vol_strain_to_conduct(mocker):
    """Test map_vol_strain_to_conduct method of Model class."""
    config = {
        "working_path": "/dummy/path",
        "eidors_path": "/dummy/path",
        "vtu_split_flag": True,
    }
    base_path = holoshed.utils.repo_base_path()
    octave.addpath(join(base_path, "tests", "reference_files", "eidors_mock"))
    mocker.patch("meshio.read")
    mocker.patch(
        "holoshed.VTUReader.VTUReaderSplit.get_filling_factor",
        return_value=np.array((3, 5)).reshape(2, 1),
    )
    mocker.patch("holoshed.lung_fusion.octave.physics_data_mapper")

    img_dummy = octave.struct(
        "type",
        "image",
        "conductivity",
        octave.struct("elem_data", np.ones((7, 1))),
    )
    img_dummy.lung_IDs = [3, 4]

    m = lung_fusion.Model(config, "dummy_fmdl")
    m.dynamic_vtu = VTUReader.VTUReader.create(config, "/dummy/path/dummy.vtu")
    m.static_vtu = VTUReader.VTUReader.create(config, "/dummy/path/dummy.vtu")
    m.all_img_i = [img_dummy]
    m.reconst_timesteps = [1]
    m.static_vtu.ac_mapping = [0, 1]

    conduct = m.map_vol_strain_to_conduct(0)

    assert np.allclose(
        conduct,
        np.array(((1.06491228e-04, 7.09941520e-05))).reshape(2, 1),
        1e-7,
    )


def test_define_inhomogeneous_conductivities(reference_files, mocker):
    """Test define_inhomogeneous_conductivities method of Model class"""
    config = {
        "working_path": "/dummy/path",
        "eidors_path": "/dummy/path",
        "conductivity_thorax": 1,
        "conductivity_other_tissues": [5],
    }
    base_path = holoshed.utils.repo_base_path()
    img_dummy = octave.struct(
        "type",
        "image",
        "conductivity",
        octave.struct("elem_data", np.ones((7, 1))),
    )
    octave.addpath(join(base_path, "tests", "reference_files", "eidors_mock"))

    mocker.patch("holoshed.lung_fusion.octave.run")
    mocker.patch("meshio.read")
    mock_oct_mk_img = mocker.patch(
        "holoshed.lung_fusion.octave.mk_image", return_value=img_dummy
    )
    mock_get_ac = mocker.patch("holoshed.VTUReader.VTUReaderSplit.get_ac")
    mock_map_ac_to_tet = mocker.patch(
        "holoshed.lung_fusion.Model.map_ac_to_tetrahedra"
    )
    mock_map_strain_to_cond = mocker.patch(
        "holoshed.lung_fusion.Model.map_vol_strain_to_conduct"
    )
    mock_pvdio = mocker.patch("holoshed.lung_fusion.PvdIO.from_file")

    fmdl = octave.struct(
        "stimulation",
        "",
        "meas_select",
        "",
        "lungIDs",
        "dummyIDs",
        "tiss_IDs",
        "dummy",
    )
    fmdl.tiss_IDs = [np.array((3))]
    m = lung_fusion.Model(config, fmdl)

    m.vtu_split_flag = True
    m.static_vtu = "/dummy/path/dummy.vtu"
    m.vtu_list = ["/dummy/path/dummy_1.vtu", "/dummy/path/dummy_2.vtu"]
    m.all_vtus = {0: "/dummy/path/dummy_1.vtu", 1: "/dummy/path/dummy_2.vtu"}

    m.define_inhomogeneous_conductivities()

    assert mock_oct_mk_img.called_once()
    assert mock_get_ac.called_once()
    assert mock_map_ac_to_tet.called_once()
    assert mock_map_strain_to_cond.call_count == 2
    assert mock_pvdio.call_count == 1
    assert m.all_img_i[0].conductivity.elem_data[3] == 5


def test_write_conductivity(tmpdir, mocker):
    """Test the write_conductivity method of the lung_fusion class."""
    # Create a temporary directory for testing
    working_path = tmpdir.mkdir("working_path")

    fmdl = octave.struct(
        "nodes",
        np.array([[0, 0, 0], [1, 0, 0], [0, 1, 0], [0, 0, 1], [1, 1, 1]]),
        "elems",
        np.array([[1, 2, 3, 4], [2, 3, 4, 5]], dtype=float),
    )
    img_template = octave.struct(elem_data=np.array([[1], [2]]))
    all_img_i = [img_template] * 2

    meshio_mock = mocker.MagicMock()
    mocker.patch("holoshed.lung_fusion.meshio.Mesh", return_value=meshio_mock)
    mock_vtu_write = mocker.patch("holoshed.lung_fusion.meshio.write")

    pvd_mock = mocker.MagicMock()
    mocker.patch("holoshed.lung_fusion.PvdIO", return_value=pvd_mock)
    pvd_mock.create_data_collection = mocker.MagicMock()
    pvd_mock.write = mocker.MagicMock()

    config = {
        "working_path": working_path,
        "sim_name": "dummy_sim",
        "reconst_timesteps": [0, 1],
    }
    m = lung_fusion.Model(config, "dummy_fmdl")
    m.all_img_i = all_img_i
    m.fmdl = fmdl
    m.glob_reco_times = np.array([0, 1])
    m.write_conductivity()

    assert mock_vtu_write.call_count == 2
    assert (
        "working_path/02_inhomogeneous_mesh/dummy_sim_cond_heterogeneous.pvd"
        in pvd_mock.mock_calls[1][1][0]
    )


def test_solve_inhom_forward_problem(reference_files, mocker):
    """Test solve_inhom_forward_model method of Model class"""

    base_path = holoshed.utils.repo_base_path()
    octave.addpath(join(base_path, "tests", "reference_files", "eidors_mock"))
    octave.addpath(join(base_path, "holoshed", "octave_scripts"))

    mocker.patch("holoshed.lung_fusion.octave.run")
    mock_oct_ens_write = mocker.patch(
        "holoshed.lung_fusion.octave.EnsightWrite"
    )

    with TemporaryDirectory() as tmpdir:
        copy_tree(join(reference_files, "split_vtu"), tmpdir)
        config = initialize_config(join(tmpdir, "00_config", "config.json"))

        square_num_elec = np.square(config["num_electrodes"])
        img_dummy = octave.struct(
            "type",
            "image",
            "conductivity",
            octave.struct("elem_data", np.ones((square_num_elec, 1))),
            "fwd_solve",
            octave.struct("get_all_meas", 0),
        )
        mock_oct_fwd_solve = mocker.patch(
            "holoshed.lung_fusion.octave.fwd_solve",
            return_value=octave.struct("meas", np.ones((square_num_elec, 1))),
        )

        m = lung_fusion.Model(config, "dummy_fmdl")
        m.check_for_valid_vtu()
        m.fmdl = octave.struct(
            "stimulation",
            "",
            "meas_select",
            "",
            "no_meas",
            np.ones((square_num_elec, 1)),
        )
        m.all_img_i = [img_dummy] * len(m.vtu_list)
        m.write_voltage = True

        m.solve_inhom_forward_problem()
        assert mock_oct_fwd_solve.called
        assert mock_oct_ens_write.called


def test_save_simulated_voltages(mocker):
    """Testing the save_simulated_voltages method."""
    config = {"working_path": "/dummy/path", "sim_name": "dummy_sim"}
    mock_json_write = mocker.patch("holoshed.lung_fusion.JsonDataIO.write")
    mocker.patch("builtins.open")
    m = lung_fusion.Model(config, "dummy_fmdl")
    m.volt_data = np.arange(3)
    m.save_simulated_voltages()

    mock_json_write.assert_called_once()


def initialize_config(config_path) -> dict:
    """Utililty method returning the user defined configuration as a dictionary.

    Args:
        config_path (str):   Path to config file.

    Returns:
        config     (dict):   Dictionary containing the configuration."""

    with open(config_path) as ff:
        config = json.load(ff)
    config["working_path"] = holoshed.utils.get_working_path(config_path)
    return config


class DummyVTUReader:
    """Dummy class mocking the behavior of the VTUReader."""

    def __init__(self, config, path):
        self.config = config
        if "dummy1" in path:
            self.dummy = 1
        elif "dummy2" in path:
            self.dummy = 2
        elif "dummy3" in path:
            self.dummy = 3
        elif "static" in path:
            self.dummy = 0

    @property
    def tracheal_idx(self):
        return 0

    def get_tracheal_flow(self, trach_idx):
        return 1e6 * np.ones((1, 10))[0, trach_idx] * self.dummy

    def get_ventil_pressure(self, trach_idx, flow):
        flow
        return 3 * np.ones((1, 10))[0, trach_idx] * self.dummy
