"""Testing the plot_fe module."""
import matplotlib.pyplot as plt
import numpy as np
from oct2py import octave

import holoshed.plot_fe


def test_plot_fe_surface(mocker):
    """Testing the plot_fe_surface function."""
    mock_set_elec_triang = mocker.patch("holoshed.plot_fe.set_elec_triang")
    mock_plot_electrodes = mocker.patch("holoshed.plot_fe.plot_electrodes")
    mock_plot_trisurf = mocker.patch(
        "mpl_toolkits.mplot3d.axes3d.Axes3D.plot_trisurf"
    )
    mock_savefig = mocker.patch("matplotlib.pyplot.savefig")
    mocker.patch("matplotlib.pyplot.show")

    fmdl = get_dummy_fmdl()
    config = {"working_path": "/dummy/path"}
    holoshed.plot_fe.plot_fe_surf(fmdl, config, True)

    mock_set_elec_triang.called_once()
    mock_plot_electrodes.called_once()
    mock_plot_trisurf.called_once()
    assert mock_savefig.call_count == 2


def test_set_elec_triang():
    """Testing the set_elec_triang function."""
    fmdl = get_dummy_fmdl()
    fmdl.boundary_py = fmdl.boundary - 1
    elec_tri = holoshed.plot_fe.set_elec_triang(fmdl)

    elec_tri_ref = [np.array([0, 1, 2]), np.array([2, 3, 4])]
    assert all(np.allclose(a, b) for a, b in zip(elec_tri_ref, elec_tri))


def test_plot_electrodes(mocker):
    """Testing the plot_electrodes function"""
    mock_plot_trisurf = mocker.patch(
        "mpl_toolkits.mplot3d.axes3d.Axes3D.plot_trisurf"
    )
    mock_text = mocker.patch("mpl_toolkits.mplot3d.axes3d.Axes3D.text")

    fmdl = get_dummy_fmdl()
    elec_tri = [np.array([[0, 1, 2]]), np.array([[2, 3, 4]])]
    fig = plt.figure()
    ax = fig.add_subplot(111, projection="3d")
    holoshed.plot_fe.plot_electrodes(fmdl, elec_tri, ax)

    assert mock_plot_trisurf.call_count == fmdl.electrode.size
    assert mock_text.call_count == fmdl.electrode.size


def get_dummy_fmdl():
    """Utility function generating a dummy forward model for testing purposes"""
    nodes = np.array([[0, 0, 0], [1, 1, 1], [1, 0, 1], [2, 1, 0], [2, 0, 0]])
    boundary = np.array([[1, 2, 3], [2, 3, 4], [3, 4, 5]])

    electrode = np.empty(
        (2,), dtype=[("nodes", np.ndarray), ("z_contact", float)]
    )
    electrode = electrode.view(np.recarray)
    electrode[0] = (np.array([1, 2, 3]), 0.01)
    electrode[1] = (np.array([3, 4, 5]), 0.01)

    fmdl = octave.struct(
        "nodes",
        nodes,
        "boundary",
        boundary,
    )
    fmdl.electrode = electrode
    return fmdl
