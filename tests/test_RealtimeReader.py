from os.path import join as join

import numpy as np

import holoshed.RealtimeReader
import holoshed.utils


def test_get_relative_time():
    """Testing the get_relative_time method."""
    config = make_config()
    rt_reader = holoshed.RealtimeReader.RealtimeReader(config)
    time = rt_reader.get_relative_time()

    assert np.allclose(time, np.arange(0, 3.02, 0.02))


def test_get_pressure_mbar():
    """Testing the get_pressure_mbar method."""
    config = make_config()
    rt_reader = holoshed.RealtimeReader.RealtimeReader(config)
    press = rt_reader.get_pressure_mbar()

    assert np.allclose(
        press[[0, 10, 40]], np.array((21.7344, 21.784, 11.7654))
    )


def test_get_volume_l():
    """Testing the get_volume_l method."""
    config = make_config()
    rt_reader = holoshed.RealtimeReader.RealtimeReader(config)
    vol = rt_reader.get_volume_l()

    assert np.allclose(
        vol[[0, 10, 40]], np.array((0.0, 0.12166223, 0.1468889))
    )


def make_config():
    """Utility function generating a config for testing purpose.
    Returns:
        config  (dict):     Holoshed configuration handle."""
    base_path = holoshed.utils.repo_base_path()
    rt_path = join(
        base_path,
        "tests",
        "reference_files",
        "comb_vtu",
        "01_input",
        "realtimeValues_20210707094239.csv",
    )
    config = {
        "realtime_values_path": rt_path,
        "start_volume_meas": 0,
        "realtime_start": 4,
        "realtime_end": 7,
    }
    return config


test_get_pressure_mbar()
