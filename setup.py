"""Setup Holo Shed.

Example:
    You can install Holo Shed with:

        $ pip install .

    For development purposes use:

        $ pip install -e .[devel]
"""
from setuptools import find_packages, setup

NAME = "holoshed"
VERSION = "0.1"
AUTHOR = "Ebenbuild GmbH"
AUTHOR_EMAIL = "info@ebenbuild.com"


def get_description():
    """Read the README.md for the long description."""
    with open("README.md", "r") as fh:
        long_description = fh.read()
    return long_description


def parse_requirements(filename):
    """Loads requirements from a pip requirements file."""
    lineiter = (line.strip() for line in open(filename))
    return [line for line in lineiter if line and not line.startswith("#")]


if __name__ == "__main__":
    setup(
        name=NAME,
        version=VERSION,
        author=AUTHOR,
        author_email=AUTHOR_EMAIL,
        url="www.ebenbuild.com",
        description="Generate EIT images from simulation output.",
        long_description=get_description(),
        long_description_content_type="text/markdown",
        packages=find_packages(),
        python_requires="==3.9.*",
        classifiers=[
            "Programming Language :: Python :: 3.9",
        ],
        # Requirements get automatically collected from requirements.in and
        # installed when installing via pip
        install_requires=parse_requirements("requirements/requirements.in"),
        # Add option [devel] to install all the development related stuff too
        extras_require={
            "devel": parse_requirements("requirements/requirements-devel.in"),
        },
        # Enter a unique command line command here so you can use your package
        # without "python main.py <ARGS>" but with "unique_command <ARGS>"
        entry_points={
            "console_scripts": [
                "holoshed=main:main",
            ]
        },
    )
