"""Configuration file for the sphinx documentation."""
import os
import sys
from datetime import date

sys.path.insert(0, os.path.abspath("../../"))

from setup import AUTHOR, NAME, VERSION  # noqa

# -- Project information -----------------------------------------------------

project = NAME
author = AUTHOR
copyright = f"{date.today().year}, {AUTHOR}"
release = VERSION

# -- General configuration ---------------------------------------------------

# Add Sphinx extension module names here, as strings.
extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.doctest",
    "sphinx.ext.todo",
    "sphinx.ext.coverage",
    "sphinx.ext.mathjax",
    "sphinx.ext.viewcode",
    "sphinx.ext.inheritance_diagram",
    "sphinx.ext.napoleon",
    "m2r2",
]
mathjax_path = (
    "https://cdn.mathjax.org/mathjax/latest/MathJax.js?"
    "config=TeX-AMS-MML_HTMLorMML"
)

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
exclude_patterns = []

source_suffix = [".rst", ".md"]

master_doc = "index"

# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.
html_theme = "bizstyle"

# The logo displayed in the top left corner
html_logo = "https://ebenbuild.com/assets/images/ebenbuild_solo.svg"

# The favicon of the documentation pages
html_favicon = "https://ebenbuild.com/assets/images/favicon.png"
