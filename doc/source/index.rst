.. calculon documentation master file, created by
   sphinx-quickstart on Thu Sep  3 13:40:57 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to holoshed's documentation!
====================================

.. mdinclude:: ../../README.md

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   modules.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
