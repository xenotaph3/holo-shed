import os

import numpy as np
from oct2py import octave

from .JsonDataIO import JsonDataIO as JsonDataIO
from .MeasEitReader import MeasEitReader as MeasEitReader


def makeGREIT(config, fmdl):
    """Function recalling the forward model and preparing the solution of the inverse model.

    Args:
        config              (dict):   User defined configuration and parameters for the EIT simulation.
        fmdl    (oct2py.io.struct):   EIDORS forward model object.

    Returns:
        eit_images_sim   (oct2py.io.Cell):   EIDORS EIT image objects based on simulated data for all reconstruction times.
        eit_images_meas  (oct2py.io.Cell):   EIDORS EIT image objects based on measured data for all reconstruction times.
        config                     (dict):   User defined configuration and parameters for the EIT simulation.
    """
    config = initialize_makeGREIT(config)

    conduct_other_tissues = None
    if "GREIT_template_conductivity_other_tissues" in config:
        conduct_other_tissues = config[
            "GREIT_template_conductivity_other_tissues"
        ]

    img_template = prepare_eidors_image(
        fmdl,
        config["GREIT_template_conductivity_lung"],
        config["GREIT_template_conductivity_thorax"],
        conduct_other_tissues,
    )

    img_template = assign_stimulation_patterns(config, img_template)

    eit_images_meas = None
    eit_images_sim = None

    if config["reconstruct_from_sim_data"]:
        vv_sim = prepare_simulation_data(config)
        eit_images_sim = octave.call_mk_GREIT_model(
            img_template, config, vv_sim, True, []
        )

    if config["reconstruct_from_meas_data"]:
        vv_meas, detached_elec = prepare_measurement_data(config)
        eit_images_meas = octave.call_mk_GREIT_model(
            img_template, config, vv_meas, False, detached_elec
        )
    return eit_images_sim, eit_images_meas, config


def initialize_makeGREIT(config):
    """Initializing the makeGREIT module. Reads the realtimeValues_*.csv and processes the time stamp.
    Saves the reconstruction times and the time for prestressing into the config.
    Args:
        config    (dict):   User defined configuration and parameters for the EIT simulation.

    Returns:
        config    (dict):   User defined configuration and parameters for the EIT simulation.
    """
    if config["reconstruct_from_sim_data"]:
        prestress_time = config["sim_duration_tot"] - (
            config["realtime_end"] - config["realtime_start"]
        )
        config["prestress_time"] = prestress_time
        print(f"Prestressing time: shifting by {prestress_time} seconds")
    return config


def prepare_eidors_image(
    fmdl,
    conductivity_lung,
    conductivity_thorax,
    conductivity_other_tissues=None,
):
    """Generate EIDORS image from forward model. Assign template conductivity to lungs and thorax.

    Args:
        fmdl       (oct2py.io.struct):      EIDORS forward model.
        conductivity_lung     (float):      Reconstruction template conductivity of the lungs.
        conductivity_thorax   (float):      Reconstruction template conductivity of the thorax.
        conductivity_other_tissues (list):  List of reconstruction template conductivities of other tissues.

    Returns:
        img     (oct2py.io.struct):      EIDORS image object.
    """
    assert conductivity_lung > 0
    assert conductivity_thorax > 0

    img = octave.mk_image(fmdl, conductivity_thorax)

    img.elem_data[fmdl.lung_IDs.astype(int)] = conductivity_lung
    if conductivity_other_tissues is not None:
        assert len(conductivity_other_tissues) == len(
            fmdl.tiss_IDs
        ), "Error. The number of other tissues in the mesh does not match the number of conductivity values in the config file. For the GREIT reconstruction, you need to provide 'GREIT_template_conductivity_other_tissues' in the config file."
        for i in range(len(fmdl.tiss_IDs)):
            assert conductivity_other_tissues[i] > 0
            img.elem_data[
                fmdl.tiss_IDs[i].astype(int)
            ] = conductivity_other_tissues[i]

    img.fwd_model = octave.rmfield(img.fwd_model, "meas_select")
    return img


def assign_stimulation_patterns(config, img):
    """Generate stimulation patterns and assign them to the EIDORS image.

    Args:
        config              (dict):      User defined configuration and parameters for EIT simulation.
        img     (oct2py.io.struct):      EIDORS image object.

    Returns:
        img     (oct2py.io.struct):      EIDORS image object.
    """
    stim, meas = octave.mk_stim_patterns(
        config["num_electrodes"],
        config["num_rings"],
        config["inject_pattern"],
        config["meas_pattern"],
        {config["meas_current"]},
        config["amp"],
        nout=2,
    )
    img.fwd_model.stimulation = stim
    img.fwd_model.meas_select = np.array(meas, dtype=bool)
    return img


def prepare_measurement_data(config):
    """Reads the eit measurement files to extract the measured voltage and the IDs of the detached electrodes.
    Args:
        config      (dict):     User defined configuration and parameters for the EIT simulation.

    Returns:
        vv              (np.array):     Complex valued measured voltages for n_ele x n_ele channels. The columns
                                        indicate the time steps.
        detached_elec       (list):     IDs of the detached electrodes. Counting from 0. --> [0, n_elec-1]
    """
    meas_reader = MeasEitReader(config)
    vv = meas_reader.get_volt_reconst_frames()
    detached_elec = meas_reader.get_detached_electrodes()
    return vv, detached_elec


def prepare_simulation_data(config):
    """Reads the simulated voltages from 03_forward_results/*_elec_voltages.json.

    Returns:
        volt    (np.array):     Simulated voltage for n_ele x n_ele measurement channels and the reconstruction time frames.
    """
    volt_data_path = os.path.join(
        config["working_path"],
        "03_forward_results",
        config["sim_name"] + "_elec_voltages.json",
    )
    json_io = JsonDataIO()
    dat = json_io.read(volt_data_path)
    volt = dat["volt_data"]
    return volt
