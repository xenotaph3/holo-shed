"""Module providing functions to generate a plot of the FE thorax model including the electrodes and their IDs."""
import os

import matplotlib.pyplot as plt


def plot_fe_surf(fmdl, config, interactive):
    """Plotting the FE surface mesh of the thorax and the electrode positions.

    Args:
        fmdl    (oct2py.io.struct):     Eidors forward model object.
        config              (dict):     User defined configuration and parameters for the EIT simulation.
        interactive         (bool):     Execution in interactive mode, i.e. the plots are shown and the execution is paused.
    """
    fmdl.boundary_py = fmdl.boundary.astype("int16") - 1

    fig = plt.figure()
    ax = fig.add_subplot(111, projection="3d")
    pos_lim = fmdl.nodes.max(axis=0) + 50
    neg_lim = fmdl.nodes.min(axis=0) - 50
    ax.set_xlim3d(neg_lim[0], pos_lim[0])
    ax.set_ylim3d(neg_lim[1], pos_lim[1])
    ax.set_zlim3d(neg_lim[2], pos_lim[2])
    ax.set_aspect("equal")

    electrode_triangles = set_elec_triang(fmdl)
    ax.plot_trisurf(
        fmdl.nodes[:, 0],
        fmdl.nodes[:, 1],
        fmdl.nodes[:, 2],
        triangles=fmdl.boundary_py,
        color=(0, 0, 1, 0.4),
    )
    plot_electrodes(fmdl, electrode_triangles, ax)

    output_dir = os.path.join(config["working_path"], "02_inhomogeneous_mesh")
    ax.view_init(30, -70, 0)
    plt.savefig(os.path.join(output_dir, "electrode_order_frontal_view.png"))
    ax.view_init(83, -90, 0)
    plt.savefig(os.path.join(output_dir, "electrode_order_top_view.png"))

    if interactive:
        plt.show()


def set_elec_triang(fmdl):
    """Function generating a list of triangle topologies for each electrode.
    Each list element contains an numpy.array with the node indices forming the triangles of the electrode surfaces.

    Args:
        fmdl    (oct2py.io.struct):     Eidors forward model object.

    Returns:
        elec_tri            (list):  List containing numpy.array for each electrode. Each array provides the triangle topology of the corresponding electrode."""
    elec_tri = []
    for i in range(fmdl.electrode.size):
        nodes = fmdl.electrode["nodes"][i].astype("int16") - 1
        nodes = set(nodes.flatten())
        triangles = []
        for line in fmdl.boundary_py:
            if all(x in nodes for x in line):
                triangles.append(line)
        elec_tri.append(triangles)
    return elec_tri


def plot_electrodes(fmdl, elec_tri, ax):
    """Function plotting all electrode surfaces based on the elctrode triangle topology. Additionally, the electrode ID is displayed.

    Args:
        fmdl                            (oct2py.io.struct):     Eidors forward model object.
        elec_tri                                    (list):     List containing numpy.array for each electrode. Each array provides the
                                                                triangle topology of the corresponding electrode.
        ax       (matplotlib.axes._subplots.Axes3DSubplot):     Axis handle of the three-dimensional FE plot.
    """
    for i in range(fmdl.electrode.size):
        ax.plot_trisurf(
            fmdl.nodes[:, 0],
            fmdl.nodes[:, 1],
            fmdl.nodes[:, 2],
            triangles=elec_tri[i],
            color=(1, 0, 0, 1),
        )
        label_node_id = elec_tri[i][0][0]
        ax.text(
            fmdl.nodes[label_node_id, 0],
            fmdl.nodes[label_node_id, 1],
            fmdl.nodes[label_node_id, 2],
            str(i + 1),
            (1, 1, 0),
            color="black",
        )
