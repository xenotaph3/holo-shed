from abc import ABC, abstractmethod
from pathlib import Path

import meshio
import numpy as np

from .tubus_parameters import get_guttmann_constants


class VTUReader(ABC):
    """Class providing methods to read VTU files and conduct the mapping of alveolar clusters to tetrahedral elements.

    Attributes:
        vtu_path                  (str):    Absolute path of the current VTU file.
        vtu         (meshio._mesh.Mesh):    VTU file containing static and/or dynamic result data.
        ac_indices        (numpy.array):    Indices of the alveolar cluster elements in the global array of elements (AC + AW).
        is_ac             (numpy.array):    Array of bools defining if element is alveolar cluster or not.
        ac_nodes          (numpy.array):    Array (n x 3) containing the coordinates of the alveolar cluster nodes. This means that n
                                            is twice the number of alveolar clusters.
        ac_radius         (numpy.array):    Equvalence radius of each AC.
        num_ac                    (int):    Number of alveolar clusters.
        tracheal_idx              (int):    Index of the tracheal element.
        config                   (dict):    Configuration of holoshed.
    """

    def __init__(self, config, vtu_path):
        """Method initializing the VTUReader class."""
        self.vtu_path = vtu_path
        self._ac_indices = None
        self._is_ac = None
        self._ac_nodes = None
        self._ac_radius = None
        self._tracheal_idx = None
        self.num_ac = 0
        self.config = config

        self.vtu = meshio.read(self.vtu_path)

    def get_ac_nodes_and_indices(self):
        """Method reading the element topology from the VTU file, filtering the alveolar clusters from the airway elements
        and writing the nodes out. As each alveolar cluster has two points the length of the nodes array is twice the
        number of alveolar clusters."""
        ac_elements = self.vtu.get_cells_type("line")
        points = self.vtu.points

        self.num_ac = np.sum(self.is_ac)
        ac_nodes = np.zeros((self.num_ac, 3))
        ac_indices = np.zeros((self.num_ac, 1), dtype=int)
        ac_radius = np.zeros((self.num_ac, 1))
        ac_idx = 0
        for ele_idx in range(0, ac_elements.shape[0]):
            if self.is_ac[ele_idx]:
                ac_nodes[ac_idx] = points[int(ac_elements[ele_idx, 1]), :]
                ac_radius[ac_idx] = np.linalg.norm(
                    points[int(ac_elements[ele_idx, 1]), :]
                    - points[int(ac_elements[ele_idx, 0]), :]
                )
                ac_indices[ac_idx] = int(ele_idx)
                ac_idx += 1
        return ac_nodes, ac_indices, ac_radius

    @staticmethod
    def create(config, vtu_path):
        """Creates the a reader instance reading the file in vtu_path depending on the VTU input shape.
        Args:
            config             (dict):      User defined configuration data.
            vtu_path            (str):      Path to the current VTU file.

        Returns:
            VTUReader     (holoshed.VTUReader):     Reader instance depending on if the VTU is split into static and
                                                    dynamic part or combined.
        """
        if config["vtu_split_flag"]:
            return VTUReaderSplit(config, vtu_path)
        elif not config["vtu_split_flag"]:
            return VTUReaderCombined(config, vtu_path)

    @abstractmethod
    def get_ac(self):
        raise NotImplementedError()

    @abstractmethod
    def get_filling_factor(self, model):
        raise NotImplementedError()

    @abstractmethod
    def tracheal_idx(self):
        raise NotImplementedError()

    @abstractmethod
    def get_tracheal_flow(self, trach_idx):
        raise NotImplementedError()

    @abstractmethod
    def get_ventil_pressure(self, trach_idx, flow):
        raise NotImplementedError()

    @property
    def ac_nodes(self):
        if self._ac_nodes is None:
            (
                self._ac_nodes,
                self._ac_indices,
                self._ac_radius,
            ) = self.get_ac_nodes_and_indices()
        return self._ac_nodes

    @property
    def ac_indices(self):
        if self._ac_indices is None:
            (
                self._ac_nodes,
                self._ac_indices,
                self._ac_radius,
            ) = self.get_ac_nodes_and_indices()
        return self._ac_indices

    @property
    def ac_radius(self):
        if self._ac_radius is None:
            (
                self._ac_nodes,
                self._ac_indices,
                self._ac_radius,
            ) = self.get_ac_nodes_and_indices()
        return self._ac_radius

    @property
    def is_ac(self):
        if self._is_ac is None:
            self._is_ac = self.get_ac()
        return self._is_ac


class VTUReaderCombined(VTUReader):
    """Reader class for reading and processing VTU files that combine static and dynamic part."""

    def __init__(self, config, vtu_path):
        super().__init__(config, vtu_path)

    def get_ac(self):
        """Method writing the is_ac attribute. In combined VTU files, alveolar cluster can be characterized by the generation -1."""
        generations = self.vtu.get_cell_data("generations", "line")
        return generations == -1

    def get_filling_factor(self, model):
        """Method getting the filling factor of the lung tissue. It is defined as \frac{V_{air}}{V_{tis}+V_{edema}}.
        Args:
            model   (holoshed.lung_fusion.Model):      Current lung_fusion Model containing the weighted mapping.
        Returns:
            FF                     (numpy.array):      Array containing the filling factor defined by \frac{V_{air}}{V_{tis}+V_{edema}}.
        """

        ac_vol_tissue = self.vtu.get_cell_data("acini_volume_tissue", "line")[
            model.ac_mapping
        ]
        ac_vol_edema = self.vtu.get_cell_data("acini_volume_edema", "line")[
            model.ac_mapping
        ]
        ac_vol_air = self.vtu.get_cell_data("acini_volume", "line")[
            model.ac_mapping
        ]
        FF = np.divide(ac_vol_air, ac_vol_tissue + ac_vol_edema)
        return FF

    @property
    def tracheal_idx(self):
        if self._tracheal_idx is None:
            self._tracheal_idx = np.where(
                self.vtu.get_cell_data("generations", "line") == 0
            )[0][1]
        return self._tracheal_idx

    def get_tracheal_flow(self, trach_idx):
        """Returns the volume flow at the tracheal inlet.
        Args:
            trach_idx    (int):  Index of the tracheal element.
        Returns:
            numpy.array:  Vector containing the tracheal flow of this VTU.
        """
        return self.vtu.get_cell_data("flow_in", "line")[trach_idx]

    def get_ventil_pressure(self, trach_idx, _):
        """Returns the ventilation pressure applied at the trachea inlet.
        Args:
            trach_idx        (int):  Index of the tracheal element.
        Returns:
            numpy.array:  Vector containing the pressure at the inlet of the trachea.
        """
        idx = self.vtu.cells_dict["line"][trach_idx][0]
        return self.vtu.point_data["pressure"][idx]


class VTUReaderSplit(VTUReader):
    """Reader class for reading and processing VTU files which are split into static and dynamic part."""

    def __init__(self, config, vtu_path):
        super().__init__(config, vtu_path)
        static_vtu_path = (
            vtu_path.rstrip(".vtu").rsplit(".", 1)[0] + ".constant.0.vtu"
        )
        self.static_reader = None
        if Path(static_vtu_path).is_file():
            self.static_reader = VTUReaderSplit(config, static_vtu_path)

    def get_ac(self):
        """Method writing the is_ac attribute. In split VTU files, alveolar cluster can be characterized by the ele_set_id."""
        ele_set_id = self.vtu.get_cell_data("ele_set_id", "line")
        return ele_set_id == 1

    def get_filling_factor(self, model):
        """Method getting the filling factor of the lung tissue. For split VTU files, this is calculated from the
        various volume contributions directly.
        Args:
            model   (holoshed.lung_fusion.Model):      Current lung_fusion Model containing the weighted mapping.
        Returns:
            filling_factor         (numpy.array):      Array containing the filling factor defined by \frac{V_{air}}{V_{tis}+V_{liq}}.
        """

        all_ac_vol_gas = self.vtu.get_cell_data("volume", "line")
        if self.static_reader is None:
            raise RuntimeError(
                'The filling factor can only be evaluated if the "constant" data file is available'
            )
        all_ac_vol_tissue = self.static_reader.vtu.get_cell_data(
            "tissue_volume", "line"
        )
        all_ac_vol_liquid = self.static_reader.vtu.get_cell_data(
            "liquid_volume", "line"
        )

        acmap = model.ac_mapping

        if acmap.ndim == 2 and acmap.shape[1] == 1:
            # one-to-one mapping between tets and ACs
            ac_vol_tissue = all_ac_vol_tissue[acmap]
            ac_vol_liquid = all_ac_vol_liquid[acmap]
            ac_vol_gas = all_ac_vol_gas[acmap]

            # filling factors are defined for the tets
            filling_factors = np.divide(
                ac_vol_gas, ac_vol_tissue + ac_vol_liquid
            )
            return filling_factors
        elif acmap.ndim == 2 and acmap.shape[1] > 1:
            # weighted mapping based on volumetric overlaps, with ac_map of size num_tets x num_acs
            all_ac_vol_nongas = all_ac_vol_tissue + all_ac_vol_liquid
            all_ac_vol_nongas[all_ac_vol_nongas == 0] = np.NaN
            ac_filling_factors = np.divide(all_ac_vol_gas, all_ac_vol_nongas)
            tet_filling_factors = np.nansum(
                acmap * ac_filling_factors.reshape(1, -1), 1
            )
            return tet_filling_factors.reshape(-1, 1)
        else:
            raise RuntimeError(
                f"The alveolar cluster mapping has an unpexted shape {acmap.shape}"
            )

    @property
    def tracheal_idx(self):
        if self._tracheal_idx is None:
            self._tracheal_idx = np.where(
                self.vtu.get_cell_data("generation", "line") == 0
            )[0][0]
        return self._tracheal_idx

    def get_tracheal_flow(self, trach_idx):
        """Returns the volume flow at the tracheal inlet.
        Args:
            trach_idx    (int):  Index of the tracheal element.
        Returns:
            numpy.array:  Vector containing the tracheal flow of this VTU.
        """
        return self.vtu.get_cell_data("flow", "line")[trach_idx]

    def get_ventil_pressure(self, trach_idx, flow):
        """Returns the ventilation pressure applied at the tubus inlet.
        For alveonx data, the tubus is not contained in the model. Therefore the ventilation pressure as described
        in Guttmann et. al. (1993): Continuous calculation of intratracheal pressure in tracheally intubated patients.

        Args:
            trach_idx           (int):  Index of the tracheal element.
            flow        (numpy.float):  Current flow through trachea.
        Returns:
            tub_press   (numpy.array):  Vector containing the pressure at the inlet of the trachea.
        """
        idx = self.vtu.cells_dict["line"][trach_idx][0]
        flow = flow.astype(complex) * 1e-6
        k1, k2 = get_guttmann_constants(self.config, flow > 0)
        aw_press = self.vtu.point_data["pressure"][idx]
        tub_press = aw_press + np.real(k1 * np.power(flow, k2))
        return tub_press
