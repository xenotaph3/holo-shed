"""Module providing functions to plot the EIT image as a contour plot."""
import os

import matplotlib.pyplot as plt
import numpy as np
from matplotlib import cm
from oct2py import octave

from .JsonDataIO import JsonDataIO as JsonDataIO
from .utils import get_intersection_line as get_intersection_line


def plot_eit(eit_images, config, output_dir, output_prefix, interactive):
    """Plots the reconstructed EIT image in a thorax slice and saves them to a folder.

       In addition the data of the EIT reconstructions underlying the plots
       is saved as a `.npz` file within the same folder.

    Args:
        eit_images    (oct2py.io.Cell):     Cell array of EIDORS image handles containing the forward model
                                            and the reconstructed element data. One image for each timestep.
        config                  (dict):     User defined configuration and parameters for the EIT simulation.
        output_dir               (str):     Path to the directory where the reconstructed png images are stored.
        output_prefix            (str):     Prefix for the names of the PNG output.
        interactive             (bool):     Command line flag running holoshed in interactive mode, i.e. plotts are shown during runtime.

    Returns:
        rec_images              (list):      List of numpy arrays containing the reconstructed images in matrix form.
    """
    numpix = config["GREIT_imgsz"]
    timesteps = []
    rec_times = []

    lungs = get_lung_contours(
        config, eit_images[0][0].fwd_model.mdl_slice_mapper.level[0][2]
    )
    rec_images = []
    for i in range(0, len(eit_images)):
        current_eit = eit_images[i][0]
        elem_data = current_eit.fwd_model.coarse2fine * current_eit.elem_data

        elem_ptr = octave.mdl_slice_mapper(current_eit.fwd_model, "elem")
        elem_ptr = elem_ptr.astype("int16")

        rval = elem_data.flatten()  # reconstructed image in vector form
        rval = np.insert(rval, 0, np.nan)
        rimg = np.reshape(
            rval[elem_ptr], (numpix, numpix)
        )  # reconstructed image in matrix form
        rec_images.append(rimg)

        rec_times.append(eit_images[i][0]["time"])
        timesteps.append(eit_images[i][0]["time_step"])

    max_value = np.nanmax([np.nanmax(arr) for arr in rec_images])
    min_value = np.nanmin([np.nanmin(arr) for arr in rec_images])
    color_levels = np.linspace(min_value, max_value, config["color_levels"])

    # mesh is static over timesteps
    X, Y = np.meshgrid(
        current_eit.fwd_model.mdl_slice_mapper.x_pts,
        current_eit.fwd_model.mdl_slice_mapper.y_pts,
    )

    for i in range(0, len(eit_images)):

        ax = plt.subplots()[1]
        plt.contourf(
            X,
            Y,
            rec_images[i],
            levels=color_levels,
            cmap=cm.Blues,
            vmin=min_value,
            vmax=max_value,
        )
        plt.colorbar()
        ax.invert_yaxis()

        if config["show_lung_contours"]:
            plot_lung_contour(lungs["left"])
            plot_lung_contour(lungs["right"])

        png_name = (
            output_prefix
            + "_EIT_image_ts"
            + str(int(eit_images[i][0]["time_step"]))
            + ".png"
        )
        plt.savefig(os.path.join(output_dir, png_name))

        if interactive:
            plt.show()
        plt.close()

    # save the mesh, timesteps and EIT reconstructions (as 3D) array
    # the first dimension of `eit` corresponds to the timesteps, with
    # second and third dimension corresponding to the mesh (X,Y).
    # also include lung masks / topology information
    np.savez_compressed(
        os.path.join(output_dir, "eit_reconstruction_data.npz"),
        X=X,
        Y=Y,
        eit=np.array(rec_images),
        timesteps=timesteps,
        rec_times=rec_times,
        left_lung=np.array(lungs["left"]),
        right_lung=np.array(lungs["right"]),
    )

    return rec_images


def get_lung_contours(config, level):
    """Function to obtain the line contours of both lung lobes in the section plane at a certain z-level.
    Currently, it is not possible to use a tilted section plane.
    Args:
        config      (dict):     User defined configuration and parameters for the EIT simulation.
        level      (float):     z-Position of the section plane.
    Returns:
        (dict):     Nodes and topology of the intersection contour of both lung lobes.
    """
    lung_topology_path = os.path.join(
        config["working_path"],
        "02_inhomogeneous_mesh",
        "lung_topology.json",
    )

    json_io = JsonDataIO()
    lung_topo = json_io.read(lung_topology_path)

    nodes = lung_topo["nodes"]
    topo_left = np.array(lung_topo["left_lung"]) - 1
    topo_right = np.array(lung_topo["right_lung"]) - 1

    left_line = get_intersection_line(nodes, topo_left, level)
    right_line = get_intersection_line(nodes, topo_right, level)
    return {"left": left_line, "right": right_line}


def plot_lung_contour(lung):
    """Function plotting the lung contour of a lung lobe into the current plot figure.
    Args:
        lung    (pyvista_ndarray):     Intersection line of the lung and the cutting plane defined by a nx3 array of coordinates.
    """
    plt.plot(lung[:, 0], lung[:, 1], color="black")
