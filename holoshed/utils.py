"""This module provides several utility methods for holoshed."""
import os

import numpy as np
import pyvista as pv


def repo_base_path() -> str:
    """Get the path to the base directory of this repository

    Returns:
        str:    Path to repo base
    """
    this_file_dir = os.path.dirname(os.path.abspath(__file__))
    return os.path.normpath(os.path.join(this_file_dir, ".."))


def get_working_path(config_path) -> str:
    """Get the path of the directory, wherre the defined folder structure is located. This is two levels above the config.json file.

    Args:
        config_path (str):  Path to the config file (working_path/00_config/config.json)

    Returns:
        str:    Path to the defined folder structure.
    """
    config_path = os.path.abspath(config_path)

    return os.path.split(os.path.dirname(config_path))[0]


def get_element_center_coordinates(elements, nodes) -> np.array:
    """Function returning the coordinates of the center of gravity for all elements. Works for all element shapes in a three-dimensional space.

    Args:
        elements    (numpy.array):      m x n array containing the node IDs of each element. m is the total number of elements that are evaluated.
                                        n refers to the number of nodes per element. (4 for tets, 2 for acini)
        nodes       (numpy.array):      p x 3 array containing the node coordinates

    Returns:
        ele_center  (numpy.array):      m x 3 array containing the coordinates of the center for all elements.
    """
    ele_center = np.zeros((elements.shape[0], 3))
    avg_factor = 1 / elements.shape[1]
    for i in range(elements.shape[0]):
        ele_coords = nodes[elements[i, :], :]
        ele_center[i, :] = np.sum(ele_coords, axis=0) * avg_factor
    return ele_center


def get_time_mapping_indices(tim_vec_sample, time_vec_target):
    """Function mapping two time vectors with different sampling rate. Maps all sample points to the closest target and returns the indices.
    Args:
        time_vec_sample     (np.array):     Time vector of length num_sample.
        time_vec_target     (np.array):     Target time vector.

    Returns:
        ids                     (list):     Contains the index of the closest time in the target for all sample points. Has length num_sample.
    """
    ids = []
    for sample in tim_vec_sample:
        diff_time = [abs(target - sample) for target in time_vec_target]
        closest_idx = diff_time.index(min(diff_time))
        ids.append(closest_idx)
    return ids


def get_intersection_line(nodes, topo, level=None, plane=None):
    """Function calculating the intersection line of a arbitrary surface mesh with an intersection plane.
    Args:
        nodes       (np.array):     nx3 array containing the coordinates of the points.
        topo        (np.array):     mx3 array containing the topology of the triangle surface mesh.
        level          (float):     z-Position of the section plane.
        plane    (pv.PolyData):     Pyvista plane object defining the intersection plane. The plane needs
                                    to be discretized by triangles. If no plane is provided, a z-plane at
                                    the defined level is used.
    Returns:
        line        (np.array):     3xk array containing the coordinates of the intersection line.
    """
    faces = np.hstack([3 * np.ones([len(topo), 1], dtype=int), topo])
    mesh = pv.PolyData(nodes, faces)

    if plane is None:
        plane = pv.Plane(
            center=(0, 0, level), direction=(0, 0, 1), i_size=600, j_size=600
        ).triangulate()

    intersec = mesh.intersection(plane, False, False)[0]

    num_lines = int(len(intersec.lines) / 3)
    connectivity = intersec.lines.reshape([num_lines, 3])[:, 1:3]
    line = sort_vertices(connectivity, intersec.points)
    return line


def sort_vertices(ele, vert):
    """Function sorting the nodes of a closed curve defined by a element connectivity and the vertex
    coordinates. The special thing here is, that the element connectivity is non cohesive meaning each
    row defines the connection between two points. Two consecutive rows however may be located at
    different parts of the curve.

    Args:
        ele     (np.array):     2xn array of int containing which vertices are connected.
        vert    (np.array):     3xm array containing the coordinates of the vertices.

    Returns:
        sorted_verts_consecutive_order      (np.array):     3xm+1 array containing the vertices in
                                                            consecutive order. This defines a closed curve.
    """

    def find_next_vertex(current_vertex):
        for next_vertex in range(num_vertices):
            if adjacency_matrix[current_vertex, next_vertex] == 1:
                adjacency_matrix[current_vertex, next_vertex] = 0
                adjacency_matrix[next_vertex, current_vertex] = 0
                return next_vertex
        return None

    num_vertices = len(vert)
    adjacency_matrix = np.zeros((num_vertices, num_vertices), dtype=int)

    for edge in ele:
        vertex1, vertex2 = edge
        adjacency_matrix[vertex1, vertex2] = 1
        adjacency_matrix[vertex2, vertex1] = 1

    sorted_vertices = [0]  # Start with the first vertex
    while len(sorted_vertices) < num_vertices:
        next_vertex = find_next_vertex(sorted_vertices[-1])
        if next_vertex is not None:
            sorted_vertices.append(next_vertex)
        else:
            raise ValueError("The curve is not closed.")

    sorted_vertices.append(0)  # End with the first vertex

    sorted_vertices = np.array(sorted_vertices)

    sorted_verts_consecutive_order = vert[sorted_vertices]
    return sorted_verts_consecutive_order


def ismember_row(a, b):
    """Function to check if the rows of a are in b.
    Args:
        a (np.array):   Array to be checked.
        b (np.array):   Array to be checked against.
    Returns:
        (np.array):     Boolean array indicating if the rows of a are in b.
    """
    _, rev = np.unique(np.concatenate((b, a)), axis=0, return_inverse=True)

    a_rev = rev[len(b) :]
    b_rev = rev[: len(b)]

    return np.isin(a_rev, b_rev)


class Tet(object):
    """Simple utility class for Tetrahedrons to lookup if points within domain."""

    def __init__(self, nodes: np.ndarray):
        """Construct tetrahedron defined by 4 points

        Args:
            points  :   nodal coordinates
        """

        # Note: implementation on the basis of a codesnippet:
        # https://stackoverflow.com/questions/25179693/how-to-check-whether-the-point-is-in-the-tetrahedron-or-not

        assert nodes.ndim == 2
        assert nodes.shape[0] == 4
        assert nodes.shape[1] == 3

        origin, *rest = nodes
        mat = (np.array(rest) - origin).T
        tetra = np.linalg.inv(mat)
        self._tetra = tetra
        self._origin = origin

    def points_inside(self, points: np.ndarray) -> np.array:
        """Vectorized lookup whether points reside within tetrahedron.

        Args:
            points  :

        Returns:
            boolean numpy vector containing true and false entries
        """

        newp = np.matmul(self._tetra, (points - self._origin).T).T
        return (
            np.all(newp >= 0, axis=-1)
            & np.all(newp <= 1, axis=-1)
            & (np.sum(newp, axis=-1) <= 1)
        )
