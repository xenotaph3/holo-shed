import gzip
import json

import numpy as np


class JsonDataIO:
    """Class providing methods to read and write dictionary data to json file.
    This is necessary as json is not able to handle numpy arrays and complex numbers.

    Attributes:
        mod_data    (dict):     Processed dictionary in json serializable format.
    """

    def __init__(self, data=None):
        if isinstance(data, dict):
            self.mod_data = modify_dict(data)
        else:
            self.mod_data = {"data", data}

    def write(self, path):
        """Method writing the modified data to a zipped json file.
        Args:
            path    (str):  Path of the json file.
        """
        with gzip.open(path, "w") as outfile:
            json_bytes = json.dumps(self.mod_data, indent=4).encode("utf-8")
            outfile.write(json_bytes)

    def read(self, path):
        """Method reading a zipped json file and returning the original data format."""
        with gzip.open(path, "r") as file:
            self.mod_data = json.loads(file.read().decode("utf-8"))

        data = restore_original_dict(self.mod_data)
        return data


def restore_original_dict(mod_data):
    """Function restoring data from the modified data format. The modified data format saves numpy arrays of
    complex numbers as lists of strings.
    Args:
        mod_data    (dict):     Data in modified format (without numpy arrays and complex numbers).
    Returns:
        data        (dict):     Data in original format (containing numpy arrays and complex numbers.)
    """
    data = {}
    for key in mod_data.keys():
        if isinstance(mod_data[key], list):
            sample_instance = get_first_instance_of_list(mod_data[key])
            if isinstance(sample_instance, str) and contains_complex_number(
                sample_instance
            ):
                field_data = np.array(mod_data[key]).astype("complex128")
            elif isinstance(sample_instance, float):
                field_data = np.array(mod_data[key])
            else:
                field_data = mod_data[key]
        elif isinstance(mod_data[key], dict):
            field_data = restore_original_dict(mod_data[key])
        else:
            field_data = mod_data[key]
        data[key] = field_data

    return data


def modify_dict(data):
    """Function modifing a data dictionary to be json serializable. The modified data format saves numpy arrays of
    complex numbers as lists of strings.
    Args:
        data        (dict):     Data in original format (containing numpy arrays and complex numbers.)
    Returns:
        mod_data    (dict):     Data in modified format (without numpy arrays and complex numbers).
    """
    mod_data = {}
    for key in data.keys():
        if isinstance(data[key], np.ndarray):
            if np.iscomplexobj(data[key]):
                field_data = np_array_to_list_of_strings(data[key])
            else:
                field_data = data[key].tolist()
        elif isinstance(data[key], dict):
            field_data = modify_dict(data[key])
        else:
            field_data = data[key]
        mod_data[key] = field_data
    return mod_data


def np_array_to_list_of_strings(var):
    """Fuction transferring a multidimensional numpy array into a multidimensional list."""
    if isinstance(var, np.ndarray):
        return [np_array_to_list_of_strings(x) for x in var]
    else:
        return str(var)


def get_first_instance_of_list(var):
    """Function returing the first instance as a representative for the data type of the list."""
    if isinstance(var, list) and var:
        return get_first_instance_of_list(var[0])
    else:
        return var


def contains_complex_number(string):
    """Determine whether the input string is of shape (1+3j) and can be transformed into a complex number."""
    try:
        complex(string)
        return True
    except:
        return False
