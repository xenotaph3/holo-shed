import xml.etree.ElementTree as ET

import defusedxml


class PvdIO:
    """Class providing methods to read and write data in the pvd format.
    Essentially this allows reading the timesteps of the individual VTUs.

    Attributes:
        data    (list):     List of dictionaries containing the DataSet collections. The length of the list
                            corresponds to the number of timesteps of the simulation.
    """

    def __init__(self, data=None):
        self.data = data

    @classmethod
    def from_file(cls, path):
        """Read data from path and store in the data attribute.
        Args:
            path    (str):      Path of the pvd-file.
        """
        tree = defusedxml.ElementTree.parse(path)
        root = tree.getroot()
        data = []
        for i in range(len(root[0])):
            data.append(root[0][i].attrib)
        return cls(data=data)

    def write(self, path):
        """Write data into the xml pvd format.
        Args:
            path    (str):      Path of the pvd-file.
        """
        root = ET.Element(
            "VTKFile",
            attrib={
                "type": "Collection",
                "version": "0.1",
                "ByteOrder": "LittleEndian",
            },
        )
        comment = ET.Comment("VTK generated with holoshed.")
        root.append(comment)

        child = ET.SubElement(root, "Collection")

        for i in range(len(self.data)):
            ET.SubElement(child, "DataSet", attrib=self.data[i])
        ET.indent(root)
        ET.ElementTree(root).write(path)

    def get_single_vtu_time(self, vtu_name):
        """Method returning the timestep of a certain vtu.
        Args:
            vtu_name    (str):      Name of the vtu-file. Only the name is considered, not the path!
        Returns:
            float:      Timestep corresponding to the vtu-file.
        """
        for i in range(len(self.data)):
            if vtu_name in self.data[i]["file"]:
                return float(self.data[i]["timestep"])
        raise RuntimeError(
            "The vtu-file can not be found in the corresponding pvd-file."
        )

    def get_timesteps_and_filenames(self):
        """Method returning the times and filenames of the vtu files.
        Returns:
            timesteps   (list):     List of floats with all simulation timesteps.
            filenames   (list):     List of strings with all simulation vtu names.
        """
        timesteps = []
        filenames = []
        for i in range(len(self.data)):
            timesteps.append(self.data[i]["timestep"])
            filenames.append(self.data[i]["file"])
        return timesteps, filenames

    def create_data_collection(self, timesteps, filenames):
        """Method creating the DataSet lines of the pvd file such that they can be written in consecutive steps.
        Args:
            timesteps   (list):     List of floats with all simulation timesteps.
            filenames   (list):     List of strings with all simulation vtu names.
        """
        self.data = []
        if len(timesteps) != len(filenames):
            raise RuntimeError(
                "Timesteps and filenames must have the same number of elements."
            )
        for i in range(len(timesteps)):
            self.data.append(
                {
                    "timestep": str(timesteps[i]),
                    "group": "",
                    "part": "0",
                    "file": filenames[i],
                }
            )
