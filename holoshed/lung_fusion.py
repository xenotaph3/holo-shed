import os
import re
from os.path import join as join
from pathlib import Path
from typing import Optional

import meshio
import numpy as np
import SimpleITK as sitk
from oct2py import octave
from scipy import spatial
from scipy.integrate import cumulative_trapezoid as cum_trapezoid

from .JsonDataIO import JsonDataIO as JsonDataIO
from .PvdIO import PvdIO as PvdIO
from .utils import Tet, get_element_center_coordinates
from .VTUReader import VTUReader as VTUReader


def lungFusion(
    config: dict,
    fmdl,
    write_voltage: bool = False,
    write_conductivity: Optional[bool] = False,
):
    """Function which sets up an EIDORS forward model with an inhomogeneous
     conductivity based on a patient specific mesh.
    The resulting forward model is then solved to obtain the electrode voltages.

    Args:
        config              (dict):     User defined configuration and parameters
                                        for the EIT simulation.
        fmdl    (oct2py.io.struct):     EIDORS forward model object.
        write_voltage       (bool):     Write simulated electrode voltages to
                                        ensight output.
        write_conductivity  (bool):     Defines whether to write the conductivity
                                        for debugging purpose

    Returns:
        config              (dict):     User defined configuration and parameters
                                        for the EIT simulation.
    """

    model = Model(config, fmdl, write_voltage)

    model.check_for_valid_vtu()

    model.read_global_quantities_from_sim_vtu()

    model.define_inhomogeneous_conductivities()

    if write_conductivity:
        model.write_conductivity()

    model.solve_inhom_forward_problem()

    model.save_simulated_voltages()

    return model.config


class Model:
    """Data format providing methods to run a forward simulation of EIT on a patient specific mesh.

    The simulation is conducted by the MATLAB/Octave based toolbox EIDORS (https://eidors3d.sourceforge.net/).
    To obtain an inhomogeneous distribution of the conductivity, the volumetric strain level of the alveolar clusters is mapped to the conductivity.

    Attributes:
        config                     (dict):      User defined configuration data.
        working_path                (str):      Path to the holoshed-specific in- and output folder structure (working_path/00_config, working_path/01_input, etc.)
        fmdl           (oct2py.io.struct):      EIDORS forward model.
        all_img_i                  (list):      List of all EIDORS image object. Each entry of this list corresponds to a certain time step.
        reconst_timesteps          (list):      Array containing the time steps. In case of adaptive time stepping they are not distributed uniformly.
        volt_data           (numpy.array):      Simulated voltages for all electrodes and all times.
        vtu_list                   (list):      List containing the relevant VTU-file paths.
        dynamic_vtu  (holoshed.VTUReader):      VTU reader containing the dynamic/time-dependent properties of the current VTU File
        static_vtu   (holoshed.VTUReader):      VTU reader containing the static/time-independent properties of the VTU files
        vtu_split_flag             (bool):      Flag determining if the VTU file is split into dynamic and static part.
        static_vtu_path             (str):      Path to the VTU file containing the static result data.
        ac_mapping          (numpy.array):      Array of integers mapping each tetrahedron to the nearest alveolar cluster.
        pvd_path                    (str):      Path to the PVD file, containing the timesteps and references to the individual vtus.
        write_voltage              (bool):      Command line option to write the voltage output for all timesteps in ensight format.
        all_vtus                   (dict):      Timestep and path of all VTUs that were found in the input directory.
        sim_duration_tot          (float):      Total duration of the simulation VTUs in seconds.
        glob_reco_times            (list):      Contains all the times where an EIT image is reconstructed.
        sim_init_time             (float):      Time at the initial simulation time frame.
        pvd              (holoshed.PvdIO):      Holoshed PvdIO object of the considered simulation.
    """

    def __init__(self, config, fmdl, write_voltage=False):
        """Method inintializing the class.

        Args:
            config              (dict):   User defined configuration and parameters for EIT simulation.
            fmdl    (oct2py.io.struct):   EIDORS forward model object.
            write_voltage       (bool):   Command line option to write the voltage output for all timesteps in ensight format.
        """
        self._config = config
        self.working_path = config["working_path"]
        self.fmdl = fmdl
        self.all_img_i = []
        if "reconst_timesteps" in config:
            self.reconst_timesteps = config["reconst_timesteps"]
        else:
            self.reconst_timesteps = []
        self.volt_data = np.array(0)
        self.vtu_list = []
        self.dynamic_vtu = None
        self.static_vtu = None
        self.vtu_split_flag = False
        self.static_vtu_path = ""
        self.ac_mapping = np.array(0)
        self.pvd_path = ""
        self.write_voltage = write_voltage
        self.all_vtus = None
        self.sim_duration_tot = 0
        self.glob_reco_times = []
        self._sim_init_time = None
        self._pvd = None

    @property
    def config(self):
        self.update_config()
        return self._config

    @property
    def sim_time_init(self):
        if not self._sim_init_time:
            init_timestep = np.min(list(self.all_vtus.keys()))
            self._sim_init_time = self.pvd.get_single_vtu_time(
                os.path.basename(self.all_vtus[init_timestep])
            )
        return self._sim_init_time

    @property
    def pvd(self):
        if not self._pvd:
            self._pvd = PvdIO.from_file(self.pvd_path)
        return self._pvd

    def load_ac_mask(self) -> Optional[sitk.Image]:
        """An AC mask specifies a mapping of 3D space to 0D ACs via a multilabel mask.

        The mask is provided as a sitk.Image multilabel mask, where the integer of
        each voxel corresponds to the 'ele_id' of each VTK element/cell representing the ACs."""

        mask = Path(self.working_path) / "01_input" / "ac_mask.nii.gz"

        if mask.exists():
            return sitk.ReadImage(str(mask))
        else:
            if (
                "use_volumetric_ac_tet_mapping" in self._config
                and self._config["use_volumetric_ac_tet_mapping"]
            ):
                raise RuntimeError(
                    f"Cannot comply with configuration `use_volumetric_at_tet_mapping : true`, since no AC mask exists at {mask}."
                )
            return None

    def check_for_valid_vtu(self):
        """Method checking if the vtu-files corresponding to the user defined timesteps exist in the input directory.
        Creates the attribute vtu_list containing the vtu-files which will be reconstructed. Additionally the attributes
        static_vtu and vtu_split_flag are set depending on the files in the input directory."""
        input_path = join(self.working_path, "01_input")
        vtus = {}
        if any(
            fname.endswith("constant.0.vtu")
            for fname in os.listdir(input_path)
        ):
            self.vtu_split_flag = True

        for p in os.listdir(input_path):
            if self.vtu_split_flag and p.endswith("constant.0.vtu"):
                if os.path.isfile(self.static_vtu_path):
                    raise RuntimeError(
                        "Error: More than one constant VTU exists in the input directory."
                    )
                self.static_vtu_path = join(input_path, p)
            elif p.endswith(".vtu"):
                try:
                    file_number = [int(s) for s in re.findall(r"\d+", p)][-1]
                    vtus[file_number] = join(input_path, p)
                except KeyError:
                    pass
            elif p.endswith(".pvd"):
                if os.path.isfile(self.pvd_path):
                    raise RuntimeError(
                        "More than one pvd-file exists in the input directory."
                    )
                self.pvd_path = join(input_path, p)

        vtu_indices = sorted(list(vtus.keys()))
        vtus_rearranged = {}
        for i in range(len(vtu_indices)):
            vtus_rearranged[vtu_indices[i]] = vtus[vtu_indices[i]]

        self.all_vtus = vtus_rearranged
        all_time_steps, total_vtu_list = zip(*sorted(vtus_rearranged.items()))

        if not self.config["reconst_timesteps"]:
            ind1 = all_time_steps.index(self.config["start_timestep"])
            ind2 = all_time_steps.index(self.config["end_timestep"]) + 1
            sampl = self.config["timestep_sampling"]

            self.reconst_timesteps = all_time_steps[ind1:ind2:sampl]
            self.vtu_list = total_vtu_list[ind1:ind2:sampl]
        else:
            assert all(
                ts in all_time_steps for ts in self.reconst_timesteps
            ), "For some reconst_timesteps there exists no VTU file."
            idx_reconst_vtus = [
                all_time_steps.index(i) for i in self.reconst_timesteps
            ]
            self.vtu_list = [total_vtu_list[i] for i in idx_reconst_vtus]

        if len(self.reconst_timesteps) < 2:
            raise RuntimeError(
                "Error: Please provide more than one VTU-file and more than one timestep."
            )
        if not os.path.isfile(self.pvd_path):
            raise RuntimeError(
                "The input folder must contain exactly one pvd-File."
            )
        if not self.vtu_split_flag:
            self.static_vtu_path = self.vtu_list[0]

        for ref_ts in self.config["reference_timestep"]:
            assert (
                ref_ts in self.reconst_timesteps
            ), f"Reference timestep {ref_ts} not included in the reconstruction timesteps. Check your input file."

    def read_global_quantities_from_sim_vtu(self):
        """Method reading global quantities from all vtu-files in the 01_input folder. The relevant global quantities
        are the simulation time, the tracheal pressure and the lung volume. As this is time consuming these values are
        stored in 06_global_postprocessing_results/*_global_quant_simulation.json to make them easily accessible during
        for later usage.
        """
        static_vtu = VTUReader.create(
            self.config, self.config["static_vtu_path"]
        )
        trach_idx = static_vtu.tracheal_idx

        vtu_ids = list(self.all_vtus.keys())
        vtu_ids.sort()

        flow = np.zeros((len(vtu_ids)))
        pres = np.zeros((len(vtu_ids)))
        sim_times = np.zeros((len(vtu_ids)))
        for i, idx in enumerate(vtu_ids):
            vtu = VTUReader.create(self.config, self.all_vtus[idx])
            flow[i] = vtu.get_tracheal_flow(trach_idx)
            pres[i] = vtu.get_ventil_pressure(trach_idx, flow[i])
            vtu_name = os.path.basename(self.all_vtus[idx])
            sim_times[i] = (
                self.pvd.get_single_vtu_time(vtu_name) - self.sim_time_init
            )

        pres_mBar = pres * 1e-2
        volume_l = cum_trapezoid(flow, sim_times) * 1e-6
        volume_l = np.insert(volume_l, 0, 0) + self.config["start_volume_sim"]
        global_quant_sim = {
            "sim_time_s": sim_times,
            "trach_pres_mBar": pres_mBar,
            "lung_vol_l": volume_l,
        }

        sim_json_path = join(
            self.config["working_path"],
            "06_global_postprocessing_results",
            self.config["sim_name"] + "_global_quant_simulation.json",
        )
        json_io = JsonDataIO(global_quant_sim)
        json_io.write(sim_json_path)
        self.sim_duration_tot = sim_times[-1] - sim_times[0]

        time_discrepancy = self.sim_duration_tot - (
            self.config["realtime_end"] - self.config["realtime_start"]
        )
        if not np.isclose(time_discrepancy, 0) and time_discrepancy < 0:
            raise ValueError(
                f"The simulation duration is shorter than the specified realtime duration by {abs(time_discrepancy)} seconds."
            )

    def define_inhomogeneous_conductivities(self):
        """Method conducting the mapping of AC strains and volume fractions to the electrical conductivities.
        This is done by reading the VTU files in the input directory for the user defined timesteps.
        Only the alveolar clusters (i.e. airways corresponding to the generation -1) are considered for this mapping."""
        img_template = octave.mk_image(
            self.fmdl, self.config["conductivity_thorax"], "conductivity"
        )
        if "tiss_IDs" in self.fmdl:
            for i in range(len(self.fmdl.tiss_IDs)):
                img_template.conductivity.elem_data[
                    self.fmdl.tiss_IDs[i]
                ] = self.config["conductivity_other_tissues"][i]
        img_template["time"] = np.nan

        img_template.lung_IDs = self.fmdl.lung_IDs
        self.static_vtu = VTUReader.create(self.config, self.static_vtu_path)
        self.map_ac_to_tetrahedra()

        self.all_img_i = []
        vtu_idx = 0
        for vtu_path in self.vtu_list:
            self.dynamic_vtu = VTUReader.create(self.config, vtu_path)
            self.all_img_i.append(img_template)
            self.glob_reco_times.append(
                self.pvd.get_single_vtu_time(os.path.basename(vtu_path))
                - self.sim_time_init
            )
            self.map_vol_strain_to_conduct(vtu_idx)
            vtu_idx += 1

    def write_conductivity(self):
        """Writes the conductivity of the finite element model to a series of .vtu files and a .pvd file."""
        path = join(self.working_path, "02_inhomogeneous_mesh")
        name_template = self.config["sim_name"] + "_cond_heterogeneous"

        nodes = self.fmdl.nodes
        elems = self.fmdl.elems.astype(int) - 1
        elems = elems.tolist()

        num_timesteps = len(self.all_img_i)
        vtu_names = []
        vtu_times = []
        for i in range(num_timesteps):
            conductivity = self.all_img_i[i].elem_data

            vtu_name = join(
                path,
                name_template
                + "_"
                + str(self.config["reconst_timesteps"][i])
                + ".vtu",
            )
            mesh = meshio.Mesh(
                points=nodes,
                cells={"tetra": elems},
                cell_data={"conductivity": [conductivity]},
            )
            meshio.write(vtu_name, mesh)
            vtu_names.append(vtu_name)
            vtu_times.append(self.glob_reco_times[i])

        pvd = PvdIO()
        pvd.create_data_collection(vtu_times, vtu_names)
        pvd.write(join(path, name_template + ".pvd"))
        print("Conductivity written to " + join(path, name_template + ".pvd"))

    def _volumetric_ac_tet_overlap_responsibilities(
        self,
        nodes: np.ndarray,
        elements: np.ndarray,
        ac_img: sitk.Image,
        tree: spatial.KDTree,
    ) -> np.ndarray:
        """Define a weighted mapping from tets to ACs on the basis of volumetric overlap (for filling factors).

           Note: the code emphasises reliability and simplicity, and is not runtime optimized.

        Args:
            elements        m x 4 array containing topology information of relevant tets within the lung
            nodes           p x 3 array containing all nodal coordinates
            ac_img          alveolar cluster mask using alveonX internal IDs
            tree            tree with alveolar cluster centres to use for nearest neighbor fallback

        Returns:
            R               responsibility matrix of size num_tets x num_ACs, with each row having unit-sum.
        """

        ele_ids = self.static_vtu.vtu.get_cell_data("ele_id", "line")
        N_tets = elements.shape[0]
        N_ele = len(ele_ids)  # all elements, not just ACs

        def alvx_index_to_internal(ac_index_alvx) -> int:
            # helper function - map alveonX ID to internal ID
            assert isinstance(ac_index_alvx, (int, np.int32, np.int64))
            return np.where(ele_ids == ac_index_alvx)[0].item()

        # this only contains attributions for tets within the lung mask
        R = np.zeros((N_tets, N_ele), np.float64)

        # keep some stats
        num_incomplete_coverage = 0
        num_no_coverage = 0
        missratio = []

        print(
            f"... finding volume weighted attribution for {N_tets} tets (this might take a while)"
        )
        attributed_acs = []
        for num_ele, element in enumerate(elements):

            # for each tet element identify the plausible index envelope
            tet_coords = nodes[element, :]
            tet_center = np.mean(tet_coords, 0)
            ind = np.array(
                [
                    ac_img.TransformPhysicalPointToIndex(
                        tet_coords[n, :].tolist()
                    )
                    for n in range(4)
                ]
            )
            indmin = ind.min(0)
            indmax = ind.max(0)

            if np.any(indmin == indmax):
                # for extreme geometries of tets, some dimension can collapse to a single voxel layer
                print(
                    f"Warning: Degenerate extent of wrapping indeces. INDMIN: {indmin} --- INDMAX: {indmax}"
                )
            if not np.all(indmax >= indmin):
                raise RuntimeError(
                    f"Error: Unexpected extent of wrapping indeces. INDMIN: {indmin} --- INDMAX: {indmax}"
                )

            # create tet element that allows to check whether point_within
            tet = Tet(tet_coords)

            points = []
            ac_inds = []
            # slow looping implementation - check for each voxel the AC attribution and whether it resides within the tet or not
            for n1 in range(indmin[0], indmax[0] + 1):
                for n2 in range(indmin[1], indmax[1] + 1):
                    for n3 in range(indmin[2], indmax[2] + 1):
                        ac_inds.append(ac_img[n1, n2, n3])
                        points.append(
                            ac_img.TransformIndexToPhysicalPoint((n1, n2, n3))
                        )

            ins = tet.points_inside(points)

            # find attribution of tet to ac elements
            ac_attrib, ac_attrib_count = np.unique(
                np.array(ac_inds)[ins], return_counts=True
            )
            ac_attrib, ac_attrib_count = (
                ac_attrib.flatten(),
                ac_attrib_count.flatten(),
            )

            # deal with special cases
            cast_alvx_internal = True
            # if no voxel centre-points reside within tet, or if any voxels within tet are background voxels
            if len(ac_attrib) == 0 or ac_attrib[0] == 0:
                #  if all voxel-centre points are assigned ID 0 (non-AC designator), or no voxel-centres contained within tet
                #  we fall back to nearest neighbor
                if len(ac_attrib) == 0 or len(ac_attrib) == 1:
                    if len(ac_attrib) == 0:
                        print(
                            f"Warning: Tet {num_ele} does not contain single voxel centre. Falling back to nearest neighbor."
                        )
                    else:
                        print(
                            f"Warning : Tet {num_ele} has zero overlap. Falling back to nearest neighbor."
                        )

                    idx_nearest = tree.query(tet_center)[1]
                    assert isinstance(
                        idx_nearest, (int, np.int32, np.int64)
                    ), "idx_nearest is not an integer"
                    ac_index = (
                        self.static_vtu.ac_indices[idx_nearest]
                        .astype("int")
                        .item()
                    )
                    ac_attrib = np.array([ac_index])
                    ac_attrib_count = np.array([1])
                    num_no_coverage += 1
                    fr = 1.0
                    cast_alvx_internal = False
                else:
                    # partial coverage (at least one voxel within tet had value 0)
                    ac_attrib_weight_tmp = ac_attrib_count / np.sum(
                        ac_attrib_count
                    )
                    fr = ac_attrib_weight_tmp[0]
                    num_incomplete_coverage += 1
                    ac_attrib = ac_attrib[1:]
                    ac_attrib_count = ac_attrib_count[1:]
            else:
                fr = 0.0

            missratio.append(fr)
            ac_attrib_weight = ac_attrib_count / np.sum(ac_attrib_count)
            assert np.isclose(
                np.sum(ac_attrib_weight), 1.0
            ), "Attribution weights do not sum up to one."

            # transform from alveonX cluster IDs to appropriate internal numbering
            if cast_alvx_internal:
                ac_attrib = [
                    alvx_index_to_internal(ac_id) for ac_id in ac_attrib
                ]
            else:
                # entries from nearest neighbor lookup do not require any additional mapping
                ac_attrib = [int(ac_id) for ac_id in ac_attrib]

            # keep a list of all alveolar cluster indeces that have come up
            attributed_acs += ac_attrib

            # define row within responsibility matrix by weight
            R[num_ele, ac_attrib] = ac_attrib_weight

        print("Tet-Ac attribution is done!")
        num_acs_exist = len(np.unique(sitk.GetArrayFromImage(ac_img)))
        num_acs_assigned = len(np.unique(np.array(attributed_acs)))

        print(f"... average miss ratio: {np.mean(missratio)}")
        print(
            f"... {num_acs_assigned} out of {num_acs_exist} ACs have been attributed."
        )
        print(
            f"... {num_incomplete_coverage} out of {N_tets} tets have incomplete coverage"
        )
        print(
            f"... {num_no_coverage} out of {N_tets} tets have no coverage (nearest neighbor fallback)"
        )

        assert np.all(
            np.isclose(np.sum(R, 1), 1.0)
        ), "Responsibility matrix is not properly normalized."

        return R

    def map_ac_to_tetrahedra(self):
        """Method responsible for the spatial mapping of the tetrahedra to the alveolar cluster.

        Three different algorithms are available:

            (1) Each Tet is mapped to multiple ACs with relative weights depending on volumetric overlap between Tet and ACs (using alveolar cluster mask)
            (2) Each Tet is mapped to the AC into which its centre of gravity falls into (using alveolar cluster mask)
            (3) Find nearest AC node for every tet. Both the tet as well as the AC are represented by its centre of gravity, with the latter assumed spherical.

        If an alveolar cluster mask is available, strategy (1) or (2) are chosen.

        The resulting ac_mapping is either a direct mapping defined by an integer array (2-3), or alternative a responsibility matrix where
        each row defines the volumetric overlaps of each tet with all ACs (i.e., num_tets x num_ele).
        """
        # get centers of tets of 3D torso mesh
        tet_elements = (
            self.fmdl.elems[self.fmdl.lung_IDs].astype("int16") - 1
        )  # necessary as the fmdl comes from the octave environment

        tet_center = get_element_center_coordinates(
            tet_elements, self.fmdl.nodes
        )

        ac_mask = self.load_ac_mask()
        tree = spatial.KDTree(self.static_vtu.ac_nodes)

        if (
            ac_mask is not None
            and "use_volumetric_ac_tet_mapping" in self._config
            and self._config["use_volumetric_ac_tet_mapping"]
        ):
            print(
                "... mapping 0D model properties to 3D torso mesh using specified AC mask and weighted volume overlaps."
            )

            # ac mapping is now defined by two-dimensional responsibility matrix with rows having unit-sum
            self.ac_mapping = self._volumetric_ac_tet_overlap_responsibilities(
                self.fmdl.nodes, tet_elements, ac_mask, tree
            )

        # if an AC mask has been specified, prefer this for attribution of material properties
        elif ac_mask is not None:
            print(
                "... mapping 0D model properties to 3D torso mesh using specified AC mask."
            )
            ele_ids = self.static_vtu.vtu.get_cell_data("ele_id", "line")
            ac_indices = list()
            num_assignment_fails = 0
            for center_coord in tet_center:
                # this is the ele_id of the AC
                ac_index_alvx = ac_mask.EvaluateAtPhysicalPoint(
                    center_coord.copy(), interp=sitk.sitkNearestNeighbor
                )
                assert (
                    int(ac_index_alvx) == ac_index_alvx
                ), "Mask returned a float value"
                ac_index_alvx = int(ac_index_alvx)
                if ac_index_alvx == 0:
                    idx_nearest = tree.query(center_coord)[1]
                    ac_index = (
                        self.static_vtu.ac_indices[idx_nearest]
                        .astype("int")
                        .item()
                    )
                    num_assignment_fails += 1
                else:
                    ac_index = np.where(ele_ids == ac_index_alvx)[0]

                assert self.static_vtu.is_ac[ac_index]
                ac_indices.append(ac_index)

            if num_assignment_fails > 0:
                print(
                    f"... had to fall back to nearest neighbor for {num_assignment_fails} out of {tet_center.shape[0]} tets."
                )

            self.ac_mapping = np.array(ac_indices, dtype="int").reshape(-1, 1)

            num_unique_acs = len(np.unique(self.ac_mapping))
            print(
                f"{tet_center.shape[0]} Tets within the lung have been mapped to {num_unique_acs} ACs"
            )
        else:
            print(
                "... mapping 0D model properties to 3D torso mesh using nearest neighbor of AC centers."
            )
            # inform mapping using nearest neighbor (centre of AC to centre of tetrahedral)
            idx_nearest = tree.query(tet_center)[1]
            self.ac_mapping = self.static_vtu.ac_indices[idx_nearest].astype(
                "int"
            )

            num_unique_acs = len(np.unique(self.ac_mapping))
            print(
                f"{tet_center.shape[0]} Tets within the lung have been mapped to {num_unique_acs} ACs"
            )

    def map_vol_strain_to_conduct(self, vtu_idx):
        """Method conducting the mapping of AC quantities. Results in a array of conductivities and the corresponding time step.
        For the mathematical background see Roth, C. J. et al. (2017): Coupling of EIT with computational lung modeling for predicting
        patient-specific ventilatory responses.

        Args:
            vtu_idx     (int):      Index of the current VTU file
        """
        FF = self.dynamic_vtu.get_filling_factor(self)

        resistivity = 1.71 / 0.7284 * (FF + 1.0)  # --> [Ohm*m]

        resistivity *= 1000  # --> [Ohm*mm]

        conduct = 1 / resistivity
        self.all_img_i[vtu_idx].conductivity.elem_data[
            self.all_img_i[vtu_idx].lung_IDs
        ] = conduct
        self.all_img_i[vtu_idx] = octave.physics_data_mapper(
            self.all_img_i[vtu_idx]
        )

        self.all_img_i[vtu_idx].time_step = self.reconst_timesteps[vtu_idx]
        return conduct

    def solve_inhom_forward_problem(self):
        """Method calling the EIDORS forward solver to obtain the electrode voltages."""

        self.volt_data = octave.zeros(
            np.square(self.config["num_electrodes"]), len(self.vtu_list)
        )
        print("Solving inhomogeneous forward problems.")
        for i in range(len(self.vtu_list)):
            img_i = self.all_img_i[i]
            img_i.fwd_solve.get_all_meas = 1
            vi = octave.fwd_solve(img_i)
            vi.meas = vi.meas * self.fmdl.no_meas

            self.volt_data.T[i, :] = vi.meas.T

            if self.write_voltage:
                octave.EnsightWrite(
                    join(
                        self.working_path,
                        "03_forward_results",
                        "eidors_Kiel_lung_heterogeneous",
                    ),
                    self.fmdl,
                    vi.volt,
                    i,
                )

    def save_simulated_voltages(self):
        """Stores the simulated volatages into 03_forward_results/*_elec_voltages.json for later usage."""
        sim_json_path = join(
            self.config["working_path"],
            "03_forward_results",
            self.config["sim_name"] + "_elec_voltages.json",
        )
        json_io = JsonDataIO({"volt_data": self.volt_data})
        json_io.write(sim_json_path)

    def update_config(self):
        """Adds model quantities into the config attribute for later reuse."""
        self._config["all_vtus"] = self.all_vtus
        self._config["reconst_timesteps"] = list(self.reconst_timesteps)
        self._config["glob_reco_times"] = np.array(self.glob_reco_times)
        self._config["vtu_split_flag"] = self.vtu_split_flag
        self._config["static_vtu_path"] = self.static_vtu_path
        self._config["sim_duration_tot"] = self.sim_duration_tot
