function [images] = call_mk_GREIT_model(img, config, vv, simulated_volt=true, detached_elec=[])
    % Function executing the GREIT algorithm and solving the inverse model.
    detached_elec = detached_elec + 1;
        
    opt.imgsz = config.GREIT_imgsz * [1 1];
    opt.distr = config.GREIT_distr;
    opt.Nsim = config.GREIT_Nsim;
    opt.target_size = config.GREIT_target_size;
    opt.target_offset = config.GREIT_target_offset;
    % select noise figure conditional on simulation or reference
    if simulated_volt
      opt.noise_figure = config.GREIT_noise_figure_simulation;
    else
      opt.noise_figure = config.GREIT_noise_figure_reference;
    end
    opt.square_pixels = config.GREIT_square_pixels;
    
    if isempty(detached_elec)
      imdl = mk_GREIT_model(img, 0.25, [], opt);
    else
      imdl = eidors_obj('inv_model','','fwd_model',img.fwd_model);
      imdl.element_data = img.elem_data;

      imdl.meas_icov_rm_elecs.elec_list = detached_elec;
      imdl.meas_icov_rm_elecs.exponent  = -1;
      imdl.meas_icov_rm_elecs.SNR       = 100;
      opt.noise_covar = meas_icov_rm_elecs(imdl);
    
    
      imdl = mk_GREIT_model(img, 0.25,  [], opt);
      imdl.meas_icov = meas_icov_rm_elecs(imdl, detached_elec);
    end
    fprintf('Reconstructing image for plane with z-coordinate %.3f mm \n', imdl.rec_model.mdl_slice_mapper.level(3))
    imdl.fwd_model.meas_select = logical(imdl.fwd_model.meas_select);
    [~, ind_ref] = ismember(config.reference_timestep, config.reconst_timesteps);
    if any(ind_ref == 0)
      missing_timesteps = config.reference_timestep(~ind_ref);
      error(['Not all reference timesteps are in the list of reconstruction timesteps. Missing timesteps: ' num2str(missing_timesteps)]);
    end
    vh = vv(:,ind_ref);
    vh = mean(vh,2);
    images = cell(size(vv,2),1);
    for i=1:size(vv,2)
        img_rec = inv_solve(imdl, real(vh), real(vv(:,i)));
        img_rec.elem_data = -(img_rec.elem_data);

        img_rec.fwd_model.inside = full(img_rec.fwd_model.inside);  % Conversion from sparse to full matrix
        img_rec.fwd_model.solve = [];
        img_rec.fwd_model.system_mat = [];
        img_rec.fwd_model.jacobian = [];
        img_rec.time_step = config.reconst_timesteps(i);
        img_rec.time = config.glob_reco_times(i);
        images{i} = img_rec;
    end
end

