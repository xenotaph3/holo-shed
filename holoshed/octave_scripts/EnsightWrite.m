function EnsightWrite(filename,fmdl,v,id)


filename = [filename, '_', num2str(id)];
[path, name] = fileparts(filename)
%% 1. Write .case file
case_filename = [filename,'.case'];
geo_filename = [filename,'.geo'];
volt_filename = [filename,'.volt'];

output_dir = fileparts(case_filename);
if ~exist(output_dir, 'dir')
   mkdir(output_dir);
end

fileID = fopen(case_filename,'w');
fprintf(fileID,'FORMAT\n');
fprintf(fileID,'type:    ensight gold\n\n');
fprintf(fileID,'GEOMETRY\n');
fprintf(fileID,'model:\t2\t1\t%s%s\n\n',name,'.geo');
fprintf(fileID,'VARIABLE\n');
fprintf(fileID,'scalar per node:\t1\t2\tvoltage\t%s%s\n\n',name, '.volt');
% Introducing pseudo time, which covers the actuation of all electrodes
ps_time = 0.01 * (0:size(v,2)-1);
ps_time_str = ['time values: ', sprintf('%.2f ', ps_time), '\n\n'];
fprintf(fileID,'TIME\n\n');
fprintf(fileID,'time set:\t1\n');
fprintf(fileID,'number of steps:\t%i\n', size(v,2));
fprintf(fileID,ps_time_str);
fprintf(fileID,'time set:\t2\n');
fprintf(fileID,'number of steps:\t1\n');
fprintf(fileID,'time values: 0\n\n');
fprintf(fileID,'FILE\n\n');
fprintf(fileID,'file set:\t1\n');
fprintf(fileID,'number of steps:\t1\n\n');
fprintf(fileID,'file set:\t2\n');
fprintf(fileID,'number of steps:\t%i\n', size(v,2));
fclose(fileID);


%% 2. Write .geo file
n_Nodes = size(fmdl.nodes,1);
node_IDs = [1:n_Nodes]';
x = fmdl.nodes(:,1);
y = fmdl.nodes(:,2);
z = fmdl.nodes(:,3);
n_Elems = size(fmdl.elems,1);
tetra = fmdl.elems;

fileID = fopen(geo_filename,'w');
fprintf(fileID,'BEGIN TIME STEP\n');
fprintf(fileID,'Describtion 1\n');
fprintf(fileID,'Describtion 2\n');
fprintf(fileID,'node id given\n');
fprintf(fileID,'element id off\n');
fprintf(fileID,'part\n\t1\n');
fprintf(fileID,'description part 1\n');
fprintf(fileID,'coordinates\n\t%i\n',n_Nodes);

fprintf(fileID,'\t%i\n',node_IDs);

fprintf(fileID,' %e\n',fmdl.nodes(:,1));
fprintf(fileID,' %e\n',fmdl.nodes(:,2));
fprintf(fileID,' %e\n',fmdl.nodes(:,3));

fprintf(fileID,'tetra4\n\t%i\n',n_Elems);

for i=1:n_Elems
    fprintf(fileID,'\t%i\t%i\t%i\t%i\n',tetra(i,1),tetra(i,2),tetra(i,3),tetra(i,4));
end

fprintf(fileID,'END TIME STEP\n');
fclose(fileID);


%% 3. Write .volt file for 16 patterns
fileID = fopen(volt_filename,'w');
for i=1:size(v,2)
    v_pattern = v(:,i);

    fprintf(fileID,'BEGIN TIME STEP\n');
    fprintf(fileID,'description\n');
    fprintf(fileID,'part\n\t1\n');
    fprintf(fileID,'coordinates\n');

    fprintf(fileID,'%e\n',v_pattern);

    fprintf(fileID,'END TIME STEP\n\n');
end
fclose(fileID);

end
