import glob
import json
import os
import pprint
import warnings
from os.path import join as join

import numpy as np
from oct2py import octave

from .MeasEitReader import MeasEitReader
from .utils import get_working_path, repo_base_path


def initialize_holoshed(config_path):
    """Initializes holoshed by setting up the config dictionary. Additionally, the required folder structure
    for the simulation output is generated.
    Args:
        config_path     (str):      Path of the config.json file.
    Returns:
        config         (dict):      User defined configuration and parameters for the EIT simulation.
    """
    initializer = HoloshedInitializer(config_path)

    initializer.check_config()

    initializer.initialize_folder_structure()
    initializer.initialize_octave()

    initializer.check_for_valid_realtimeValues()
    if initializer.config["reconstruct_from_meas_data"]:
        initializer.check_for_valid_eit_meas()
    if not initializer.config["reconstruct_from_sim_data"]:
        initializer.create_dummy_simulation_time()
    return initializer.config


class HoloshedInitializer:
    """Class initializing Holoshed. After reading the user defined config.json file, the user input is checked and the
    the necessary input files are searched in the input folder.

    Attributes:
        config                  (dict):     User defined configuration and parameters for the EIT simulation.
        base_path                (str):     Path to the holoshed base directory.
    """

    def __init__(self, config_path):
        with open(config_path) as ff:
            self._config = json.load(ff)
        self._config["working_path"] = get_working_path(config_path)
        self.base_path = repo_base_path()
        if "eidors_cache_size" in self.config:
            self._eidors_cache_size = self.config["eidors_cache_size"]
        else:
            self._eidors_cache_size = 2.0
        if "reference_timestep" in self.config and not isinstance(
            self.config["reference_timestep"], list
        ):
            self._config["reference_timestep"] = [
                self.config["reference_timestep"]
            ]
            print(
                "Deprecation Warning: Your integer valued 'reference_timestep' was transformed to a list. In future this might cause an error. Consider changing the 'reference_timestep' to a list of several timesteps. This should also improve the quality of your reconstructions."
            )

        print("Configuration:\n")
        pp = pprint.PrettyPrinter()
        pp.pprint(self.config)

    @property
    def config(self):
        """Method returning the current value of config.
        Returns:
            config  (dict):     User defined configuration and parameters for the EIT simulation
        """
        return self._config

    def check_config(self):
        """Method checking a correct user input in the config.json. Function throws an error otherwise.
        This is to be implemented. Checking for the most common input errors before the execution of holoshed.
        increases the speed to get to a ready-to-go config file."""
        if not isinstance(self.config["filter_order"], int):
            raise TypeError('The "filter_order" needs to be an integer.')
        if self.config["filter_type"] in ["lowpass", "highpass"]:
            if not isinstance(self.config["cutoff_frequency"], float):
                raise TypeError(
                    '"lowpass" and "highpass" filters require a single float as cutoff frequency.'
                )
        elif self.config["filter_type"] in ["bandpass", "bandstop"]:
            if not (
                isinstance(self.config["cutoff_frequency"], list)
                and len(self.config["cutoff_frequency"]) == 2
            ):
                raise RuntimeError(
                    '"bandpass" and "bandstop" filters require a list of two frequencies.'
                )
        else:
            raise ValueError(
                'No valid "filter_type" provided. Possible options are "lowpass", "highpass", "bandpass", "bandstop". The most suitable filter for EIT applications is the lowpass filter with a cutoff frequency of 6.7 Hz.'
            )

        if self.has_param("num_measurements_for_reference"):
            if (
                self.config["num_measurements_for_reference"] < 1
                or self.config["num_measurements_for_reference"] > 20
            ):
                raise ValueError(
                    "Please choose 'num_measurements_for_reference' between 1 and 20. Averaging over larger intervals to obtain a reference does not make sense."
                )

        if (
            not self.config["reconstruct_from_sim_data"]
            and not self.config["reconstruct_from_meas_data"]
        ):
            raise ValueError(
                "Please set at least one of the two parameters 'reconstruct_from_sim_data' and 'reconstruct_from_meas_data' to 'true'."
            )

        if self.config["reconstruct_from_sim_data"]:
            params_without_effect = [
                "meas_reconst_frequency",
                "realtime_reference",
                "realtime_reconst_times",
            ]
            for param in params_without_effect:
                if self.has_param(param):
                    warnings.warn(
                        f"You provided a value for the parameter for '{param}'. As you are running holoshed also for simulation data, this parameter is ignored. Be aware!"
                    )
        else:
            if not self.has_param("realtime_reference"):
                raise RuntimeError(
                    "Please provide a value for the 'realtime_reference'."
                )
        if self.has_all_params(
            ["meas_reconst_frequency", "realtime_reconst_times"]
        ):
            warnings.warn(
                "As 'realtime_reconst_times' has a value, the variable 'meas_reconst_frequency' is ignored!"
            )
        if (
            self.has_at_least_one_of_the_params(
                ["start_timestep", "end_timestep", "timestep_sampling"]
            )
        ) and self.has_param("reconst_timesteps"):
            warnings.warn(
                "You defined 'reconst_timesteps'. Therefore the values of 'start_timestep', 'end_timestep', and 'timestep_sampling' are ignored."
            )
        if self.has_param("reconst_timesteps"):
            if not self.list_is_ascending("reconst_timesteps"):
                raise ValueError(
                    "'reconst_timesteps' must be in ascending order"
                )
        if self.has_param("realtime_reconst_times"):
            if not self.list_is_ascending("realtime_reconst_times"):
                raise ValueError(
                    "'realtime_reconst_times' must be in ascending order"
                )
        # TODO also check other user defined parameters in config.json

    def initialize_folder_structure(self):
        """Method initializing the in-/output folder structure. If holoshed is executed in an existing folder structure,
        the preprocessed measurement data in 05_measured_reconstruction_results/eit_meas.json is deleted to avoid reading
        deprecated data.
        """
        dirs = [
            "02_inhomogeneous_mesh",
            "03_forward_results",
            "04_simulated_reconstruction_results",
            "05_measured_reconstruction_results",
            "06_global_postprocessing_results",
        ]
        for output_dir in dirs:
            dir_path = join(self.config["working_path"], output_dir)
            if not os.path.exists(dir_path):
                os.makedirs(dir_path)

        eit_meas_json_path = join(
            self.config["working_path"],
            MeasEitReader.EIT_MEAS_CACHE_PATH,
        )
        if (
            os.path.exists(eit_meas_json_path)
            and self.config["reconstruct_from_meas_data"]
        ):
            os.remove(eit_meas_json_path)  # To prevent reading deprecated data

    def initialize_octave(self):
        """Initializes paths to make the octave scripts runnable. Executes the EIDORS startup function."""
        startup_path = join(self.config["eidors_path"], "startup.m")
        print(f"Initializing octave by running script: {startup_path}")
        octave.run(startup_path, nout=0)
        octave.addpath(join(self.base_path, "holoshed", "octave_scripts"))
        octave.addpath(self.config["eidors_path"])
        octave.eidors_cache(
            "cache_size", self._eidors_cache_size * np.power(1024, 3)
        )

    def check_for_valid_realtimeValues(self):
        """Checks if the input folder contains exactly one realtimeValues*.csv. Throws an error if this is not the case."""

        realtime_list = glob.glob1(
            join(self.config["working_path"], "01_input"),
            "realtimeValues*.csv",
        )
        if len(realtime_list) == 1:
            self._config["realtime_values_path"] = join(
                self.config["working_path"], "01_input", realtime_list[0]
            )
        elif len(realtime_list) > 1:
            raise RuntimeError(
                "Error. There are multiple realtimeValue CSVs in the input directory. Please make sure that only one 'realtimeValues*.csv' exists in directory."
            )
        else:
            raise RuntimeError(
                "Error. There exists no 'realtimeValues*.csv' in the input directory."
            )

    def check_for_valid_eit_meas(self):
        """Checks if the input folder contains at least one eit file. Throws an error if this is not the case."""
        self._config["eit_meas_list"] = sorted(
            glob.glob1(join(self.config["working_path"], "01_input"), "*.eit")
        )
        if len(self.config["eit_meas_list"]) < 1:
            raise RuntimeError(
                "Error. There exists no '*.eit' in the input directory."
            )
        self._config["eit_meas_list"] = [
            join(self.config["working_path"], "01_input", eit)
            for eit in self.config["eit_meas_list"]
        ]

    def create_dummy_simulation_time(self):
        """In the later execution of holoshed, the simulation time is considered as the global time. All other
        times are synchronized with respect to the simulation time. If the user is running holoshed without
        simulation data, this method creates a dummy time serving as the simulation reference time."""
        time_duration = (
            self.config["realtime_end"] - self.config["realtime_start"]
        )
        if "realtime_reconst_times" not in self.config:
            num_steps = (
                int(time_duration * self.config["meas_reconst_frequency"])
            ) + 1
            self._config["reconst_timesteps"] = np.arange(num_steps).tolist()
            self._config["glob_reco_times"] = np.linspace(
                0, time_duration, num_steps
            )
        else:
            num_steps = int(time_duration * 100)
            self._config["glob_reco_times"] = list(
                np.array(self.config["realtime_reconst_times"])
                - self.config["realtime_start"]
            )
            self._config["reconst_timesteps"] = [
                int(np.round(time * 100))
                for time in self.config["glob_reco_times"]
            ]

        self._config["num_pseudo_timesteps"] = num_steps

        if not isinstance(self.config["realtime_reference"], list):
            self.config["realtime_reference"] = [
                self.config["realtime_reference"]
            ]

        self._config["reference_timestep"] = []
        for rt_ref in self.config["realtime_reference"]:
            self._config["reference_timestep"].append(
                int(
                    np.round(
                        (rt_ref - self.config["realtime_start"])
                        / time_duration
                        * num_steps
                    )
                )
            )
        self._config["prestress_time"] = 0.0

    def has_param(self, param):
        """Checks if self.config has a certain parameter key.
        Args:
            param   (str):  Parameter keyword in config.json.
        Returns:
            (bool):     self.config has a parameter.
        """
        return param in self.config

    def has_at_least_one_of_the_params(self, param_list):
        """Checks if self.config has at least one parameter of a parameter list.
        Args:
            param_list   (list):  List of strings containing possible parameter keywords.
        Returns:
            (bool):     self.config contain one of the parameters of the list.
        """
        for param in param_list:
            if self.has_param(param):
                return True
        return False

    def has_all_params(self, param_list):
        """Checks if self.config has all parameters of a parameter list.
        Args:
            param_list   (list):  List of strings containing possible parameter keywords.
        Returns:
            (bool):     self.config contains all of the parameters of the list.
        """
        return all(param in self.config for param in param_list)

    def list_is_ascending(self, param):
        """Checks if a list in self.config is in ascending order.
        Args:
            param   (str):  Parameter keyword in config.json.
        Returns:
            (bool):     The list is in ascending order.
        """
        return all(
            self.config[param][i] < self.config[param][i + 1]
            for i in range(len(self.config[param]) - 1)
        )
