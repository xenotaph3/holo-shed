import numpy as np
import pandas as pd
from scipy.integrate import cumulative_trapezoid as cum_trapezoid


class RealtimeReader:
    """Class providing methods to read medical ventilation data from csv files.
    By default the ventilation data is stored in realtimeValues*.csv

    Attributes:
        realtime_path   (str):  Path of the realtimeValues*.csv which is considered.
        init_volume   (float):  Initial lung volume of the measurement in liter defined in the config.
        idx_start       (int):  Starting index of the relevant time interval in the realtimeValues*.csv
        idx_end         (int):  Ending index of the relevant time interval in the realtimeValues*.csv
        rel_time   (np.array):  Relative time of from the beginning of the considered time interval in seconds.
    """

    def __init__(self, config):
        """Method initializing the RealtimeReader class."""
        self.realtime_path = config["realtime_values_path"]
        self.init_volume = config["start_volume_meas"]
        self.data = pd.read_csv(self.realtime_path)

        time = pd.to_datetime(
            self.data["Timestamp"], format="%Y.%m.%d %H:%M:%S.%f"
        )
        time = time - time[0]
        time = time.dt.total_seconds().to_numpy()

        self.idx_start = np.argmax(time >= config["realtime_start"])
        self.idx_end = np.argmax(time >= config["realtime_end"]) + 1
        time = time[self.idx_start : self.idx_end]
        self.rel_time = time - time[0]

    def get_relative_time(self):
        """Method returning the realtive time with respect to the user defined time interval.
        Returns:
            (np.array):     Relative time from with respect to the user defined time interval in seconds."""
        return self.rel_time

    def get_pressure_mbar(self):
        """Method returing the ventilation pressure in mBar.
        Returns:
            (np.array): Vector of external ventilation pressure for the user defined time interval in mBar."""
        return self.data["Pressure [mbar]"].to_numpy()[
            self.idx_start : self.idx_end
        ]

    def get_volume_l(self):
        """Method returning the total lung volume based on the flow measurements of the ventilation device.
        The volume flow is integrated over time. The user defined "start_volume_meas" serves as the initial
        value for the time integration.
        Returns:
            volume_l  (np.array):  Vector containing the total lung volume for the user defined interval in liter."""
        flow_l_p_min = self.data["Flow [l/min]"].to_numpy()[
            self.idx_start : self.idx_end
        ]
        volume_l = cum_trapezoid(flow_l_p_min / 60, self.rel_time)
        volume_l = np.insert(volume_l, 0, 0)
        volume_l += self.init_volume
        return volume_l
