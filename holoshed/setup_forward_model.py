import glob
from os.path import join as join

import meshio
import numpy as np
from oct2py import Cell, Struct, octave

from .JsonDataIO import JsonDataIO as JsonDataIO
from .utils import ismember_row


def setup_forward_model(config):
    """Setting up the EIDORS forward model object based on the thorax mesh and the user defined stimulation/measurement settings.
    Args:
        config           (dict):     User defined configuration and parameters for the EIT simulation.
    Returns:
        fmdl (oct2py.io.struct):     EIDORS forward model object.
    """

    mesh_file = check_for_valid_mesh(config["working_path"])
    fmdl, topo_left_lung, topo_right_lung = mesh2EIDORSfmdl(mesh_file, config)

    write_lung_mesh_to_cache(
        config["working_path"], topo_left_lung, topo_right_lung, fmdl.nodes
    )
    fmdl = define_stimulation_pattern(fmdl, config)

    idx = np.concatenate(
        (fmdl.mat_idx[0][1] - 1, fmdl.mat_idx[0][2] - 1), axis=0
    )  # both lung lobes
    idx = idx.flatten().astype(int)
    fmdl.lung_IDs = idx
    if len(fmdl.mat_idx[0]) > 3:
        fmdl.tiss_IDs = determine_IDs_of_other_tissues(fmdl, config)
    return fmdl


def check_for_valid_mesh(working_path):
    """Checks if the input folder contains exactly one mesh file. Throws an error if this is not the case.
    Args:
        working_path    (str):      Path of the current working directory.
    """
    mesh_list = glob.glob1(join(working_path, "01_input"), "*.mesh")
    if len(mesh_list) == 1:
        mesh_file = join(working_path, "01_input", mesh_list[0])
        return mesh_file
    elif len(mesh_list) > 1:
        raise RuntimeError(
            "Error. There are multiple mesh-files in the input directory. Please make sure that only one mesh exists in directory."
        )
    else:
        raise RuntimeError(
            "Error. There exists no mesh-file in the input directory."
        )


def mesh2EIDORSfmdl(mesh_file, config):
    """Converts the mesh file to an EIDORS forward model object and extracts the node topologies of the lung lobes.
    Args:
        mesh_file (str):     Path to the mesh file.
        config    (dict):    User defined configuration and parameters for the EIT simulation.
    Returns:
        fmdl          (oct2py.io.struct):     EIDORS forward model object.
        topo_left     (np.array):            Triangle topology of the left lung surface.
        topo_right    (np.array):            Triangle topology of the right lung surface.
    """
    name = mesh_file.split("/")[-1].split(".")[0]
    fmdl = octave.eidors_obj("fwd_model", name)
    mesh = meshio.read(mesh_file)
    fmdl.nodes = mesh.points
    fmdl.elems = mesh.cells_dict["tetra"] + 1

    fmdl.boundary = outer_surface(fmdl.elems)
    fmdl.gnd_node = np.linalg.norm(mesh.points, axis=1).argmin() + 1

    # Extract electrode surfaces
    n_elec = config["num_electrodes"]
    assert (
        len(config["surfaceID_elec"]) == n_elec
    ), "Error. The number of electrodes in the config file does not match the number of electrode surfaces in the mesh."
    fmdl.electrode = np.empty(
        (n_elec,), dtype=[("nodes", np.ndarray), ("z_contact", float)]
    )
    fmdl.electrode = fmdl.electrode.view(np.recarray)
    for i in range(1, n_elec + 1):
        ele_tri = np.where(
            mesh.cell_data_dict["medit:ref"]["triangle"]
            == config["surfaceID_elec"][i - 1]
        )[0]
        ele_nodes = np.unique(mesh.cells_dict["triangle"][ele_tri].reshape(-1))
        fmdl.electrode[i - 1] = (ele_nodes + 1, 0.01)

    # Extract material indices
    mat_idx = []
    for i, volumeID in enumerate(config["volumeID_mat"]):
        ids = (
            np.where(mesh.cell_data_dict["medit:ref"]["tetra"] == volumeID)[0]
            + 1
        )
        mat_idx.append(ids.reshape(-1, 1))
    fmdl.mat_idx = Cell([mat_idx])

    # Set default values
    fmdl.solve = "eidors_default"
    fmdl.jacobian = "eidors_default"
    fmdl.system_mat = "eidors_default"
    fmdl.normalize_measurements = 0
    fmdl.np_fwd_solve = Struct()
    fmdl.np_fwd_solve.perm_sym = "{n}"

    # Extract lung surfaces
    left_lung_tri_ids = np.where(
        np.isin(
            mesh.cell_data_dict["medit:ref"]["triangle"],
            config["surfaceID_lungs"][0],
        )
    )[0]
    left_lung = mesh.cells_dict["triangle"][left_lung_tri_ids] + 1
    right_lung_tri_ids = np.where(
        np.isin(
            mesh.cell_data_dict["medit:ref"]["triangle"],
            config["surfaceID_lungs"][1],
        )
    )[0]
    right_lung = mesh.cells_dict["triangle"][right_lung_tri_ids] + 1
    return fmdl, left_lung, right_lung


def outer_surface(elems):
    """Extracts the nodes of the outer surface of the mesh. The outer surface is defined by the faces of the tetrahedra that are only part of one tetrahedron.
    Args:
        elems (np.array):           Element topology of the mesh.
    Returns:
        outer_faces (np.array):     Node indices of the outer surface.
    """

    allF = np.vstack(
        [
            np.hstack((elems[:, 0:1], elems[:, 1:2], elems[:, 2:3])),
            np.hstack((elems[:, 0:1], elems[:, 2:3], elems[:, 3:4])),
            np.hstack((elems[:, 0:1], elems[:, 3:4], elems[:, 1:2])),
            np.hstack((elems[:, 1:2], elems[:, 3:4], elems[:, 2:3])),
        ]
    )
    sortedF = np.sort(allF, axis=1)

    u, counts = np.unique(sortedF, axis=0, return_counts=True)

    sorted_exteriorF = u[counts == 1]
    exteriorF = ismember_row(sortedF, sorted_exteriorF)
    return allF[np.where(exteriorF)]


def write_lung_mesh_to_cache(working_path, topo_left, topo_right, nodes):
    """Method writing the node topologies of both lung lobes and the node coordinates to the 02_inhomogeneous_mesh/lung_topology.json cache file.
    Args:
        working_path     (str):     Path to the current working directory
        topo_left   (np.array):     Triangle topology of the left lung surface.
        topo_right  (np.array):     Triangle topology of the right lung surface.
        nodes       (np.array):     Coordinates of the mesh nodes.
    """
    lung_topology_path = join(
        working_path,
        "02_inhomogeneous_mesh",
        "lung_topology.json",
    )
    json_io = JsonDataIO(
        {"left_lung": topo_left, "right_lung": topo_right, "nodes": nodes}
    )
    json_io.write(lung_topology_path)


def define_stimulation_pattern(fmdl, config):
    """Method defining the electrode stimulation based on the user input in the config-file.
    Args:
        fmdl    (oct2py.io.struct):     EIDORS forward model object.
        config              (dict):     User defined configuration and parameters for the EIT simulation.
    Returns:
        fmdl    (oct2py.io.struct):     EIDORS forward model object.
    """
    _, fmdl.no_meas = octave.mk_stim_patterns(
        config["num_electrodes"],
        config["num_rings"],
        config["inject_pattern"],
        config["meas_pattern"],
        {config["meas_current"]},
        config["amp"],
        nout=2,
    )
    stim, meas = octave.mk_stim_patterns(
        config["num_electrodes"],
        config["num_rings"],
        config["inject_pattern"],
        config["meas_pattern"],
        {"meas_current"},
        config["amp"],
        nout=2,
    )
    fmdl.stimulation = stim
    fmdl.meas_select = meas

    return fmdl


def determine_IDs_of_other_tissues(fmdl, config):
    """Determins the element IDs of other tissues that are provided as separate volumes in the mesh.
    Args:
        fmdl    (oct2py.io.struct):     EIDORS forward model object.
        config              (dict):     User defined configuration and parameters for the EIT simulation.
    Returns:
        tiss_IDs            (list):     List of element IDs of the other tissues.
    """
    num_other_tissues = len(fmdl.mat_idx[0]) - 3
    assert num_other_tissues == len(
        config["conductivity_other_tissues"]
    ), "Error. The number of other tissues in the mesh does not match the number of conductivity values in the config file."
    tiss_IDs = []
    for i in range(num_other_tissues):
        idx = fmdl.mat_idx[0][i + 3] - 1
        idx = idx.flatten().astype(int)
        tiss_IDs.append(idx)
    return tiss_IDs
