"""Module providing parameters for the corresponding tubus version."""


def get_guttmann_constants(config, positive_flow):
    """Function returning the constants corresponding to the provided tubus version according to
    Guttmann et. al. (1993): Continuous calculation of intratracheal pressure in tracheally intubated patients."""
    if config["tubus"] == "mallinckrodt_107_80":
        if positive_flow:
            k1 = 6.57 * 0.980665 * 1e2
            k2 = 1.94
        else:
            k1 = 7.50 * 0.980665 * 1e2
            k2 = 1.75
    else:
        raise RuntimeError(
            "Error. The provided tubus is not yet implemented. Do so!"
        )
    return k1, k2
