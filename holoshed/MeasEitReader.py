import os

import numpy as np
from oct2py import octave
from scipy import signal

from .JsonDataIO import JsonDataIO as JsonDataIO
from .utils import get_time_mapping_indices


class MeasEitReader:
    """Class reading and processing measured EIT data from the .eit file format.

    Attributes:
        config                  (dict):     User defined configuration and parameters for the EIT simulation.
        _detached_electrodes    (list):     Indices of the detached electrodes. Counting from 0. --> Range [0, n_ele-1]
        _imped_thresh           (list):     Minimal and maximal threshold for the categorization of the contact impedance.
        _high_imped_electrodes  (list):     Indices of the electrodes with high impedance. Counting from 0. --> Range [0, n_ele-1]
        _faulty_ch          (np.array):     Binary mask of faulty measurement channels (over n_ele**2 channels). This is only used as additional output as
                                            EIDORS handles faulty data automatically by setting the detached electrodes.
        eit_meas                (dict):     Preprocessed measurement data, e.g. channel voltage, relative time, reconstruction times, detached electrodes ...
        elec_imped          (np.array):     Contact impedance for n_ele electrodes and all time frames.
    """

    EIT_MEAS_CACHE_PATH = os.path.join(
        "05_measured_reconstruction_results", "eit_meas.json"
    )

    def __init__(self, config):
        """Method initializing the MeasEitReader class. Stores the data into 05_measured_reconstruction_results/eit_meas.json.
        If MeasEitReader was once called in the execution of Holo-Shed, the file eit_meas.json already exists and thus the time
        consuming reading of the raw data is avoided.
        Args:
            config      (dict):     User defined configuration and parameters for the EIT simulation.
        """
        self.config = config
        self._detached_electrodes = None
        self._imped_thresh = None
        self._faulty_ch = None
        self._high_imped_electrodes = None

        meas_json_path = os.path.join(
            config["working_path"], self.EIT_MEAS_CACHE_PATH
        )
        if not os.path.exists(meas_json_path):
            self.eit_meas = self.read_raw_eit(config)
            json_io = JsonDataIO(self.eit_meas)
            json_io.write(meas_json_path)

        else:
            json_io = JsonDataIO()
            self.eit_meas = json_io.read(meas_json_path)

    @property
    def detached_electrodes(self):
        if self._detached_electrodes is None:
            (
                self._detached_electrodes,
                self._imped_thresh,
                self._high_imped_electrodes,
            ) = self.determine_detached_electrodes()
        return self._detached_electrodes

    @property
    def imped_thresh(self):
        if self._imped_thresh is None:
            (
                self._detached_electrodes,
                self._imped_thresh,
                self._high_imped_electrodes,
            ) = self.determine_detached_electrodes()
        return self._imped_thresh

    @property
    def high_imped_electrodes(self):
        if self._high_imped_electrodes is None:
            (
                self._detached_electrodes,
                self._imped_thresh,
                self._high_imped_electrodes,
            ) = self.determine_detached_electrodes()
        return self._high_imped_electrodes

    @property
    def faulty_ch(self):
        if self._faulty_ch is None:
            self._faulty_ch = self.determine_faulty_channels()
        return self._faulty_ch

    def read_raw_eit(self, config):
        """Read the voltage and impedance data from the binary .eit file. This is mainly based on the eidors_readdata.m function.
        As the measurement device stores the binary data into multiple files of 10 min at max, all eit files are read in consecutive order.
        The reader stops if the time user defined time interval is reached.
        Args:
            config      (dict):     User defined configuration and parameters for the EIT simulation.

        Returns:
            eit_meas    (dict):     Preprocessed measurement data, e.g. channel voltage, relative time, reconstruction times, detached electrodes ...
        """
        time_start = config["realtime_start"] + config["eit_realtime_offset"]
        time_end = config["realtime_end"] + config["eit_realtime_offset"]

        meas_duration = [0]
        vv = np.empty(0)
        t_abs = np.empty(0)
        elec_imped = np.empty(0)
        for eit_path in sorted(config["eit_meas_list"]):
            vv_old = vv
            t_abs_old = t_abs
            elec_imped_old = elec_imped
            vv, aux, _ = octave.eidors_readdata(eit_path, "LQ4", [], nout=3)
            t_abs = aux.t_abs[0]
            elec_imped = aux.elec_impedance
            if (
                t_abs[1] - t_abs[0]
            ) * 3600 * 24 > 1:  # necessary bc of strange outlier in SMART_E2/20210707094239.eit
                t_abs = t_abs[1:]
                vv = vv[:, 1:]
                elec_imped = elec_imped[:, 1:]

            meas_duration.append((t_abs[-1] - t_abs[0]) * 3600 * 24)
            if sum(meas_duration) >= time_end:
                break

        tot_duration_prev_eit = sum(meas_duration[:-1])
        if tot_duration_prev_eit > time_start:
            vv = np.concatenate((vv_old, vv), axis=1)
            t_abs = np.concatenate((t_abs_old, t_abs))
            elec_imped = np.concatenate((elec_imped_old, elec_imped), axis=1)
            tot_duration_prev_eit = sum(meas_duration[:-2])

        eit_time = (t_abs - t_abs[0]) * 3600 * 24
        vv = self.apply_meas_pattern_to_voltages(vv)
        if config["filter_type"] is not None:
            vv = self.filter_voltages(
                vv,
                eit_time,
                config["filter_type"],
                config["filter_order"],
                config["cutoff_frequency"],
            )
        eit_start_time = time_start - tot_duration_prev_eit
        eit_end_time = time_end - tot_duration_prev_eit

        eit_start_idx = np.argmax(eit_time > eit_start_time)
        eit_end_idx = np.argmax(eit_time > eit_end_time)

        vv = vv[:, eit_start_idx:eit_end_idx]
        self.electrode_imped = elec_imped[:, eit_start_idx:eit_end_idx]
        eit_time = (
            eit_time[eit_start_idx:eit_end_idx] - eit_time[eit_start_idx]
        ) + config["prestress_time"]

        # reconst_ids may have duplicate integer entries depending on the sampling
        reconst_ids = get_time_mapping_indices(
            config["glob_reco_times"], eit_time
        )

        eit_meas = {
            "vv": vv,
            "relative_eit_time": eit_time,
            "detached_electrodes": self.detached_electrodes,
            "imped_thresh": self.imped_thresh,
            "electrode_imped": self.electrode_imped,
            "reco_times": (reconst_ids, config["glob_reco_times"].tolist()),
            "faulty_ch": self.faulty_ch,
        }
        return eit_meas

    def filter_voltages(
        self, vv, eit_time, filt_type, filt_order, filt_cutoff
    ):
        """Temporal frequency filter of the electrode channels by user defined filter settings.
        Args:
            vv                 (np.array):      Complex array of voltage measurements with size (n_ele^2) x n_frames.
            eit_time           (np.array):      Measurement time in seconds.
            filt_type               (str):      Type of Butterworth filter, i.e. lowpass, highpass, bandpass, bandstop
            filt_order              (int):      Order of the filter.
            filt_cutoff    (float / list):      Cutoff-frequency of the filter. May be float or listof two floats depending
                                                on which filter type is used.
        Returns:
            vv_filt     (np.array):     Frequency filtered voltage measurements.
        """
        fs = (len(eit_time) - 1) / eit_time[-1]
        nyquist_freq = 0.5 * fs
        normalized_cutoff = filt_cutoff / nyquist_freq

        b, a = signal.butter(
            filt_order,
            normalized_cutoff,
            btype=filt_type,
            analog=False,
        )
        vv_filt = signal.filtfilt(b, a, vv, axis=1)

        return vv_filt

    def apply_meas_pattern_to_voltages(self, vv):
        """Applies the binary measurement pattern mask to the raw voltage data.
        Args:
            vv  (np.array):     Array containing the voltage data that needs to be masked.
        Returns:
            (np.array):     Masked voltage array.
        """
        _, meas = octave.mk_stim_patterns(
            self.config["num_electrodes"],
            self.config["num_rings"],
            self.config["inject_pattern"],
            self.config["meas_pattern"],
            {self.config["meas_current"]},
            self.config["amp"],
            nout=2,
        )
        return np.multiply(vv, meas)

    def get_normalized_mean_voltage(self):
        """Returns the normalized mean voltage of all channels. The mean voltage is filtered by a lowpass filter
        with a user defined cutoff_frequency and then normalized to the range [0, 1].

        Returns:
            norm_volt   (np.array):     Normalized mean voltage of all channels for the considered time interval.
        """
        mean_volt = np.abs(np.mean(self.eit_meas["vv"], axis=0))
        norm_volt = (mean_volt - np.min(mean_volt)) / (
            np.max(mean_volt) - np.min(mean_volt)
        )
        return norm_volt

    def get_eit_time(self):
        """Getting the relative time of the eit measurement.

        Returns:
            np.array:   Relative time in seconds with reference at the beginning of the user defined time interval.
        """
        return self.eit_meas["relative_eit_time"]

    def get_reco_time_indices(self):
        """Getting the indices of the reconstruction times.
        Returns:
            list:   Indices of the measurement reconstruction times.
        """
        return sorted([int(i) for i in self.eit_meas["reco_times"][0]])

    def get_volt_reconst_frames(self, reconst_ids=None):
        """Returns the voltage for the reconstruction time frames.

        Returns:
            voltages    (np.array):     Complex valued voltage for n_elec x n_elec channels and all reconstruction time frames.
        """
        if not reconst_ids:
            reconst_ids = self.get_reco_time_indices()
        voltages = self.eit_meas["vv"][:, reconst_ids]
        if "num_measurements_for_reference" in self.config:
            sim_reco_timesteps = self.config["reconst_timesteps"]
            ref_index = []

            for ref_ts in self.config["reference_timestep"]:
                assert (
                    ref_ts in sim_reco_timesteps
                ), f"The reference timestep {ref_ts} is not in the reconstruction timesteps."
                ref_index.append(sim_reco_timesteps.index(ref_ts))

            voltages = self.average_reference_voltage(
                voltages,
                reconst_ids,
                ref_index,
                self.config["num_measurements_for_reference"],
            )

        return voltages

    def average_reference_voltage(
        self, voltages, reconst_ids, ref_index, num_meas_for_ref
    ):
        """Method averaging over a user defined set of voltage frames for the reconstruction reference time.
        This enhances the results of the reconstructed clinical measurements in case of noisy data.

        The method searches for the position of the reference time and determins the "neighbor" measurements.
        The number of neighbors considered for averaging the measurement channels is defined in the config
        variable "num_measurements_for_reference".

        Args:
            voltages    (np.array):     Complex valued voltage for n_elec x n_elec channels and all reconstruction time frames.
            reconst_ids     (list):     Indices of the measurement frames, which are used for the reconstruction.
            ref_index       (list):     Index of the reconstruction reference.
            num_meas_for_ref (int):     Number of measurement frames which are averaged to get an improved reconstruction reference.
        Returns:
            voltages    (np.array):     Complex valued voltage for n_elec x n_elec channels and all reconstruction time frames.
        """
        num_ref_avg = num_meas_for_ref - 1
        avg_ref_offset = int(num_ref_avg / 2)

        for ind in ref_index:
            ref_ids = range(
                reconst_ids[ind] - avg_ref_offset,
                reconst_ids[ind] + avg_ref_offset + 1,
            )
            voltages[:, ind] = np.average(
                self.eit_meas["vv"][:, ref_ids], axis=1
            )
        return voltages

    def get_detached_electrodes(self):
        """Returns the IDs of the detached electrodes.

        Returns:
            list:   Indices of the detached electrodes. Counting from 0. --> Range [0, n_ele-1]
        """
        return self.eit_meas["detached_electrodes"]

    def determine_detached_electrodes(self):
        """Determining the detached electrodes by analysing their contact impedance. The median of the difference between
        the median impedance and the current impedance serves as a criterion for the contact quality. The user defined variable
        "electrode_detachment_controller" determines the maximal deviation from the median impedance. A electrode is considered
        detached if its impedance exceeds the threshold in more than one time sample.
        Due to the character of the stimulation and measurement pattern [num_1, num_2], one detached electrode leads to two electrodes of high
        impedance. Consequently, electrode n which has a high impedance is not considered detached if the electrode at position n + (num_2 - num_1)
        does also have a high impedance.

        Returns:
            det_indices  (list):    Indices of the detached electrodes. Counting from 0. --> Range [0, n_ele-1]
            glob_thresh  (list):    Minimal and maximal threshold for the categorization of the contact impedance.
            high_imped_indices (list): Indices of the electrodes with high impedance. Counting from 0. --> Range [0, n_ele-1]
        """
        imped = np.abs(self.electrode_imped)
        med = np.median(imped, axis=0)
        med_diff = np.median(np.abs(imped - med), axis=0)

        factor = self.config["electrode_detachment_controller"]
        local_thresh = med + factor * med_diff

        high_imped_elec = np.zeros(imped.shape, dtype=bool)
        for i in range(imped.shape[1]):
            for j in range(imped.shape[0]):
                if imped[j, i] > local_thresh[i]:
                    high_imped_elec[j, i] = True
        high_imped_elec = np.any(high_imped_elec, axis=1)
        high_imped_elec = high_imped_elec.tolist()
        high_imped_indices = np.where(high_imped_elec)[0]

        glob_thresh = [
            float(np.min(local_thresh)),
            float(np.max(local_thresh)),
        ]

        n_skip_plus_1 = (
            self.config["meas_pattern"][1] - self.config["meas_pattern"][0]
        )

        detached_elec = [False] * self.config["num_electrodes"]
        for i in range(self.config["num_electrodes"]):
            if high_imped_elec[i] and high_imped_elec[i - n_skip_plus_1]:
                detached_elec[i] = True
            else:
                detached_elec[i] = False

        det_indices = [
            i for i in range(len(detached_elec)) if detached_elec[i]
        ]
        if det_indices:
            print(
                "For the reconstruction of the EIT measurement, the electrodes {} are considered detached.".format(
                    np.array(det_indices) + 1
                )
            )
        return det_indices, glob_thresh, high_imped_indices

    def determine_faulty_channels(self):
        """Determine the faulty measurement channels based on the electrode contact impedance. Faulty channels are
        all channels that are involved in the stimulation or measurement with a detached electrode. This mask is only
        used as additional output as EIDORS handles faulty data automatically by setting the detached electrodes.
        Returns:
            faulty_ch   (np.array):     Binary mask of faulty measurement channels (over n_ele**2 channels).
        """
        faulty_elec = self.high_imped_electrodes
        num_elec = self.config["num_electrodes"]
        faulty_ch = np.zeros(num_elec**2, dtype=bool)
        for elec in faulty_elec:
            faulty_ch[
                elec::num_elec
            ] = True  # channels where the faulty electrode pair is involved in the measurement
            faulty_ch[
                elec * num_elec : (elec + 1) * num_elec
            ] = True  # channels where the faulty electrode pair is involved in the stimulation
        return faulty_ch
