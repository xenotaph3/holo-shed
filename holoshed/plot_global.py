import os.path

import matplotlib.patches as patches
import matplotlib.pyplot as plt
import numpy as np

from .JsonDataIO import JsonDataIO as JsonDataIO
from .MeasEitReader import MeasEitReader as MeasEitReader
from .RealtimeReader import RealtimeReader as RealtimeReader
from .utils import get_time_mapping_indices


def plot_global(config, interactive):
    """Plotting global quantities such as the lung volume, the tracheal pressure and the averaged voltage.
    Args:
        config          (dict):     User defined configuration and parameters for the EIT simulation.
        interactive     (bool):     Command line flag running holoshed in interactive mode, i.e. plotts are shown during runtime.
    """
    fig = plt.figure(figsize=(16, 9))
    if config["reconstruct_from_sim_data"]:
        plot_global_sim_data(config)

    plot_global_meas_data(config)
    sort_legend(fig)

    plt.savefig(
        os.path.join(
            config["working_path"],
            "06_global_postprocessing_results",
            config["sim_name"] + "_global_quantities.png",
        )
    )
    if interactive:
        plt.show()
    plt.close()


def plot_global_sim_data(config):
    """Function plotting the global quantities of the simulation.
    Args:
        config      (dict):     User defined configuration and parameters for the EIT simulation.
    """
    vtu_idx = get_sorted_vtu_idx(config["all_vtus"])

    sim_times, sim_vol_l, sim_pres_mBar = get_tracheal_quant(config)

    rec_times = config["glob_reco_times"]

    reconst_ids = get_time_mapping_indices(rec_times, sim_times)

    idx_ref = []
    for ref_ts in config["reference_timestep"]:
        idx_ref.append(vtu_idx.index(ref_ts))

    mean_volt_sim = get_mean_voltage_sim(config)

    plt.subplot(3, 1, 1)
    plt.plot(sim_times, sim_vol_l, label="Simulation")
    plot_markers(sim_times, sim_vol_l, reconst_ids, idx_ref)

    plt.subplot(3, 1, 2)
    plt.plot(sim_times, sim_pres_mBar)
    plot_markers(sim_times, sim_pres_mBar, reconst_ids, idx_ref)

    plt.subplot(3, 1, 3)
    volt_reconst_ids = [i for i in range(len(reconst_ids))]
    volt_idx_ref = [reconst_ids.index(i) for i in idx_ref]
    plt.plot(sim_times[reconst_ids], mean_volt_sim, "-")
    plot_markers(
        sim_times[reconst_ids], mean_volt_sim, volt_reconst_ids, volt_idx_ref
    )


def plot_global_meas_data(config):
    """Function plotting the global quantities from clinical measurements.
    Args:
        config      (dict):     User defined configuration and parameters for the EIT simulation.
    """
    real_time, real_pres_mbar, real_vol_l = get_realtime_values(config)
    real_time += config["prestress_time"]
    if config["reconstruct_from_meas_data"]:
        mean_volt_meas, meas_time, meas_ids = get_mean_voltage_meas(config)
    if "all_vtus" in config:
        vtu_idx = get_sorted_vtu_idx(config["all_vtus"])
    else:
        vtu_idx = np.arange(
            config["num_pseudo_timesteps"]
        )  # config["all_pseudo_timesteps"]

    idx_ref = map_sim_index_on_realtime_index(
        config["reference_timestep"],
        vtu_idx,
        real_time,
        config["prestress_time"],
    )
    reconst_ids = map_sim_index_on_realtime_index(
        config["reconst_timesteps"],
        vtu_idx,
        real_time,
        config["prestress_time"],
    )

    ax1 = plt.subplot(3, 1, 1)
    plt.plot(real_time, real_vol_l, "g", label="Measurement")
    plot_markers(real_time, real_vol_l, reconst_ids, idx_ref)
    plt.xlabel("Time (s)")
    plt.ylabel("Volume (l)")
    ax1.grid(True)
    box = ax1.get_position()
    ax1.set_position([box.x0, box.y0, box.width * 0.93, box.height])

    ax2 = plt.subplot(3, 1, 2)
    plt.plot(real_time, real_pres_mbar, "g")
    plot_markers(real_time, real_pres_mbar, reconst_ids, idx_ref)
    plt.xlabel("Time (s)")
    plt.ylabel("Pressure (mbar)")
    ax2.grid(True)
    box = ax2.get_position()
    ax2.set_position([box.x0, box.y0, box.width * 0.93, box.height])

    ax3 = plt.subplot(3, 1, 3)
    if config["reconstruct_from_meas_data"]:
        volt_idx_ref = [
            meas_ids[np.where(reconst_ids == i)[0][0]] for i in idx_ref
        ]
        plt.plot(meas_time, mean_volt_meas, "g")
        plot_markers(meas_time, mean_volt_meas, meas_ids, volt_idx_ref)
    plt.xlabel("Time (s)")
    plt.ylabel("Normalized mean voltage")
    ax3.set_xlim(ax2.get_xlim())
    ax3.grid(True)
    box = ax3.get_position()
    ax3.set_position([box.x0, box.y0, box.width * 0.93, box.height])


def get_tracheal_quant(config):
    """Read the tracheal flow and pressure from the preprocessed data in *_global_quant_simulation.json.
    Args:
        config      (dict):     User defined configuration and parameters for the EIT simulation.

    Returns:
        sim_time    (numpy.array):  Vector containing the time information of all VTUs.
        volume_l    (numpy.array):  Vector containing the lung volume obtained by integrating the tracheal air flow in liter.
        pres_mBar   (numpy.array):  Vector containing the ventilation pressure at the trachea inlet in mBar.
    """
    sim_json_path = os.path.join(
        config["working_path"],
        "06_global_postprocessing_results",
        config["sim_name"] + "_global_quant_simulation.json",
    )
    json_io = JsonDataIO()
    global_quant_sim = json_io.read(sim_json_path)

    sim_time = global_quant_sim["sim_time_s"]
    volume_l = global_quant_sim["lung_vol_l"]
    pres_mBar = global_quant_sim["trach_pres_mBar"]

    return sim_time, volume_l, pres_mBar


def get_realtime_values(config):
    """Function reading the measured ventilation data and returning the quantities which are required for the global plots.
    Args:
        config          (dict):     User defined configuration and parameters for the EIT simulation.
    Returns:
        time            (np.array):     Time from the beginning of the user defined maneuvers in seconds.
        volume_l        (np.array):     Measured volume of the lung in liter.
        pressure_mBar   (np.array):     Ventilation pressure in mBar.
    """
    rt_reader = RealtimeReader(config)
    time = rt_reader.get_relative_time()
    pressure_mbar = rt_reader.get_pressure_mbar()
    volume_l = rt_reader.get_volume_l()
    return time, pressure_mbar, volume_l


def get_mean_voltage_sim(config):
    """Reading the global mean voltage from the previously simulated electrode voltages. The returned voltages are normalized to
    the range [0, 1] by their maximal and minimal values.
    Args:
        config      (dict):     User defined configuration and parameters for the EIT simulation.

    Returns:
        norm_volt  (numpy.array):       Vector containing the values of the mean voltage for each reconstructed timestep.
                                        The values of the voltage are normalized, so they lie between 0 and 1.
    """
    volt_data_path = os.path.join(
        config["working_path"],
        "03_forward_results",
        config["sim_name"] + "_elec_voltages.json",
    )

    json_io = JsonDataIO()
    volt = json_io.read(volt_data_path)["volt_data"]

    mean_volt = np.abs(np.mean(volt, axis=0))
    norm_volt = (mean_volt - np.min(mean_volt)) / (
        np.max(mean_volt) - np.min(mean_volt)
    )
    return norm_volt


def get_mean_voltage_meas(config):
    """Reading the measured voltages and returning the the normalized mean voltage. The returned voltages are normalized to
    the range [0, 1] by their maximal and minimal values.
    Args:
        config      (dict):     User defined configuration and parameters for the EIT simulation.

    Returns:
        norm_volt  (numpy.array):       Vector containing the values of the mean voltage for each reconstructed timestep.
                                        The values of the voltage are normalized, so they lie between 0 and 1.
        eit_time   (numpy.array):       Measurement time in seconds with reference at the start of the user defined time slot.
        reconst_ids       (list):       List containing the indices of the reconstructed measurement frames.
    """
    meas_eit_reader = MeasEitReader(config)
    norm_volt = meas_eit_reader.get_normalized_mean_voltage()
    eit_time = meas_eit_reader.get_eit_time()
    reconst_ids = meas_eit_reader.get_reco_time_indices()

    return norm_volt, eit_time, reconst_ids


def plot_detached_electrodes(config, interactive):
    """Function plotting the contact impedances and the detached electrodes for the clinical EIT measurements."""
    num_ele = config["num_electrodes"]
    n_skip_plus_1 = config["meas_pattern"][1] - config["meas_pattern"][0]

    meas_eit_reader = MeasEitReader(config)

    mean_imped = np.abs(
        np.mean(meas_eit_reader.eit_meas["electrode_imped"], axis=1)
    )
    thresh = meas_eit_reader.eit_meas["imped_thresh"]
    detach_ele = meas_eit_reader.eit_meas["detached_electrodes"]

    electrodes = range(0, num_ele)

    attach_high_imp_ele = [
        j - n_skip_plus_1
        for j in detach_ele
        if j - n_skip_plus_1 not in detach_ele
    ]
    attach_ele = [
        i
        for i, value in enumerate(electrodes)
        if (value not in detach_ele or value not in attach_high_imp_ele)
    ]

    fig, ax = plt.subplots(figsize=(16, 9))
    ax.bar(
        np.array(attach_ele) + 1,
        mean_imped[attach_ele],
        color="blue",
        label="Attached",
    )
    if attach_high_imp_ele:
        ax.bar(
            np.array(electrodes)[attach_high_imp_ele] + 1,
            mean_imped[attach_high_imp_ele],
            color="#3381ff",
            label="Attached, high impedance",
        )
    if detach_ele:
        ax.bar(
            np.array(detach_ele) + 1,
            mean_imped[detach_ele],
            color="red",
            label="Detached",
        )

    xlim = ax.get_xlim()
    y_ax_max = np.max([thresh[1] + 100, np.max(mean_imped)])
    ax.set_ylim(0, y_ax_max)
    rect = patches.Rectangle(
        (xlim[0], thresh[0]),
        xlim[1] - xlim[0],
        thresh[1] - thresh[0],
        color="red",
        alpha=0.5,
    )
    ax.annotate(
        "Detachment Threshold Range",
        (xlim[1] + 0.5, thresh[0]),
        annotation_clip=False,
        fontsize=12,
    )
    plt.xticks(range(1, num_ele + 1))
    ax.add_patch(rect)
    ax.set_ylabel("Contact Impedance [Ohm]", fontsize=12)
    ax.set_xlabel("Electrode ID", fontsize=12)
    ax.set_title("Contact Impedances and Detached Electrodes.")
    ax.legend(bbox_to_anchor=(1.0, 1.0), loc="upper left")
    fig.tight_layout()
    if interactive:
        plt.show()

    detachment_plot_path = os.path.join(
        config["working_path"],
        "05_measured_reconstruction_results",
        "detached_electrodes.png",
    )
    fig.savefig(detachment_plot_path)


def plot_markers(time, quant, reconst_ids, idx_ref):
    """Function plotting the different marker positions for the global plot. This includes the start, reference and
    end position of the reconstruction as well as the reconstruction samples.
    Args:
        time        (np.array):     Time array, to plot on the x-axis.
        quant       (np.array):     Array containing the considered quantity, to plot on y-axis.
        reconst_ids     (list):     Postion of all reconstructed frames with respect to the time array.
        idx_ref          (int):     Position of the reference frame with respect to the time array.
    """
    m_size = 10
    opac_samp = 0.3
    plt.plot(
        time[reconst_ids[0]],
        quant[reconst_ids[0]],
        "ro",
        label="Start reconstruction",
        markersize=m_size,
    )
    plt.plot(
        time[reconst_ids[-1]],
        quant[reconst_ids[-1]],
        "bo",
        label="End reconstruction",
        markersize=m_size,
    )
    plt.plot(
        time[idx_ref],
        quant[idx_ref],
        "yP",
        label="Reference reconstruction",
        markersize=m_size * 0.8,
    )
    plt.plot(
        time[reconst_ids],
        quant[reconst_ids],
        "m*",
        alpha=opac_samp,
        label="Reconstruction samples",
        markersize=m_size * 0.8,
    )


def map_sim_index_on_realtime_index(
    index, all_indices, rt_time, prestress_time
):
    """Function mapping the timestep index of the simulation to the corresponding index of the
    realtime array.
    Args:
        index            (int):  Considered timestep index of the simulation.
        all_indices     (list):  List containing all timesteps which are provided in the input folder.
        rt_time     (np.array):  Array containing the time of the realtime measurements.
        prestress_time (float):  Time for prestressing in the simulation. After this time the real maneuver
                                 starts, i.e. simulation and measurement run synchronously.
    """
    index = np.array(index)
    interpol_rt = (
        (index - all_indices[0])
        * (rt_time[-1] - (rt_time[0] - prestress_time))
        / (all_indices[-1] - all_indices[0])
    )
    rt_idx = np.searchsorted(rt_time, interpol_rt, side="left")
    rt_idx = np.clip(rt_idx, a_min=None, a_max=len(rt_time) - 1)

    return rt_idx


def get_sorted_vtu_idx(vtu_dict):
    """Returns the sorted vtu timesteps based on the vtu dictionary.
    Args:
        vtu_dict    (dict):     Dictionary containing the vtu paths with their corresponding timesteps.
    Returns:
        vtu_idx     (list):     Sorted array of all timesteps.
    """
    vtu_idx = list(vtu_dict.keys())
    vtu_idx.sort()
    return vtu_idx


def sort_legend(fig):
    """Function sorting the entries of the legend into a default order. Removes duplicates in legend.
    Args:
        fig     (matplotlib.figure.Figure):     Figure handle of the global plot.
    Returns:
        labels_resort   (list):     Resorted list of legend labels.
    """
    default_order = [
        "Simulation",
        "Measurement",
        "Start reconstruction",
        "End reconstruction",
        "Reference reconstruction",
        "Reconstruction samples",
    ]
    handles, labels = fig.axes[0].get_legend_handles_labels()

    mapping = []
    unique = set()
    for item in default_order:
        if item in labels and item not in unique:
            unique.add(item)
            mapping.append(labels.index(item))

    labels_resort = [labels[i] for i in mapping]
    handles_resort = [handles[i] for i in mapping]
    fig.legend(
        handles_resort, labels_resort, loc="right", bbox_to_anchor=(1, 0.5)
    )
    return labels_resort
