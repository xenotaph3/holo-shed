"""Main module of holoshed."""
import os

import click

import holoshed.plot_eit
import holoshed.plot_fe
import holoshed.plot_global
import holoshed.utils
from holoshed.initialize_holoshed import (
    initialize_holoshed as initialize_holoshed,
)
from holoshed.lung_fusion import lungFusion
from holoshed.makeGREIT import makeGREIT
from holoshed.setup_forward_model import setup_forward_model


@click.command()
@click.argument("config_file", required=True, type=click.Path(exists=True))
@click.option(
    "--interactive",
    is_flag=True,
    help="Interactive mode. Show plots for debugging purposes.",
)
@click.option(
    "--write-voltage",
    is_flag=True,
    help="Write simulated voltage output for every timestep into ensight format.",
)
@click.option(
    "--write-conductivity",
    is_flag=True,
    help="Defines whether to write the conductivity for debugging purpose.",
)
def main(config_file, interactive, write_voltage, write_conductivity):
    """Main function of holoshed.
    Args:
        interactive         (bool):     Run holoshed in interactive mode, i.e.
                                        plots are shown during runtime for
                                        debugging purposes.
        write_voltage       (bool):     Write simulated electrode voltages
                                        to ensight output.
        write_conductivity  (bool):     Defines whether to write the conductivity
                                        for debugging purpose
    """
    config = initialize_holoshed(config_file)

    fmdl = setup_forward_model(config)

    if config["reconstruct_from_sim_data"]:
        config = lungFusion(
            config=config,
            fmdl=fmdl,
            write_voltage=write_voltage,
            write_conductivity=write_conductivity,
        )
    holoshed.plot_fe.plot_fe_surf(fmdl, config, interactive)

    sim_eit_img, meas_eit_img, config = makeGREIT(config, fmdl)

    if config["reconstruct_from_sim_data"]:
        output_dir = os.path.join(
            config["working_path"], "04_simulated_reconstruction_results"
        )
        holoshed.plot_eit.plot_eit(
            sim_eit_img, config, output_dir, config["sim_name"], interactive
        )

    if config["reconstruct_from_meas_data"]:
        output_dir = os.path.join(
            config["working_path"], "05_measured_reconstruction_results"
        )
        holoshed.plot_eit.plot_eit(
            meas_eit_img, config, output_dir, "meas", interactive
        )
        holoshed.plot_global.plot_detached_electrodes(config, interactive)

    holoshed.plot_global.plot_global(config, interactive)
    print("Execution of Holo-Shed successfull.")
    return 0


if __name__ == "__main__":
    main()
