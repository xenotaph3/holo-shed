# Holo Shed

Generate EIT images from simulation output.

# Install and run holoshed

> The following section is directed at holoshed users. If you are a contributor, please proceed to the [Guide for Developers](./CONTRIBUTING.md).

Holoshed can be installed and executed using [Docker](https://www.docker.com). You can pull the latest docker container using

```bash
docker pull registry.gitlab.com/xenotaph3/holo-shed:main
```

To start the container, run the following command from a directory containing the [predefined In-/output structure](https://gitlab.com/xenotaph3/holo-shed/-/blob/main/README.md#in-output-structure)
```bash
docker run -it -v $(pwd):/opt/data --rm --entrypoint /bin/bash registry.gitlab.com/xenotaph3/holo-shed:main
```
Then activate the conda environment and navigate to the holoshed directory by
```bash
conda activate /opt/conda/holoshed
cd /opt/holo-shed
```
Now you can run holoshed with a suitable `config.json` by
```bash
python holoshed/main.py /opt/data/00_config/config.json
```
When running from a docker container the `"eidors_path"` in the `config.json` needs to be set to `"/opt/eidors/"`.

To get help on the usage of *holoshed* run:
```bash
python holoshed/main.py --help
```

# In-/output structure
The Holo Shed expects a particular structure of the input data and will in return write the results back into it.

- `00_config`: Patient specific configuration of the EIT. Contains `config.json` defining EIT specific parameters etc. See [`config.json`](https://gitlab.com/xenotaph3/holo-shed/-/blob/main/tests/reference_files/comb_vtu/00_config/config.json) for a configuration template.
- `01_input`: Per run input  
   Individual files: patient-specific torso mesh, BACI/AlveonX results, raw clinical EIT measurements (`.eit` format), raw ventilation data (`realtimeValues*.csv`). If an optional `ac_mask.nii.gz` exists, it is used to inform the attribution of 0D filling factors to the 3D torso mesh.
- `02_inhomogeneous_mesh`: The result of modifying the torso mesh with ventilation dependent conductivities in ensight format
- `03_forward_results`: The result of simulating the electrical forward problem on the inhomogeneous mesh for all points in time in ensight format. Additionally, the electrode voltages of all electrodes are written in the `holoshed` json format.
- `04_simulated_reconstruction_results`: The result of the GREIT reconstruction on the simulated electrical measurements. So far only the png of the plot figure is written out. Additionally, we save the data underlying the plots as a `.npz` file.
- `05_measured_reconstruction_results`: The results of the GREIT reconstruction on the measured electrical signal. This also includes the png of the plots and the associated `.npz` file.
- `06_global_postprocessing_results`: Plots of global quantities such as lung volume, tracheal pressure and mean electrode voltage. The alignment of lung volume and mean electrode voltage can be used to guarantee a synchronized comparison between simulation and measurement.

# Definition of parameters in `config.json`
| Parameter             | Type   | Explanation                                                                                                                                             | Recommended Value |
| --------------------- | ------ | ------------------------------------------------------------------------------------------------------------------------------------------------------- | ----------------- |
| `"reconstruct_from_sim_data"` | `bool` | Run holoshed for the EIT reconstruction from simulation data, i.e. generate a virtual EIT. | `true` |
| `"reconstruct_from_meas_data"` | `bool` | Run holoshed for the EIT reconstruction from clinical measurement data | `true` | 
| `"sim_name"` | `str` | User defined name prefix. This may be helpful when running holoshed for different simulation configurations. |  |
| `"eidors_cache_size"` | `float` | Maximal size of the cache for the execution of EIDORS in GB| |
| `"show_lung_contours"` | `bool` | Show the contour of the lung in the resulting EIT images. | |
| `"num_electrodes"` | `int` | Number of electrodes of the EIT belt. For *Sentec* devices this is 32. | `32` |
| `"num_rings"` | `int` | Number of electrode ring arrays. | `1` |
| `"inject_pattern"` | `list` | Injection pattern of the electrical current. For *Sentec* devices this is `[0,5]` which means that 4 electrodes lie between the two injecting electrodes. | `[0,5]` |
| `"meas_pattern"` | `list` | Pattern for the measurement of the electrical voltages. For *Sentec* devices this is `[0,5]`, see above.  | `[0,5]`           |
| `"meas_current"` | `str` | Defines how many electrodes are left free between the injecting and measuring ones. To leave one, two, or three electrodes free between injection and measurement set this to `"no_meas_current_next1"`, `"no_meas_current_next2"` or `"no_meas_current_next3"`, respectively. | `"no_meas_current_next2"`| 
| `"amp"` | `float` | The value of the injecting current in Ampère. For *Sentec* devices a current of 5 mA is injected. | `0.005`|
| `"conductivity_thorax"` | `float` | This defines the conductivity of the thorax in 1/(mm*Ohm) | `4.26e-4` |
| `"surfaceID_elec"` | `list` | The surface IDs of the electrode surfaces in the mesh file. The first electrode is located on the left side of the sternum. |  |
| `"surfaceID_lungs"` | `list` | The surface IDs of the lung lobes. The first ID refers to the left lung, the second ID to the right lung. If your lung surface geometry consists of more than one surface, please provide a list of lists.| 
| `"volumeID_mat"` | `list` | The volume IDs of the three mesh compartments. The thorax is considered volume #1, the left lung volume #2 and the right lung volume #3. You can also provide more than these three volumes, if your mesh has more compartments. In this case, please also provide `"conductivity_other_tissues"`. | | 
| `"conductivity_other_tissues"` | `list` | The conductivities of the additional volumes defined in `"volumeID_mat"`. | |
| `"eidors_path"` | `str` | Local path of the eidors package. | |
| `"tubus"` | `str` | Name of the tubus device. See `holoshed/tubus_parameters.py` for possible values. If your custom tubus is not yet available, implement it.  | `"mallinckrodt_107_80"`|
| `"start_timestep"` | `int` | First timestep to consider for EIT reconstruction. | | 
| `"end_timestep"` | `int` | Last timestep to consider for EIT reconstruction. | | 
| `"timestep_sampling"` | `int` | Number of simulated timesteps which are skipped for reconstruction. | |
| `"reference_timestep"` | `int/list` | ID of the timestep, which is considered as reference for the EIT reconstruction. Select your reference according to the global ventilation. Usually the end-expiratory state is a good choice. It is also possible to provide a list of integer indices. E.g. you could use several end-expiratory states to reduce the influence of noise. <br/>So far the reference needs to be chosen according to the first timestep and the timestep sampling. <br/>*Example: "start_timestep": 1, "timestep_sampling": 2, in this case the reference timestep needs to be an odd number.* | | 
| `"reconst_timesteps"` | `list` | IDs of the timesteps that are to be reconstruced. This enables a non uniform sampling of the reconstruction frames. This option overwrites the timesteps defined by `"start_timestep"`, `"end_timestep"` and `"timestep_sampling"`.| |
| `"color_levels"` | `int` | Number of color levels in the reconstructed image. The higher this number is, the smoother the resulting image looks. | `100` |
| `"realtime_start"` | `float` | Beginning of the simulation with respect to the raw ventilation file (i.e. `realtimeValues*.csv`) in seconds. | |
| `"realtime_end"` | `float` | End of the simulation with respect to the raw ventilation file (i.e. `realtimeValues*.csv`) in seconds. | |
| `"meas_reconst_frequency"` | `float` | The frequency of the reconstruction frames in Hertz. This option is only used if `"reconstruct_from_sim_data" == False`. |  |
| `"realtime_reference"` | `float/list` | The reconstruction reference in the RealtimeValues time system. This option is only used if `"reconstruct_from_sim_data" == False`. Consider providing a list of float valued reference times. This reduces the influence of measurement noise. |  |
| `"realtime_reconst_times"` | `list` | The reconstruction frames in the RealtimeValues time system. This option is only used if `"reconstruct_from_sim_data" == False`. If this option is set, the variable `"meas_reconst_frequency"` is ignored.|  |
| `"eit_realtime_offset"` | `float` | Compensate time stamp difference between ventilation data (i.e. `realtimeValues*.csv`) and EIT measurements (i.e. `*.eit`). The offset is added to the EIT time in seconds. | `0.0` |
| `"start_volume_sim"` | `float` | Initial total lung volume for the simulation. ||
| `"start_volume_meas"` | `float` | Initial total lung volume for the measurement. If BACI simulations are considered, `"start_volume_sim"` and `"start_volume_meas"` may not be the same. ||
| `"electrode_detachment_controller"` | `int` | This parameter controls which electrodes are considered detached. It describes the number of standard deviations which are used to find a tolerable range of contact impedances. | `8`|
| `"num_measurements_for_reference"` | `int` | To find a suitable reference for the reconstruction of the measured EIT, it is helpful to use an averaged reference over a couple of frames at the reference position. This parameter determines how many measurement frames are considered to determine this average. Integers between `1` and `20` are valid. | `5`|
| `"filter_type"` | `str` | Holoshed allows filtering measurement noise. This parameter sets the filter type according to the definition in `scipy.signal.butter`. `"lowpass"`, `"highpass"`, `"bandpass"` and `"bandstop"` filters are available. | `"lowpass"`|
| `"filter_order"` | `int` | Filter order according to the definition in `scipy.signal.butter`. | 1 |
| `"cutoff_frequency"` | `float`/`list` | Cutoff frequency of the filter in Hz. `"lowpass"` and `"highpass"` filter require a `float`, `"bandpass"` and `"bandstop"` filters require a tuple of `float`. | `6.7` (for a `"lowpass"` filter)|
| `"use_volumetric_ac_tet_mapping"` | `bool` | If an alveolar cluster mask is presented, a value of `true` will determine filling factors based on soft volumetric overlap rather than hard nearest neighbor assignment. If entry not present, assumed to be set `false` by default. |   |
| **GREIT Parameters:** <td colspan=4>The following parameters provide a setup for the GREIT algorithm. The GREIT algorithm solves the inverse problem of the EIT reconstruction. Details on the method can be looked up in *Adler A et al. GREIT: a unified approach to 2D linear EIT reconstruction of lung images. Physiol Meas. 2009 Jun;30(6):S35-55. doi: 10.1088/0967-3334/30/6/S03.* |
| `"GREIT_imgsz"` | `int` | Number of pixels in one dimension for the GREIT image reconstruction. The image is reconstructed on 2D triangular elements in a plane of the averaged z-coordinates of the element positions. | `32` |
| `"GREIT_distr"` | `int` | Distribution of training points for GREIT. There is probably no need to touch this. | `3` | 
| `"GREIT_Nsim"` | `int` | Number of training points for GREIT. There is probably no need to touch this. | `500` | 
| `"GREIT_target_size"` | `float` | Size of the simulated targets as proportion of the mesh radius. There is probably no need to touch this. | `0.03` | 
| `"GREIT_target_offset"` | `float` | Maximal allowed offset of the target position in z-direction. There is probably no need to touch this. | `0.0` | 
| `"GREIT_noise_figure_reference"` | `float` | Noise figure to be achieved for measured voltage data. For explanation, see the documentation on GREIT. This parameter has a significant influence on the EIT images. A low noise figure smears the resulting images and makes them less sensitive on measurement noise. | `0.5` |
| `"GREIT_noise_figure_simulation"` | `float` | Noise figure to be achieved for simulated voltage data. For explanation, see the documentation on GREIT. This parameter has a significant influence on the EIT images. A low noise figure smears the resulting images and makes them less sensitive on measurement noise. | `0.5` |
| `"GREIT_conductivity_template_lung"` | `float` | The template conductivity of the lung used for the GREIT reconstruction. | `3.0` |
| `"GREIT_conductivity_template_thorax"` | `float` | The template conductivity of the thorax used for the GREIT reconstruction | `1.0` |
| `"GREIT_template_conductivity_other_tissues"` | `list` | The template conductivity of the other tissues used for the GREIT reconstruction. This is optional. By default other tissues get the thorax conductivity.|  |
| `GREIT_square_pixels"` | `bool` | Force pixels to be squares (aspect ratio of 1). No need to touch this. | `1` |

# Time Handling in Holoshed
There are mainly three different configurations to run holoshed. They are defined by the values of the variables `reconstruct_from_sim_data` and `reconstruct_from_meas_data`. As in each case different data is available, holoshed adapts its handling of time according to the corresponding case. 
### 1. Virtual EIT & Clinical EIt
```
"reconstruct_from_sim_data": True, 
"reconstruct_from_meas_data": True 
```
   
This is the most common case. You want to compare clinical and virtual EIT. In this case, you have a reduced dimensional lung simulation, the ventilation profiles (realtime values), and the eit measurement files available.
![image info](doc/source/time_handling.png)
###  2. Virtual EIT 
```
"reconstruct_from_sim_data": True, 
"reconstruct_from_meas_data": False
```
In this case you only have the reduced dimensional lung simulation. As you only want the virtual EIT, no further synchronization is necessary
###  3. Clinical EIT 
```
"reconstruct_from_sim_data": False, 
"reconstruct_from_meas_data": True
```
In case 1, the most common case, the start of the simulation is set to the time zero. Thus, the procedure in holoshed relies on the presence of a simulation time.

As no such simulation time is available in the third case, holoshed creates a dummy simulation time which is used for synchronization. Depending on the definition of the times that are to be reconstructed, holoshed differs between two ways to calculate the dummy simulation time:

1. You want to reconstruct EIT images in a uniform pattern between the `realtime_start` and `realtime_end`. The value of `meas_reconst_frequqncy` defines how many images are reconstructed per second.![image info](doc/source/time_handling_dummy_time_uniform.png)
2. You are reconstructing EIT images for non-uniformly spreaded times defined by `realtime_reconst_times`. A possible applictaion for this is that you want e.g. only to compare the end-expiratory states of ventilation profile.![image info](doc/source/time_handling_dummy_time_non_uniform.png)










