from os.path import join as join

import meshio
import numpy as np

from holoshed.PvdIO import PvdIO as PvdIO
from holoshed.utils import get_element_center_coordinates, repo_base_path


def generate_tripart_lobe_vtu(
    vent_areas_left,
    vent_areas_right,
    FF_ampl,
    vtu_split_flag,
    output_path,
):
    """Function generating artificial VTUs with time dependent AC volumes. The ventilated area can
    be specified by user input. The lung is partitioned in three areas defined by their y-coordinate.
    The obtained VTUs can be used to try out the reconstruction setup.
    Args:
        vent_areas_left     (list):     List of ventilated areas in left lung.
        vent_areas_right    (list):     List of ventilated areas in right lung.
        FF_ampl            (float):     Amplitude of FF manipulation.
        vtu_split_flag      (bool):     Generate VTU in split or combined format
        output_path          (str):     Output directory.
    """

    # Setting up function over time for the gas volume
    num_timesteps = 13
    times = np.arange(0, num_timesteps)
    ff_t = FF_ampl * 3 * np.sin(np.pi / (num_timesteps - 1) * times) + 3

    # Setup depending on baci/alveonx data
    if vtu_split_flag:
        reference_dir = "split_vtu"
        static_file = "simulation_0002.constant.0.vtu"
        prefix_name = "simulation_0002."
        flow_field = "flow"
        pressure_field = "pressure"
        vol_gas_field = "volume"
        vol_tiss_field = "tissue_volume"
        vol_liq_field = "liquid_volume"
    else:
        reference_dir = "comb_vtu"
        static_file = "dummy-07817.vtu"
        prefix_name = "dummy-"
        flow_field = "flow_in"
        pressure_field = "pressure"
        vol_gas_field = "acini_volume"
        vol_tiss_field = "acini_volume_tissue"
        vol_liq_field = "acini_volume_edema"

    input_path = join(
        repo_base_path(), "tests", "reference_files", reference_dir, "01_input"
    )

    # Reading static sample vtu
    static_path = join(input_path, static_file)
    stat_vtu = meshio.read(static_path)

    ele = stat_vtu.cells[0].data
    nod = stat_vtu.points

    num_ele = ele.shape[0]
    num_nod = nod.shape[0]

    centers = get_element_center_coordinates(ele, nod)

    # Setting up cell data to be written into vtu
    vol_gas = np.zeros(
        [
            num_ele,
        ]
    )
    flow = np.zeros(
        [
            num_ele,
        ]
    )
    pressure = np.zeros(
        [
            num_nod,
        ]
    )

    if vtu_split_flag:
        cell_data = {
            flow_field: [flow],
            vol_gas_field: [vol_gas],
        }
        stat_vtu.write(join(output_path, static_file))

        is_ac = stat_vtu.cell_data["ele_set_id"][0] == 1

    else:
        cell_data = {
            flow_field: [flow],
            vol_gas_field: [vol_gas],
            "generations": [stat_vtu.get_cell_data("generations", "line")],
            "acini_volume_tissue": [
                stat_vtu.get_cell_data("acini_volume_tissue", "line")
            ],
            "acini_volume_edema": [
                stat_vtu.get_cell_data("acini_volume_edema", "line")
            ],
        }
        is_ac = stat_vtu.cell_data["generations"][0] == -1

    # Determine which ACs correspond to the active regions
    y_lim = [np.min(stat_vtu.points[:, 1]), np.max(stat_vtu.points[:, 1])]
    x_lim = [np.min(stat_vtu.points[:, 0]), np.max(stat_vtu.points[:, 0])]
    y_thresh = np.linspace(y_lim[0], y_lim[1], 4)
    x_thresh = np.mean(x_lim)

    left_thresh = generate_threshold_range(vent_areas_left, y_thresh)
    right_thresh = generate_threshold_range(vent_areas_right, y_thresh)

    active_ele = []
    for i in range(num_ele):
        if is_ac[i]:
            if centers[i, 0] > x_thresh:
                for j in range(len(vent_areas_left)):
                    if (
                        centers[i, 1] > left_thresh[j][0]
                        and centers[i, 1] < left_thresh[j][1]
                    ):
                        active_ele.append(i)
            if centers[i, 0] < x_thresh:
                for j in range(len(vent_areas_right)):
                    if (
                        centers[i, 1] > right_thresh[j][0]
                        and centers[i, 1] < right_thresh[j][1]
                    ):
                        active_ele.append(i)

    # write VTUs
    cells = [meshio.CellBlock("line", ele.tolist())]
    vtu_names = []

    point_data = {pressure_field: pressure}

    vol_tiss = stat_vtu.get_cell_data(vol_tiss_field, "line")
    vol_liq = stat_vtu.get_cell_data(vol_liq_field, "line")
    for i in range(num_timesteps):
        cell_data[vol_gas_field][0][active_ele] = ff_t[i] * (
            vol_tiss[active_ele] + vol_liq[active_ele]
        )
        dyn_vtu = meshio.Mesh(nod, cells, point_data, cell_data)
        name = prefix_name + str(int(times[i])) + ".vtu"
        vtu_names.append(name)
        dyn_vtu.write(join(output_path, name))

    pvd = PvdIO()
    pvd.create_data_collection(times, vtu_names)
    pvd.write(join(output_path, "my_pvd.pvd"))


def generate_threshold_range(ventil_list, y_thresh):
    """Creates ranges of y-coordinates which are active due to the current user input.
    Args:
        ventil_list     (list):     Contains which lung regions are active.
        y_thresh    (np.array):     Array of length 4: Deviding the y-coord range into
                                    three regions. (ventral, middle, dorsal)
    Returns:
        thresh_range    (list):     List (n x 2) containing the valid y-range of all
                                    n active regions.
    """
    thresh_range = []
    for region in ventil_list:
        if region == "ventral":
            thresh_range.append([y_thresh[0], y_thresh[1]])
        if region == "middle":
            thresh_range.append([y_thresh[1], y_thresh[2]])
        if region == "dorsal":
            thresh_range.append([y_thresh[2], y_thresh[3]])
    return thresh_range
