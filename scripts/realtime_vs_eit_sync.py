import os
from os.path import join as join

import fire
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from oct2py import octave

from holoshed.MeasEitReader import MeasEitReader
from holoshed.utils import repo_base_path


def main(input_path, realtime_start, realtime_end, eidors_path):
    """This script allows you to compare the volume curve from the realtimeValues*.csv file with the mean voltage curve from the eit measurements. You might need this in case you're struggeling in synchronizing both data.

    Args:
        input_path       (str): Path to the folder containing the realtimeValues*.csv file and the
                                eit measurements. For regular executions of holoshed this is the
                                folder 01_input.
        realtime_start (float): Start time of the evaluation in seconds.
        realtime_end   (float): End time of the evaluation in seconds. Note: The interval should
                                not be longer than 1200s.
        eidors_path      (str): Path of the eidors package."""
    eit_files = [f for f in os.listdir(input_path) if f.endswith(".eit")]
    eit_files.sort()
    csv_file = [f for f in os.listdir(input_path) if f.endswith(".csv")]
    assert len(csv_file) == 1
    csv_file = csv_file[0]

    config = {
        "eidors_path": eidors_path,
        "working_path": os.path.dirname(os.path.abspath(input_path)),
        "realtime_start": realtime_start,
        "realtime_end": realtime_end,
        "eit_realtime_offset": 0.0,
        "filter_type": "lowpass",
        "filter_order": 1,
        "cutoff_frequency": 6.7,
        "prestress_time": 0.0,
        "glob_reco_times": np.array([10.0, 20.0]),
        "electrode_detachment_controller": 6,
        "num_electrodes": 32,
        "num_rings": 1,
        "inject_pattern": [0, 5],
        "meas_pattern": [0, 5],
        "meas_current": "no_meas_current_next2",
        "amp": 0.005,
    }

    config["eit_meas_list"] = eit_files
    config["eit_meas_list"] = [
        join(config["working_path"], "01_input", eit)
        for eit in config["eit_meas_list"]
    ]

    octave.run(join(config["eidors_path"], "startup.m"), nout=0)
    octave.addpath(join(repo_base_path(), "holoshed", "octave_scripts"))
    octave.addpath(config["eidors_path"])
    eit = MeasEitReader(config)

    volt = eit.get_normalized_mean_voltage()
    time = eit.get_eit_time()

    rt_time, volume = read_csv(join(input_path, csv_file))

    rt_time_cut = rt_time[
        (rt_time >= config["realtime_start"])
        & (rt_time <= config["realtime_end"])
    ]
    rt_time_cut = rt_time_cut - rt_time_cut[0]
    volume_cut = volume[
        (rt_time >= config["realtime_start"])
        & (rt_time <= config["realtime_end"])
    ]

    _, ax1 = plt.subplots()

    ax1.plot(rt_time_cut, volume_cut, color="tab:blue")
    ax1.set_xlabel("Time")
    ax1.set_ylabel("Volume [ml]", color="tab:blue")
    ax1.tick_params(axis="y", labelcolor="tab:blue")

    ax2 = ax1.twinx()

    ax2.plot(time, volt, color="tab:red")
    ax2.set_ylabel("Voltage", color="tab:red")
    ax2.tick_params(axis="y", labelcolor="tab:red")

    plt.show()
    os.remove(join(config["working_path"], eit.EIT_MEAS_CACHE_PATH))
    print("")


def read_csv(file):
    """Read the realtimeValues*.csv file and return the time and volume array.

    Args:
        file (str): Path to the realtimeValues*.csv file.

    Returns:
        time_array (np.array): Array containing the time values.
        volume     (np.array): Array containing the volume values.
    """
    df = pd.read_csv(file)

    df["Timestamp"] = pd.to_datetime(
        df["Timestamp"], format="%Y.%m.%d %H:%M:%S.%f"
    )

    df["time_diff"] = (
        df["Timestamp"] - df["Timestamp"].iloc[0]
    ).dt.total_seconds()

    time_array = np.array(df["time_diff"])
    volume = np.array(df["Volume [ml]"])

    return time_array, volume


if __name__ == "__main__":
    fire.Fire(main)
