from generate_tripart_lobe_vtu import generate_tripart_lobe_vtu

# Path where to store the generated VTU files
output_path = "/some/dummy/path/01_input"

# Ventilated areas of the left or right lung
# possible options are "ventral", "mid", and "dorsal"
vent_areas_left = [
    "ventral",
    "dorsal",
]  # left lung is ventilated in ventral and dorsal region
vent_areas_right = []  # no ventilation of any region in the right lung

# Flag defining if the vtu-files are split into constant and dynamic part.
# i.e.  alveonx --> True
#       baci    --> False
vtu_split_flag = True

# Tidal amplitude factor to determine the filling factor
# The FF curve follows the function
# f(t) = FF_ampl * 3 * sin(pi/12 *t) +3
# and is defined for t in [0, 12]
# For FF_ampl = 1.0 the filling factor is in the range [3, 6]
FF_ampl = 1.0


generate_tripart_lobe_vtu(
    vent_areas_left, vent_areas_right, FF_ampl, vtu_split_flag, output_path
)
