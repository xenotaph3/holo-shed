import json
import os
import re

import fire
import matplotlib.pyplot as plt
import numpy as np
import pyvista as pv

import holoshed.PvdIO as PvdIO


def main(pvd_path, config, eit_npz, z_plane: float):
    """
    Calculate and visualize the difference in resistivity in a plane for each timestep.This helps
    assessing the quality of the EIT reconstruction. The resistivity diffrerence which is shown
    here should strongly correlate with the impedance changes that can be seen in the virtual EIT.
    The resulting resistivity images are saved in the same folder as the conductivity pvd file.

    Args:
        pvd_path  (str): The path to the PVD file of the conductivity data. This is created if you
                         run holoshed with the --write-conductivity option.
        config    (str): The path to the config.json file of your holoshed execution.
        eit_npz   (str): The path to the eit_reconstruction_data.npz file of your holoshed execution.
        z_plane (float): The z-coordinate of the electrode plane.

    Returns:
        None
    """
    z_dist = 40
    eit_data = np.load(eit_npz)
    outpath = os.path.dirname(pvd_path)
    X = eit_data["X"]
    Y = eit_data["Y"]

    pvd = PvdIO.PvdIO.from_file(pvd_path)
    files = pvd.get_timesteps_and_filenames()[1]
    timesteps = []
    resistivity = []
    for vtu in files:
        match = re.search(r"_(\d+)\.vtu$", vtu)
        if match:
            timesteps.append(int(match.group(1)))
        else:
            raise ValueError(f"No timestep id found in the vtu file {vtu}")

        mesh = pv.read(vtu)
        mesh = mesh.cell_data_to_point_data()

        pointset = mesh.cast_to_pointset()
        pointset.point_data["dist_weight"] = (
            1 - np.abs(pointset.points[:, 2] - z_plane) / z_dist
        )
        pointset = pointset.threshold(0.0, scalars="dist_weight")

        pointset.points[:, 2] = z_plane

        pointset.point_data["resistivity"] = (
            1 / pointset.point_data["conductivity"]
        )
        res = get_regular_grid_data(pointset, "resistivity", X, Y)
        resistivity.append(res)

    resistivity = np.array(resistivity)

    with open(config) as json_file:
        config = json.load(json_file)

    ref_ts = config["reference_timestep"]

    ref_idx = [timesteps.index(ts) for ts in ref_ts]

    ref_resistivity = np.nanmean(resistivity[ref_idx], axis=0)

    diff_resistivity = resistivity - ref_resistivity

    min_d = np.nanmin(diff_resistivity)
    max_d = np.nanmax(diff_resistivity)

    for i in range(len(timesteps)):
        ax = plt.subplots()[1]
        p1 = ax.contourf(
            X, Y, diff_resistivity[i], levels=np.linspace(min_d, max_d, 100)
        )
        plt.title("Resistivity difference")
        plt.colorbar(p1)
        ax.invert_yaxis()

        plt.savefig(
            os.path.join(outpath, f"resistivity_diff_{timesteps[i]}.png")
        )

    avg_ts = [ts for ts in timesteps if ts not in ref_ts]
    rec_idx = [timesteps.index(ts) for ts in avg_ts]
    mean_diff = np.nanmean(diff_resistivity[rec_idx], axis=0)

    ax = plt.subplots()[1]
    p1 = ax.contourf(X, Y, mean_diff, levels=np.linspace(min_d, max_d, 100))
    plt.title("Averaged resistivity difference")
    plt.colorbar(p1)
    ax.invert_yaxis()
    plt.savefig(os.path.join(outpath, "resistivity_diff_avg.png"))

    np.savez(
        os.path.join(outpath, "resistivity_diff.npz"),
        X=X,
        Y=Y,
        resistivity_diff=diff_resistivity,
        resistivity_diff_avg=mean_diff,
        resistivity_abs=resistivity,
        timesteps=timesteps,
        reference_timestep=config["reference_timestep"],
    )


def get_regular_grid_data(pointset, field, X, Y):
    """
    Determine the averaged value of a field based on point data of points that are distributed
    without a regular grid. All points are projected to the same plane normal to the z-axis.
    By weighted averaging, the distance to the plane is considered.

    Args:
        pointset (pyvista.pointset): The pointset containing the data.
        field                 (str): The name of the field to be averaged.
        X                (np.array): The x-coordinates of the grid.
        Y                (np.array): The y-coordinates of the grid.
    """
    assert np.isclose(
        np.abs(np.diff(X[0, :2]) / np.diff(Y[:2, 0])), 1
    ), "The grid must have a pixel ratio of 1:1"
    x = X.flatten()
    y = Y.flatten()

    s_pix = 2 * np.diff(X[0, :])[0]  # Considering points within 2 pixel radius

    val = np.ones(x.shape) * np.nan
    for i in range(len(x)):
        ind = np.where(
            np.linalg.norm(
                pointset.points[:, :2] - np.array([x[i], y[i]]), axis=1
            )
            < s_pix
        )[0]
        if len(ind) > 0:
            local_weights = pointset.point_data["dist_weight"][ind] / np.sum(
                pointset.point_data["dist_weight"][ind]
            )
            val[i] = np.dot(local_weights, pointset.point_data[field][ind])

    val = val.reshape(X.shape)
    return val


if __name__ == "__main__":
    fire.Fire(main)
