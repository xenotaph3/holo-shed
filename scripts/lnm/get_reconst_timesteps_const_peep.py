import json
import os

import matplotlib.pyplot as plt
import numpy as np
import scipy.signal as signal

import holoshed.JsonDataIO as JsonDataIO
import holoshed.lung_fusion as lung_fusion


def main(pre_config):
    """This script extracts the relevant timesteps for the EIT reconstruction based on the volume curve. It helps setting up the config file for the clinical EIT study.
    The relevant data is copied to your clipboard, so you can paste it into the config file.
    Args:
        pre_config (str): Path to the pre_config.json file, containing the evaluation and cutting times.
    """
    pre_config = os.path.abspath(pre_config)
    with open(pre_config) as json_file:
        config = json.load(json_file)
    parts = pre_config.split("/")
    config["working_path"] = os.path.join(*["/", *parts[:-2]])

    begin_rm = config["remove_id_begin"]
    end_rm = config["remove_id_end"]
    assert len(begin_rm) == len(end_rm)

    eval_frames = np.array(
        config["evaluate_times"][config["considered_maneuver"]]
    )
    eval_duration = np.diff(eval_frames)[0] * 1 / config["sim_freq"]

    rt_file_id, cum_times_per_rt_files = determine_rt_file_id(
        begin_rm, end_rm, eval_frames, config
    )

    step_offset = config["evaluate_times"][config["considered_maneuver"]][0]

    rt_start, rt_end = determine_rt_time_limits(
        begin_rm,
        end_rm,
        rt_file_id,
        step_offset,
        eval_duration,
        cum_times_per_rt_files,
        config,
    )

    vol, time = get_volume_data(config)

    delta_press = np.max(vol) - np.min(vol)

    maxima = signal.find_peaks(vol, prominence=0.5 * delta_press)
    minima = signal.find_peaks(-vol, prominence=0.5 * delta_press)

    eval_timesteps, ref_timesteps, plot_timesteps = get_eval_and_ref_timesteps(
        maxima, minima, step_offset, config["num_add_reco_timesteps"]
    )

    plt.plot(time, vol)
    plt.plot(time[plot_timesteps], vol[plot_timesteps], "x")
    plt.plot(time[minima[0]], vol[minima[0]], "p")
    plt.xlabel("time [s]")
    plt.ylabel("volume [l]")
    plt.show()

    final_str = (
        '"reconst_timesteps":'
        + str(eval_timesteps)
        + ",\n"
        + '"reference_timestep":'
        + str(ref_timesteps)
        + ",\n"
        + '"realtime_start":'
        + str(rt_start)
        + ",\n"
        + '"realtime_end":'
        + str(rt_end)
        + ",\n"
    )

    print("The string containing the timesteps and the reference timestep is:")
    print(final_str)
    try:
        import clipboard

        clipboard.copy(final_str)
        print("It is copied to your clipboard. Paste it into the config file.")
    except ModuleNotFoundError:
        pass

    return 0


def determine_rt_file_id(begin_rm, end_rm, eval_frames, config):
    """
    Determines the ID of the realtimeValued file for a given maneuver based on the evaluated time range for this maneuver under consideration of the removed time frames.

    Args:
        begin_rm (list): A list of lists representing the first frames of the removed intervals.
        end_rm (list): A list of lists representing the ending frames of the removed intervals.
        eval_frames (list): The first and last frame of the removed interval.
        config (dict): A dictionary containing configuration parameters.

    Returns:
        tuple: A tuple containing the real-time file ID and the cumulative times per real-time file.
    """
    kept_frames_per_rt_files = []

    for i in range(len(begin_rm)):
        kept_frames_per_rt_files.append(
            sum(
                [
                    begin_rm[i][j + 1] - end_rm[i][j]
                    for j in range(len(begin_rm[i]) - 1)
                ]
            )
        )

    kept_frames_per_rt_files = np.array(kept_frames_per_rt_files)
    times_per_rt_files = kept_frames_per_rt_files * 1 / config["realtime_freq"]
    cum_times_per_rt_files = np.cumsum(times_per_rt_files)

    start_sim_time = eval_frames[0] * 1 / config["sim_freq"]

    for i in range(len(cum_times_per_rt_files)):
        if cum_times_per_rt_files[i] > start_sim_time:
            print("*************************************\n,\n")
            print(
                f"The evaluated time range for the maneuver {config['considered_maneuver']} is part of the realtimeValues file {i+1}. \nCopy it into the dir '01_input'!"
            )
            print("\n\n*************************************")
            rt_file_id = i
            break
    return rt_file_id, cum_times_per_rt_files


def determine_rt_time_limits(
    begin_rm,
    end_rm,
    id,
    step_offset,
    eval_duration,
    cum_times_per_rt_files,
    config,
):
    """
    Determine the 'realtime_start' and 'realtime_end' times considering the offsets from ramping and cutting.

    Args:
        begin_rm    (list): A list of lists representing the first frames of the removed
                            intervals.
        end_rm      (list): A list of lists representing the ending frames of the removed
                            intervals.
        id           (int): The id of the relevant realtimeValues file.
        step_offset  (int): The timestep offset of the current maneuver referred to the first
                            simulation step.
        eval_duration (float):  The duration of the evaluation interval.
        cum_times_per_rt_files (numpy.ndarray): Array of cumulative times per real-time file.
        config (dict): Configuration dictionary containing simulation and real-time frequency.

    Returns:
        tuple: A tuple containing the real-time start and end times.

    """
    cum_times_per_rt_files = np.insert(cum_times_per_rt_files, 0, 0)
    removed_frames = []
    for i in range(len(begin_rm[id])):
        removed_frames.append(end_rm[id][i] - begin_rm[id][i])
    removed_frames = np.array(removed_frames)
    removed_times = removed_frames * 1 / config["realtime_freq"]
    cum_removed_times = np.cumsum(removed_times)

    kept_frames = [
        begin_rm[id][j + 1] - end_rm[id][j]
        for j in range(len(begin_rm[id]) - 1)
    ]
    kept_frames = np.array(kept_frames)
    kept_times = kept_frames * 1 / config["realtime_freq"]
    cum_kept_times = np.cumsum(kept_times)

    offset_time = (
        step_offset * 1 / config["sim_freq"] - cum_times_per_rt_files[id]
    )
    int_id = np.where(offset_time < cum_kept_times)[0][0]

    rt_start = cum_removed_times[int_id] + offset_time - config["ramping_time"]
    rt_end = rt_start + eval_duration

    return rt_start, rt_end


def get_volume_data(config):
    """
    Retrieves volume data and corresponding time from a simulation.

    Args:
        config (dict): Configuration parameters for the simulation.

    Returns:
        tuple: A tuple containing the volume data and corresponding time.
    """
    cache_dir = os.path.join(
        config["working_path"], "06_global_postprocessing_results"
    )
    if not os.path.isdir(cache_dir):
        os.mkdir(cache_dir)
    sim_json_path = os.path.join(
        cache_dir,
        config["sim_name"] + "_global_quant_simulation.json",
    )

    if not os.path.isfile(sim_json_path):
        m = lung_fusion.Model(config, "dummy_fmdl")
        m.check_for_valid_vtu()
        m.read_global_quantities_from_sim_vtu()
    else:
        print("Reading global quantities from cached json file.")

    jsonio = JsonDataIO.JsonDataIO()
    sim_data = jsonio.read(sim_json_path)

    time = sim_data["sim_time_s"]
    volume = sim_data["lung_vol_l"]
    return volume, time


def get_eval_and_ref_timesteps(
    maxima, minima, step_offset, num_add_eval_timesteps
):
    """
    Get evaluation and reference timesteps with offset. We add additinal frames to the maxima
    to reduce the influence of noise. The reference timesteps are the minima.

    Parameters:
        maxima (list): List of maximum values.
        minima (list): List of minimum values.
        step_offset (int): Offset value to be added to the timesteps.
        num_add_eval_timesteps (int): Number of additional evaluation timesteps for every maximum to reduce noise.

    Returns:
        timesteps_offset (list): List of timesteps with offset.
        ref_timesteps_offset (list): List of reference timesteps with offset.
        eval_timesteps (numpy.ndarray): Array of evaluation timesteps.
    """
    eval_timesteps = []
    for i in range(num_add_eval_timesteps + 1):
        eval_timesteps.append(maxima[0] - i)
    eval_timesteps = np.concatenate(eval_timesteps)
    # timesteps is a merge of maxima and minima
    timesteps = np.concatenate((eval_timesteps, minima[0]))
    timesteps.sort()
    timesteps_offset = timesteps + step_offset
    timesteps_offset = timesteps_offset.tolist()

    ref_timesteps_offset = minima[0] + step_offset
    ref_timesteps_offset = ref_timesteps_offset.tolist()

    return timesteps_offset, ref_timesteps_offset, eval_timesteps


if __name__ == "__main__":
    import fire

    fire.Fire(main)
