"""This module prepares BACI simulation results for the holoshed execution. This is necessary for result files,
which are generated on a cluster. In this case multiple vtus are written for each processor for each timestep.
The pvtu file links the processor results. As holoshed uses only one vtu for each timestep, this script merges
the individual processsor vtus and makes them compatible with the holoshed pipeline."""
import os
from os.path import join as join

import click
import defusedxml
import pyvista as pv

import holoshed.PvdIO


@click.command()
@click.argument("pvd-file", required=True, type=click.Path(exists=True))
@click.argument("output-dir", required=True, type=click.Path())
@click.option(
    "--factor",
    required=False,
    type=float,
    default=None,
    help="Factor to adapt the time in the pvd file.",
)
def prepare_baci_input(pvd_file, output_dir, factor=1.0):
    """
    Function merging the vtus of the individual processors. The merged vtus are saved together with a pvd into the output_dir.\n
    The following folder structure is assumed:

    \b
    /some/path/with/simulation/results
        - my_file.pvd
        - vtu_dir/
            - my_vtu.vtu
            - my_pvtu.pvtu

    pvd_file      (str):    The path of the pvd_file of the considered simulation.
    output_dir    (str):    The output directory of the merged simulation results.
    factor      (float):    Factor to adapt the time in the pvd file."""
    if not os.path.exists(output_dir):
        os.mkdir(output_dir)

    pvd_dir, pvd_name = os.path.split(pvd_file)
    pvd = holoshed.PvdIO.PvdIO.from_file(pvd_file)
    times, pvtu_files = pvd.get_timesteps_and_filenames()

    # adapt times using factor
    times = [str(float(t) * factor) for t in times]

    new_vtu_filenames = []
    for pvtu in pvtu_files:
        new_vtu_name = merge_vtus(pvtu, pvd_dir, output_dir)
        new_vtu_filenames.append(new_vtu_name)

    new_pvd = holoshed.PvdIO.PvdIO()
    new_pvd.create_data_collection(times, new_vtu_filenames)
    new_pvd.write(join(output_dir, pvd_name))


def merge_vtus(pvtu, pvd_dir, output_dir):
    """Function merging the vtus defined in a pvtu file and saving the result in the output directroy.
    Args:
        pvtu        (str):  Relative path to the current pvtu referring to the pvd directory.
        pvd_dir     (str):  Absolute directory of the pvd-file.
        output_dir  (str):  Desired absolute path for the output.
    Returns:
        new_vtu_name    (str):  Filename of the merged vtu.
    """
    vtu_dir, pvtu_name = os.path.split(pvtu)
    vtu_list = get_vtus(join(pvd_dir, pvtu))

    vtu_merged = pv.UnstructuredGrid()
    for vtu_file in vtu_list:
        vtu_path_abs = join(pvd_dir, vtu_dir, vtu_file)
        vtu_merged += pv.read(vtu_path_abs)

    new_vtu_name = os.path.splitext(pvtu_name)[0] + ".vtu"
    vtu_merged.save(join(output_dir, new_vtu_name))
    return new_vtu_name


def get_vtus(pvtu_path):
    """Function obtaining the individual processor vtus from the pvtu file.
    Args:
        pvtu_path   (str):  Path of the pvtu file.
    Returns:
        vtus    (list): All vtu files that need to be merged for this timestep.
    """
    tree = defusedxml.ElementTree.parse(pvtu_path)
    root = tree.getroot()
    vtus = []
    for i in range(len(root[0])):
        if "Source" in root[0][i].attrib:
            vtus.append(root[0][i].attrib["Source"])
    return vtus


if __name__ == "__main__":
    prepare_baci_input()
